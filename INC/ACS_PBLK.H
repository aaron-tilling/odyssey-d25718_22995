#ifndef ACS_PBLK_H
#define ACS_PBLK_H

#include "mag.h"


/*
** 1.  CCS Bad List
*/
typedef struct serial_number
{
  byte  sf;        /*  status flag, where     0  ::=  active     */
                   /*                         1  ::=  detected   */
                   /*                       255  ::=  undefined  */
  byte  ct;        /*  card type                                 */
  byte  pc;        /*  passenger category                        */
  byte  sq;        /*  sequence number                           */
  byte  s3;        /*  series number; high byte                  */
  byte  s2;        /*  series number                             */
  byte  s1;        /*  series number                             */
  byte  s0;        /*  series number; low byte                   */
}  S_NUM;


typedef struct serial_number_range
{
  S_NUM      lo;      /*  start of range */
  S_NUM      hi;      /*  end of range   */
} LA_RANGE;


typedef struct bad_list
{
  byte      rev_id;    /*  revision number     */
  byte      allign;    /*  for word allignment */
  S_NUM     n[512];    /*  number              */
  RANGE     r[ 32];    /*  range               */
                       /*                      */
  byte crcL;           /*  Lo byte of CRC      */
  byte crcH;           /*  Hi byte of CRC      */
}  B_LST;


/*
** Note:  The bad list is to be presented to the BVU and PEM sorted in
**      ascending order with no duplicate entries and no range overlap,
**      i.e.;
**
**          B_LST.n[i]     <  B_LST.n[i+1]
**
**          B_LST.r[i].lo  <  B_LST.r[i].hi
**          B_LST.r[i].hi  <  B_LST.r[i+1].low - 1
*/

/*
**
**  2.  ACS Parameter Block
**
*/


/*
** RANGE DATA
*/
#define    AI      32      /*  maximum number of issuing  agencies    */
#define    AF      32      /*  maximum number of 1st  use agencies    */
#define    AL      32      /*  maximum number of last use agencies    */
#define    CT      16      /*  maximum number of card types           */
#define    PC       8      /*  maximum number of passenger categories */


/*
**  FARE TABLE DATA
*/
#define    PG       8      /*  maximum number of fare table pages           */
#define    LAFS    12      /*  maximum number of fare sets per page         */
#define    LAFE    12      /*  maximum number of fare entries per fare set  */


typedef struct agency_parameter_block
{
  byte rev_id;             /*  revision number                              */
  byte ag;                 /*  agency number                                */
                           /*  bit 6 of ag is capture flag                  */
                           /*  i chose bit 6 because bit 7 is already       */
                           /*  masked off for some reason                   */

  byte cv[AI][CT];         /*  CARD VALIDITY MATRIX                         */
                           /*                                               */
                           /*  This bitmapped matrix is defined such that   */
                           /*  -  rows correspond to issuing agency         */
                           /*  -  columns correspond to card type           */
                           /*  -  bits correspond to passenger category;    */
                           /*    i.e.,  bit 'n' corresponds to passenger    */
                           /*        category 'n' such that                 */
                           /*                                               */
                           /*          0  ::=  valid/defined                */
                           /*          1  ::=  invalid/undefined            */
                           /*                                               */
  byte ft[PG][LAFS][LAFE]; /*  FARE TABLE                                   */
                           /*                                               */
                           /*  This is an 8-page, 12x12 matrix where        */
                           /*  - page is selected via pr[PC] defined        */
                           /*    below                                      */
                           /*  - rows correspond to the driver-selected     */
                           /*    fareset                                    */
                           /*  - columns correspond to fare entries,        */
                           /*    encoded as fare in nickels                 */
                           /*                                               */
                           /*         0  -  250 ::=   fare in nickels       */
                           /*       251  -  254 ::=   disabled              */
                           /*               255 ::=   reserved              */
                           /*                                               */
                           /*  - note:  Of the twelve elements per          */
                           /*    row, the first 10 are associated with      */
                           /*    OCU Keys 0 through 9; element 10           */
                           /*    encodes the default fare, and element      */
                           /*    11 holds the implied transfer value.       */
                           /*                                               */
  byte pr[PC];             /*  PAGE REFERENCE                               */
                           /*                                               */
                           /*  This array allows for the assignment of a    */
                           /*  fare table page for each passenger category. */
                           /*                                               */
  byte tc[AF][4];          /*  TRANSFER CONTROL                             */
                           /*                                               */
                           /*  This matrix is encoded so that, for each     */
                           /*  of the 32 possible agencies of first use:    */
                           /*                                               */
                           /*  -  tc[af][0] encodes the incremental fare    */
                           /*              required to upgrade a local      */
                           /*              transfer to an IAT transfer,     */
                           /*              where                            */
                           /*                                               */
                           /*                0 - 250 ::= fare in nickels    */
                           /*              251 - 254 ::= reserved           */
                           /*              255       ::= no upgrade         */
                           /*                                               */
                           /*  -  tc[af][1] encodes the default grace       */
                           /*              period for incoming transfers    */
                           /*                                               */
                           /*  -  tc[af][2] encodes the grace period to     */
                           /*              be applied on Sundays and        */
                           /*              holidays                         */
                           /*                                               */
                           /*  -  tc[af][3] encodes a discretionary grace   */
                           /*              period                           */
                           /*                                               */
                           /*  The grace periods above are encoded as some  */
                           /*  discrete number of 15-minute increments.     */
                           /*                                               */
  byte at[2][PC];          /*  AUTO-TRANSFER TABLE                          */
                           /*                                               */
                           /*  Under certain circumstances, a transfer may  */
                           /*  be issued automatically.  The domain of the  */
                           /*  Auto-Transfer Table is restricted to the     */
                           /*  following card types:                        */
                           /*                                               */
                           /*    row 0, i.e., at[0][pc] ::= debitcards      */
                           /*    row 1, i.e., at[1][pc] ::= group cards     */
                           /*                                               */
                           /*  The value encoded for each card type and     */
                           /*  passenger category is defined as follows:    */
                           /*                                               */
                           /*    at[n][pc] = 0 ::= no automatic transfer    */
                           /*    at[n][pc] = 1 ::= local transfer           */
                           /*    at[n][pc] = 2 ::= IAT transfer             */
                           /*                                               */
  struct date_list         /*  DATE LIST                                    */
  {                        /*                                               */
    byte  f;               /*  control                                      */
    byte  s;               /*  spare                                        */
    byte  m;               /*  month of year                                */
    byte  d;               /*  day of month                                 */
  }  dl[16];               /*                                               */
                           /*  Up to 16 holidays and other special dates    */
                           /*  are stored in the Date List.  The control    */
                           /*  field 'f' is defined as follows:             */
                           /*                                               */
                           /*    dl[n].f =   0 -  11 ::= default fareset    */
                           /*    dl[n].f =        12 ::= no default         */
                           /*    dl[n].f =  13 - 254 ::= reserved           */
                           /*    dl[n].f =       255 ::= disabled           */
                           /*                                               */
  struct peak_period       /*  PEAK PERIOD TABLE                            */
  {                        /*                                               */
    byte  f;               /*  control                                      */
    byte  s;               /*  spare                                        */
    byte  h_start;         /*  hour   - start of peak                       */
    byte  m_start;         /*  minute - start of peak                       */
    byte  h_end;           /*  hour   - end of peak                         */
    byte  m_end;           /*  minute - end of peak                         */
  }  pp[2];                /*                                               */
                           /*  The Peak Period Table defines AM and PM      */
                           /*  peak periods respectively.  The control      */
                           /*  field is defined as above, i.e.,             */
                           /*                                               */
                           /*    pp[n].f =   0 -  11 ::= default fareset    */
                           /*    pp[n].f =        12 ::= no default         */
                           /*     pp[n].f =  13 - 254 ::= reserved          */
                           /*    pp[n].f =       255 ::= disabled           */
                           /*                                               */
  struct time_adjust       /* TIME ADJUST LIST                              */
  {                        /*                                               */
    byte  m;               /*  month of year                                */
    byte  d;               /*  day of month                                 */
  }  ta[2];                /*                                               */
                           /*  The Time Adjust List holds the dates for     */
                           /*  the Spring and Fall Time Change respectively */
                           /*                                               */
  byte crcL;               /*  Lo byte of CRC                               */
  byte crcH;               /*  Hi byte of CRC                               */

}  A_BLK;


#option sep_on segment S_LaCnf
   extern A_BLK A_blk;
   extern B_LST B_lst;
#option sep_off

/* int get_agency( void ); */
int get_fare_entry( int pc, int fs, int x );
int is_card_valid( byte ct, int ai, int pc );
int get_tc_data( int level, int af );
int get_auto_xfer_data( int ct, int pc );
/* S_NUM * scan_bad_list( FD_STRUCT *  ); */
int is_acs_ok( void );
int is_b_list_ok( void );
int load_acs( void );
int load_b_lst( void );
A_BLK * get_acs_addr( int );
A_BLK * get_temp_acs_addr( void );
B_LST * get_b_list_addr( int );
B_LST * get_temp_b_list_addr();
byte get_ablk_id();
byte get_blst_id();
void clr_acs_sig();
/* int scan_bad_ranges( FD_STRUCT * cd ); */


/*
** Passenger Categories
*/
#define ADULT        0x01     /* Adult                         */
#define ELDERLY      0x02     /* Elderly                       */
#define DISABLED     0x03     /* Disabled                      */
#define STUDENT      0x04     /* School - Secondary            */
#define CHILD        0x05     /* School - Elementary           */
#define FREE         0x06     /* Free                          */
#define EMPLOYEE     0x07     /* employee/maintenance          */


/*
** Card Types
*/
#define  DEBIT_CARD  0x01     /* Debit Card                    */
#define  SIG         0xF00D
#define  MAINTENANCE_CARD  0x0a

#endif

