/*----------------------------------------------------------------------------\
|  Src File:   cnf.h                                                          |
|  Authored:   09/17/96, tgh                                                  |
|  Function:   Defines & prototypes for farebox configuration data.           |
|  Comments:                                                                  |
|                                                                             |
|  Revision:                                                                  |
|      1.00:   09/17/96, tgh  -  Initial release.                             |
|      1.01:   10/28/98, sjb  -  cut time added in a time_t format            |
|      1.02:   12/03/98, sjb  -  add embed transfer Implemented               |
|      1.03:   04/09/03, aat  -  added breakdown of logon to include seting/  |
|                                clearing bit 4 for choosing GFI or CTS OCU   |
|                                                                             |
|           Copyright (c) 1993-1999  GFI All Rights Reserved                  |
\----------------------------------------------------------------------------*/
#ifndef  _CNF_H
#define  _CNF_H

#include "gen.h"

/* configuration flags
 */

/* ( Clist.PRIVATE.OpFlags )
 */
#define  C_LOG             0x0001    /* driver log enabled                  */
#define  C_DRIVER          0x0002    /* driver no. required                 */
#define  C_ROUTE           0x0004    /* route no. required                  */
#define  C_RUN             0x0008    /* run no. required                    */
#define  C_TRIP            0x0010    /* trim no. required                   */
#define  C_DIR             0x0020    /* direction required                  */
#define  C_STOP            0x0040    /* stop no. required                   */
#define  C_CUT             0x0080    /* cut time required                   */
#define  C_CITY            0x0100    /* city no. required                   */
#define  C_ETI             0x0200    /* embedded transfers implemented      */
#define  C_BV07            0x0400    /* flag to indicat new BV used         */
#define  C_EV_TIME         0x0800    /* ev timed event 1 = enabled          */
#define  C_CBID            0x1000    /* cashbox id implimented              */
#define  C_HOUR            0x2000    /* hourly r/r implimented              */
#define  C_VER4            0x4000    /* version 4 enabled                   */
#define  C_DATA            0x8000    /* data system enabled                 */

/* configuration flags ( Clist.PRIVATE.Spare )
*/
#define  C_EXPRESS         0x01      /* Express (Westchester)               */
#define  C_TIME            0x02      /* display time ver 4                  */
#define  C_DIGIT3          0x04      /* 3 digit display                     */
#define  C_TEXT            0x08      /* display tick/tokn text ver 4        */
#define  C_SWIPE_XFER      0x10      /* process xfer in swipe reader        */
#define  C_DLS_TIME        0x20      /* dls time 1st sun APR/last sun Oct   */
#define  C_DIRECTION       0x40      /* direction type  NSEW or I/O         */
#define  C_PPS_ENB         0x80      /* PPS system used                     */

/* configuration flags ( Clist.PUBLIC.Spare )
*/
#define  C_KEY12           0x01      /* 12 button key pad                   */
#define  C_KEY1690         0x02      /* 16 button key pad 90�               */
#define  C_TRIPENABLE      0x04      /* selection to prompt for trip or not */
#define  C_BLOCKENABLE     0x08      /* prompt for block instead of run     */
#define  C_FBOX_CONTROLED  0x10      /* fabox determines capture/return     */
#define  C_CAPTURE_ENB     0x20      /* capture dispite Data System setting */
#define  C_X_ENB           0x40      /* put 'X' on no longer valid cards    */
#define  C_EXP_ENB         0x80      /* print 'EXPIRED' on expired transfers*/

/* ( Cash_transaction time )
*/
/*
 * bits 0-3 - time in 5 minute increments when to store transaction
 *            unused at this time
 * bit 4    - set if J1708 Pid 501 enabled in place of Pid 508
 * bit 5    - farebox host to TRIM
 * bit 6    - 24 hour time format
 * bit 7    - initial login via card only
 */
#define  cash_trans_mask   0x0f      /* mask of transaction time lower nibble*/
#define  J1708_ENB501      0x10      /* J1708 Pid 501 enabled */
#define  FBX_IS_HOST       0x20
#define  FBX_12_24         0x40
#define  DRIVER_CARD_LOG   0x80      /* driver log in via card only */

/* ( Logon )
*/
#define  DRIVER_LOGON      0x01
#define  OOS_TIME_DISP     0x02
#define  CUBIC_DCU         0x04
#define  TRIM_STAT_LED     0x08
#define  NO_KEY_REPEAT     0x10
#define  IRMA_ENABLE       0x20
#define  J1708_FS_DIR_ENB  0x40
#define  DRIVER_LOGOFF     0x80   /* used to only clear driver no. entry */

/* ( Hourlyevent )
*/
#define  HOURLY_BITS       0x03
#define  CLEVER_DEVICE     0x04
#define  IDLE_CHK          0x08
#define  ADD_OFFSET_FLG    0x10
#define  DRI_FLAG          0x20
#define  SMARTCARD_FLG     0x40
#define  PROBE_ID_REQ      0x80

/* ( daylight )
*/
#define  DLS_FLAG          0x01
#define  BYPASS_PROBE      0x02
#define  EXT_TRIM_DIAG     0x04
#define  BLANK_KEY_DISPLAY 0x08
#define  LOGOFFDRIVER      0x10
#define  CAN_TWO_BILL      0x20
#define  EV_CONTROL_TME    0x40
#define  NO_HOLD           0x80

/* ( Clist2.mf )
*/
#define  ACIT_RECEIPT      0x01

/* ( Clist2.LA_MetroOption )
*/
#define  DISPLAY_KEY       0x01
#define  GFI_HOSTS_TRIM    0x02
#define  ZERO_RUN_ENB      0x04
#define  SAL__UNIT         0x08
#define  DIM_OCU           0x10
#define  FARESET_BLANK     0x20
#define  FBX_CONTROL       0x40
#define  DRV_OPR_PROMPT    0x80

/* ( Clist2.OOS_coin_bill )
*/
#define  C_COINENABLE      0x01      /* enable coin validator when logged off*/
#define  C_BILLENABLE      0x02      /* enable bill validator when logged off*/
#define  C_DS_LAYOUT       0x04      /* use layout received from ds          */
#define  C_AUTOTRIMBYPASS  0x08      /* auto trim bypass                     */
#define  C_MANUALFEEDTRIM  0x10      /* manual feed only                     */
#define  C_VOICE           0x20      /* use voice messeges instead of sound  */
#define  C_AUTO_TIC_PROC   0x40      /* process tickets immediately          */
#define  C_SWIPE_XFERS     0x80      /* enable xfers in swipe reader         */

/* ( Clist2.OptionFlags1 )
*/
#define  OF1_ROUTE_ENB     0x0001
#define  OF1_RUN_ENB       0x0002
#define  OF1_BLOCK_ENB     0x0004
#define  OF1_TRIPREQ_ENB   0x0008
#define  OF1_TRIP_ENB      0x0010
#define  OF1_DIR_ENB       0x0020
#define  OF1_DIREC_TYPE    0x0040
#define  OF1_CITY_ENB      0x0080
#define  STOCK_ADD_CRD     0x0100
#define  FRIENDLIES_ENB    0x0200
#define  BIG_FARES_ENB     0x0400
#define  OCU_MENU_ENB      0x0800
#define  CARD_AT_PROBE     0x1000
#define  LOGOFF_BILREJ     0x2000
#define  DISABL_HYBERNATE  0x4000
#define  OF1_ENB_FLG       0x8000

/* ( Clist2.OptionFlags2 )
*/
#define  BILL_RESET_ENB    0x0001
#define  SAVE_NEW_ROUTE    0x2000
#define  DIS_CARD_READER   0x4000
#define  EL_KEY_FLG        0x8000

/* ( Clist2.OptionFlags3 )
*/
#define  J1708_ENB         0x0001    /* J1708 enabled                        */
#define  J1708TRANS_ENB    0x0002    /* save j1708 diag transaction          */
#define  J1708REPEAT_ENB   0x0004    /* 1=don't save time received on J1708  */
#define  J1708DVRLOG_ENB   0x0008    /* send j1708 driver data               */
#define  J1708ALARM_ENB    0x0010    /* send j1708 alarms                    */
#define  J1708RSTOP_ENB    0x0020    /* 1=save bad time received transaction */
#define  REV_OCUSCREEN_ENB 0x0040    /* allow reversing the OCU screen       */
#define  CANADA_ENB        0x0080    /* select canada/us                     */
#define  TRIP_2_BLOCK      0x0100    /* change TRIP prompt to BLOCK          */
#define  ENGLISH_ENB       0x0000    /* ENGLISH                              */
#define  SPANISH_ENB       0x0200    /* SPANISH                              */
#define  FRENCH_ENB        0x0400    /* FRENCH                               */
#define  CHINESE_ENB       0x0600    /* CHINESE                              */
#define  LANGUAGE_MASK     0x0600    /* language mask                        */
#define  CAB_CHANGECARD    0x0800    /* change card on CAB                   */
#define  ENB_RECEIPT       0x1000    /* allow a receipt for CC or SC recharge*/
#define  AUTO_RECEIPT      0x2000    /* automatically issue receipt if TRiM  */
#define  SHORT_LOGOFF      0x4000    /* cut auto-logoff/power save time 50%  */
#define  BV_COUPON_ENB     0x8000    /* tell BV to operate in coupon/escrow  */

/* ( Clist2.OptionFlags4 )
*/
#define  DRIVERCHKFLG      0x01
#define  VIEW_BILLS        0x02
#define  TICK_BILL_OVR     0x04
#define  SPEC_CARD_ENB     0x08
#define  BACKLITE          0x10
#define  ROUTECHKFLG       0x20
/*#define  CABTIME           0x40 /* display time on CAB alpha-numeric display */
#define  USELOCKCODELIST   0x40
#define  TICK_PROCESS      0x80

/* ( Clist2.OptionFlags5 )
*/
#define  SONY_SCR          0x0001
#define  OTI3000_SCR       0x0002
#define  OTI6000_SCR       0x0003
#define  ASCOM_SCR         0x0004
#define  GEMPROXC2_SCR     0x0005
#define  SMARTCARD_MASK    0x000f
#define  ONE_VSC_BLOCK     0x0010
#define  TWO_VSC_BLOCKS    0x0020
#define  SC_BLOCK_MASK     0x0030
#define  NO_FARE_DISP      0x0040
#define  NO_FARE_CHNG      0x0080
#define  DISP_ZONE_ONLY    0x0100
#define  DISP_FARE_ZONE    0x0200
#define  TRIP_2_ZONE       0x0400
#define  LCD_IDLE_TOGGLE   0x0800
#define  LOGIN_UNLOCK      0x1000
#define  CITY_2_ZONE       0x2000
#define  ALL_DIRECTIONS    0x4000
#define  POST_PRNT_EXP     0x8000

/* ( Clist2.OptionFlags6 )
*/
#define  CONVERT_OLD_2_NEW 0x0001
#define  LIMITED_ISSUE     0x0002
#define  J1708_STATUSPOLL  0x0004
#define  EXACT_FARE        0x0008
#define  NO_KEY_DRIVER     0x0010
#define  ALLOW_0_LOGOFF    0x0020
#define  DRVR_CRD_NO_RIDE  0x0040
#define  RF_PORT_ENABLE    0x0080
#define  NEW_DRIVER_TRANS  0x0100
#define  ACS_SC            0x0200
#define  SC_SEARCH         0x0400
#define  ROUTE_MATRIX      0x0800
/** AAT - 04/15/2008 - Upper nibble of OF6 is used for
                       J1708 reset time
**/

/* ( Clist2.OptionFlags7 )
*/
#define  ZONE_SYSTEM       0x0001
#define  NO_BADLIST_SC     0x0002
#define  NO_BADLIST_MG     0x0004
#define  UNBADLIST         0x0008
#define  BLUE_SAME_ZERO    0x0010
#define  NEWFORMAT_SN      0x0800
#define  NO_REISSUE        0x1000
#define  NO_SHUFFLE        0x2000
#define  GOOD_LIST         0x4000
#define  SWAP_TEXT         0x8000

/* ( Clist2.OptionFlags8 )
*/
#define  ROUTE_MATRIX_OFF  0x0001

/* ( J1708Poll )
*/
#define  J1708_REQENABLE   0x80
#define  J1708_POSITION    0x40
#define  J1708_STOP        0x20
#define  J1708_TME_MASK    0x1f

/* ( Clist2.PrintOption )
*/
#define  BILLVALIDATOR     0x01
#define  TRIM_DISABLED     0x02
#define  LID_ACTIVE        0x04
#define  DATE_ADJUSTMENT   0x08 /* add 180 days on expired passes */
#define  OVERRIDESCREEN    0x10 /* Automaticlly go to bill override screen */
#define  PB_QUE_CRD        0x20
#define  PRINT_X           0x40
#define  PRINT_EXP         0x80


#define  NO_LOG           (Clist.PRIVATE.OpFlags & C_LOG)
#define  NO_DRIVER        (FbxCnf.driver == 0L && (Clist.PRIVATE.OpFlags & C_DRIVER))
#define  NO_ROUTE         (FbxCnf.route  == 0L && (Clist.PRIVATE.OpFlags & C_ROUTE))
#define  NO_RUN           ( FbxCnf.run   == 0L && !ZERO_RUN_OK &&\
                          ( Clist.PRIVATE.OpFlags & C_RUN ) )
#define  NO_TRIP          (FbxCnf.trip   == 0L && (Clist.PRIVATE.OpFlags & C_TRIP))
#define  NO_DIR           (FbxCnf.dir    == 0  && (Clist.PRIVATE.OpFlags & C_DIR))
#define  NO_STOP          (FbxCnf.stop   == 0L && (Clist.PRIVATE.OpFlags & C_STOP))
#define  NO_CUT           (Clist.PRIVATE.OpFlags & C_CUT)
#define  NO_CITY          (FbxCnf.city   == 0L && (Clist.PRIVATE.OpFlags & C_CITY))
#define  NO_ZONE          ((FbxCnf.zone  == 0L || FbxCnf.dst_zone == 0L )&& (Clist.PRIVATE.OpFlags & C_CITY ))
#define  EBED_XFER        (Clist.PRIVATE.OpFlags & C_ETI )
#define  BV_VERS07        (Clist.PRIVATE.OpFlags & C_BV07)
#define  CBID_ENB         (Clist.PRIVATE.OpFlags & C_CBID)
#define  EV_TME_CTRL      (Clist.PRIVATE.OpFlags & C_EV_TIME)
#define  HOUR_RR          (Clist.PRIVATE.OpFlags & C_HOUR)
#define  VER_4            (Clist.PRIVATE.OpFlags & C_VER4)
#define  DATA_ON          (Clist.PRIVATE.OpFlags & C_DATA)

#define  EXPRESS          (Clist.PRIVATE.Spare & C_EXPRESS)
#define  DISP_TIME        (Clist.PRIVATE.Spare & C_TIME )
#define  DIGIT3           (Clist.PRIVATE.Spare & C_DIGIT3)
#define  DISP_TEXT        (Clist.PRIVATE.Spare & C_TEXT )
#define  SWIPE_XFER       (Clist.PRIVATE.Spare & C_SWIPE_XFER )
#define  DLS_TIME         (Clist.PRIVATE.Spare & C_DLS_TIME )
#define  DIR_TYPE         (Clist.PRIVATE.Spare & C_DIRECTION )
/*#define  PPS_ON           (Clist.PRIVATE.Spare & C_PPS_ENB )*/

#define  KEY12            (Clist.PUBLIC.Spare & C_KEY12)
#define  KEY1690          (Clist.PUBLIC.Spare & C_KEY1690)
#define  TRIPENB          (Clist.PUBLIC.Spare & C_TRIPENABLE)
#define  BLOCKENB         (Clist.PUBLIC.Spare & C_BLOCKENABLE)
#define  FB_CONTROL       (Clist.PUBLIC.Spare & C_FBOX_CONTROLED)
#define  CAPTURE          (Clist.PUBLIC.Spare & C_CAPTURE_ENB)
#define  PUT_X            (Clist.PUBLIC.Spare & C_X_ENB)
#define  PUT_EXP          (Clist.PUBLIC.Spare & C_EXP_ENB)

/* ( Logon ) */
#define  DriverLogOn       ( Clist2.logon & DRIVER_LOGON )
#define  DispTime_NoLogo   ( Clist2.logon & OOS_TIME_DISP )
#define  CTS_Dcu           ( Clist2.logon & CUBIC_DCU )
#define  TrimStatusLED     ( Clist2.logon & TRIM_STAT_LED )
#define  NoOCU_KeyRepeat   ( Clist2.logon & NO_KEY_REPEAT )
#define  IRMAP_Check       ( Clist2.logon & IRMA_ENABLE )
#define  J1708_FS_DIR      ( Clist2.logon & J1708_FS_DIR_ENB )
#define  DriverLogOff      ( Clist2.logon & DRIVER_LOGOFF )

/* ( HourlyEvent ) */
#define  HOUR_EV_ENB       ( Clist2.hourlyevent & HOURLY_BITS )
#define  CLEVER_LOGIN      ( Clist2.hourlyevent & CLEVER_DEVICE )
#define  J1708IDLECHK      ( Clist2.hourlyevent & IDLE_CHK )
#define  ADD_OFFSET_FIX    ( Clist2.hourlyevent & ADD_OFFSET_FLG )
#define  DigitalRecorder   ( Clist2.hourlyevent & DRI_FLAG )
#define  SMART_CARD_ENB    ( Clist2.hourlyevent & SMARTCARD_FLG )
#define  PROBE_ENABLE      ( Clist2.hourlyevent & PROBE_ID_REQ )

/* ( Daylight ) */
#define  DLS_ENABLED       (Clist2.daylight & DLS_FLAG )
#define  PROBED_BYPASS     (Clist2.daylight & BYPASS_PROBE )
#define  TRIM_EXT_DIAG     (Clist2.daylight & EXT_TRIM_DIAG )
#define  KEY_DISABLED      (Clist2.daylight & BLANK_KEY_DISPLAY )
#define  ONE_KEYLOG_OFF    (Clist2.daylight & LOGOFFDRIVER )
#define  CAN_2_DOLLAR      (Clist2.daylight & CAN_TWO_BILL )
#define  FB_TIMED_EV       (Clist2.daylight & EV_CONTROL_TME)
#define  NO_HOLD_BYPASS    (Clist2.daylight & NO_HOLD)

/* Mf flags ( pass exception storeage ) */
#define  MarketReceipt     ( Clist2.mf & ACIT_RECEIPT )

/* ( RevStatus ) */
#define  J1708_PID501     (Clist2.cash_transact_time & J1708_ENB501)
#define  FBX_HOST_TRIM    (Clist2.cash_transact_time & FBX_IS_HOST)
#define  FBX_24HR_TIME    (Clist2.cash_transact_time & FBX_12_24)
#define  DRIVER_LOG_ENB   (Clist2.cash_transact_time & DRIVER_CARD_LOG)

/* ( OOS_Coin_Bill ) */
#define  OOS_COIN         (Clist2.OOS_coin_bill & C_COINENABLE )
#define  OOS_BILL         (Clist2.OOS_coin_bill & C_BILLENABLE )
#define  DS_LAYOUT        (Clist2.OOS_coin_bill & C_DS_LAYOUT )
#define  AUTO_TRIMBYP     (Clist2.OOS_coin_bill & C_AUTOTRIMBYPASS )
#define  MANUAL_TRIM      (Clist2.OOS_coin_bill & C_MANUALFEEDTRIM )
#define  VOICE_ENABLED    (Clist2.OOS_coin_bill & C_VOICE )
#define  AUTO_TICKET      (Clist2.OOS_coin_bill & C_AUTO_TIC_PROC )
#define  SWIPE_XFERS      (Clist2.OOS_coin_bill & C_SWIPE_XFERS )

/* ( LA_MetroOption ) */
#define  KEY_DISPLAY      ( Clist2.LA_MetroOption & DISPLAY_KEY )
#define  GFI_HOST_TRIM    ( Clist2.LA_MetroOption & GFI_HOSTS_TRIM )
#define  ZERO_RUN_OK      ( Clist2.LA_MetroOption & ZERO_RUN_ENB )
#define  CARDQUEST        ( Clist2.LA_MetroOption & SAL__UNIT )
#define  TURN_OFF_OCU     ( Clist2.LA_MetroOption & DIM_OCU )
#define  Blank_Fareset()  ( Clist2.LA_MetroOption & FARESET_BLANK )
#define  CaptureControl() ( Clist2.LA_MetroOption & FBX_CONTROL )
#define  Driver_Prompt()  ( Clist2.LA_MetroOption & DRV_OPR_PROMPT )

/* ( OptionFlags1 ) */
#define  OF1_ROUTE        ( Clist2.OptionFlags1 & OF1_ROUTE_ENB )
#define  OF1_RUN          ( Clist2.OptionFlags1 & OF1_RUN_ENB )
#define  OF1_BLOCK        ( Clist2.OptionFlags1 & OF1_BLOCK_ENB )
#define  OF1_TRIP_REQ     ( Clist2.OptionFlags1 & OF1_TRIPREQ_ENB )
#define  OF1_TRIP         ( Clist2.OptionFlags1 & OF1_TRIP_ENB )
#define  OF1_DIR          ( Clist2.OptionFlags1 & OF1_DIR_ENB )
#define  OF1_DIR_TYPE     ( Clist2.OptionFlags1 & OF1_DIREC_TYPE )
#define  OF1_CITY         ( Clist2.OptionFlags1 & OF1_CITY_ENB )
#define  OF1_TEST_CARD    ((Clist2.OptionFlags1 & STOCK_ADD_CRD) && OF1_ENABLED )
#define  OF1_FRIENDLY     ((Clist2.OptionFlags1 & FRIENDLIES_ENB) && OF1_ENABLED )
#define  OF1_BIG_FARES    ((Clist2.OptionFlags1 & BIG_FARES_ENB) && OF1_ENABLED )
#define  OF1_OCU_MENUS    ((Clist2.OptionFlags1 & OCU_MENU_ENB) && OF1_ENABLED )
#define  OF1_PRBTST_CRD   ((Clist2.OptionFlags1 & CARD_AT_PROBE) && OF1_ENABLED )
#define  OF1_RET_BILL     ((Clist2.OptionFlags1 & LOGOFF_BILREJ) && OF1_ENABLED && OOS_BILL )
#define  OF1_DISABL_HYB   ((Clist2.OptionFlags1 & DISABL_HYBERNATE) && OF1_ENABLED )
#define  OF1_ENABLED      ( Clist2.OptionFlags1 & OF1_ENB_FLG )

/* ( OptionFlags2 ) */
#define  OF2_BILL_OVR     ( Clist2.OptionFlags2 & BILL_RESET_ENB )
#define  OF2_COIN_REJ     ( Clist2.OptionFlags2 & 0x0002 )
#define  OF2_COIN_SND     ((Clist2.OptionFlags2 & 0x0004) && OF2_COIN_REJ)
#define  OF2_COIN_RSND    ((Clist2.OptionFlags2 & 0x0008) && OF2_COIN_REJ)
#define  OF2_CN_PFLS      ((Clist2.OptionFlags2 & 0x0010) && OF2_COIN_REJ)
#define  OF2_CN_DFLS      ((Clist2.OptionFlags2 & 0x0020) && OF2_COIN_REJ)
#define  OF2_CN_LEDS      ((Clist2.OptionFlags2 & 0x0040) && OF2_COIN_REJ)
#define  OF2_CN_MESS      ((Clist2.OptionFlags2 & 0x0080) && OF2_COIN_REJ)
#define  ENC_CURENT_ROUTE ( Clist2.OptionFlags2 & SAVE_NEW_ROUTE )
#define  NO_CARD_READER   ( Clist2.OptionFlags2 & DIS_CARD_READER )
#define  OF2_EL_KEY       ( Clist2.OptionFlags2 & EL_KEY_FLG )

/* ( OptionFlags3 ) */
#define  J1708            (Clist2.OptionFlags3 & J1708_ENB )
#define  J1708TRANS       (Clist2.OptionFlags3 & J1708TRANS_ENB )
#define  J1708REPEAT      (Clist2.OptionFlags3 & J1708REPEAT_ENB )
#define  J1708DVRLOG      (Clist2.OptionFlags3 & J1708DVRLOG_ENB )
#define  J1708ALARM       (Clist2.OptionFlags3 & J1708ALARM_ENB )
#define  J1708RSTOP       (Clist2.OptionFlags3 & J1708RSTOP_ENB )
#define  OF3_REV_SCREEN   (Clist2.OptionFlags3 & REV_OCUSCREEN_ENB )
#define  OF3_CANADA_USA   (Clist2.OptionFlags3 & CANADA_ENB )
#define  OF3_TRIP_BLOCK   (Clist2.OptionFlags3 & TRIP_2_BLOCK )
#define  OF3_LANGUAGE     (Clist2.OptionFlags3 & LANGUAGE_MASK)
#define  OF3_CAB_CHANGE   (Clist2.OptionFlags3 & CAB_CHANGECARD)
#define  OF3_RECEIPT      (Clist2.OptionFlags3 & ENB_RECEIPT)
#define  OF3_AUTO_RECEIPT (Clist2.OptionFlags3 & AUTO_RECEIPT)
#define  OF3_SHORT_LOGOFF (Clist2.OptionFlags3 & SHORT_LOGOFF)
#define  OF3_BV_ESCRO_ENB (Clist2.OptionFlags3 & BV_COUPON_ENB)

/* ( OptionFlags4 ) */
#define  DriverChkEnb()  (Clist2.OptionFlags4 & DRIVERCHKFLG)
#define  Disp_Tick()     (Clist2.OptionFlags4 & VIEW_BILLS)
#define  OvrTickBill()   (Clist2.OptionFlags4 & TICK_BILL_OVR)
#define  SpecCardTbl()   (Clist2.OptionFlags4 & SPEC_CARD_ENB)
#define  BackLight()     (Clist2.OptionFlags4 & BACKLITE)
#define  RouteChkEnb()   (Clist2.OptionFlags4 & ROUTECHKFLG)
/* #define  CABTime()       (Clist2.OptionFlags4 & CABTIME) */
#define  USE_LOCK_LIST   (Clist2.OptionFlags4 & USELOCKCODELIST)
#define  AutoTickProc()  (Clist2.OptionFlags4 & TICK_PROCESS)

/* ( OptionFlags5 ) */
#define  OF5_SMARTCARD     (Clist2.OptionFlags5 & SMARTCARD_MASK)
#define  OF5_SC_BLOCK_MASK (Clist2.OptionFlags5 & SC_BLOCK_MASK)
#define  OF5_NO_FARE_DISP  (Clist2.OptionFlags5 & NO_FARE_DISP)
#define  OF5_NO_FARE_CHNG  (Clist2.OptionFlags5 & NO_FARE_CHNG)
#define  OF5_DISP_ZONE_ONLY (Clist2.OptionFlags5 & DISP_ZONE_ONLY)
#define  OF5_DISP_FARE_ZONE (Clist2.OptionFlags5 & DISP_FARE_ZONE)
#define  OF5_TRIP_TO_ZONE  (Clist2.OptionFlags5 & TRIP_2_ZONE)
#define  OF5_LCD_IDLE      (Clist2.OptionFlags5 & LCD_IDLE_TOGGLE)
#define  OF5_LOGIN_CODE    (Clist2.OptionFlags5 & LOGIN_UNLOCK)
#define  OF5_CITY_TO_ZONE  (Clist2.OptionFlags5 & CITY_2_ZONE)
#define  OF5_ALL_DIRECT    (Clist2.OptionFlags5 & ALL_DIRECTIONS)
#define  OP5_POST_PRNT_EXP (Clist2.OptionFlags5 & POST_PRNT_EXP)

/* ( OptionFlags6 ) */
#define  OF6_OLD_2_NEW        (Clist2.OptionFlags6 & CONVERT_OLD_2_NEW)
#define  OF6_LIMITED_ISSUE    (Clist2.OptionFlags6 & LIMITED_ISSUE)
#define  OF6_J1708_STATUSPOLL (Clist2.OptionFlags6 & J1708_STATUSPOLL )
#define  OF6_EXACT_PRESET     (Clist2.OptionFlags6 & EXACT_FARE)
#define  OF6_NO_KEY_DRV_ENTRY (Clist2.OptionFlags6 & NO_KEY_DRIVER)
#define  OF6_ALLOW_0_LOGOFF   (Clist2.OptionFlags6 & ALLOW_0_LOGOFF)
#define  OF6_NO_DRVR_CRD_RIDE (Clist2.OptionFlags6 & DRVR_CRD_NO_RIDE)
#define  OF6_RF_PORT          (Clist2.OptionFlags6 & RF_PORT_ENABLE)
#define  OF6_NEW_DRIVER_TRANS (Clist2.OptionFlags6 & NEW_DRIVER_TRANS)
#define  OF6_ACS_SMARTCARD    (Clist2.OptionFlags6 & ACS_SC)
#define  OF6_AUTO_SC_DETECT   (Clist2.OptionFlags6 & SC_SEARCH)
#define  OF6_ROUTE_MATRIX     (Clist2.OptionFlags6 & ROUTE_MATRIX)
#define  OF6_J1708IDLE_RESET  ((Clist2.OptionFlags6 >>12) * 60 )

/* ( OptionFlags7 ) */
#define  OF7_ZONE_SYSTEM     (Clist2.OptionFlags7 & ZONE_SYSTEM)
#define  OF7_NOMARK_BL_SC    (Clist2.OptionFlags7 & NO_BADLIST_SC )
#define  OF7_NOMARK_BL_MG    (Clist2.OptionFlags7 & NO_BADLIST_MG )
#define  OF7_UNBADLIST_SC    ((Clist2.OptionFlags7 & (UNBADLIST|NO_BADLIST_SC)) == (UNBADLIST|NO_BADLIST_SC))
#define  OF7_BLUE_SAME_ZERO  (Clist2.OptionFlags7 & BLUE_SAME_ZERO )
#define  OF7_LOWLIMIT_PRT    ((Clist2.OptionFlags7 >>5)& 0x003f)  /* low limit printing on value ride cards */
#define  OF7_NEWFORMAT_SN    (Clist2.OptionFlags7 & NEWFORMAT_SN )
#define  OF7_NO_REPLACEMENT  (Clist2.OptionFlags7 & NO_REISSUE )
#define  OF7_NO_CARD_SHUFFLE (Clist2.OptionFlags7 & NO_SHUFFLE )
#define  OF7_GOOD_LIST       (Clist2.OptionFlags7 & GOOD_LIST )
#define  OF7_SWAP_FARE_TEXT  (Clist2.OptionFlags7 & SWAP_TEXT )

/* ( OptionFlags8 ) */
#define  OF8_RT_MATRIX_OFF   (Clist2.OptionFlags8 & ROUTE_MATRIX_OFF)
#define  OF8_ZONE_LIMIT   ((Clist2.OptionFlags8 >> 1) & 0x000f)

/* ( J1708Poll ) */
#define  J1708_Req        (Clist2.J1708Poll & J1708_REQENABLE )
#define  J1708_ReqPos     (Clist2.J1708Poll & J1708_POSITION )
#define  J1708_ReqStop    (Clist2.J1708Poll & J1708_STOP )
#define  J1708_Poll       (Clist2.J1708Poll & J1708_TME_MASK )

/* ( TransferPrintOption ) */
#define  NO_BV            (Clist2.PrintOption & BILLVALIDATOR)
#define  NO_TRIM          (Clist2.PrintOption & TRIM_DISABLED)
#define  LID_OPEN_DETECT  (Clist2.PrintOption & LID_ACTIVE)
#define  ADJUST_DATE      (Clist2.PrintOption & DATE_ADJUSTMENT)
#define  AUTO_BILL_SCREEN (Clist2.PrintOption & OVERRIDESCREEN)
#define  QUE_ISS_CRD      ((Clist2.PrintOption & PB_QUE_CRD) == 0 )
#define  PRINT_MASK       (Clist2.PrintOption & (PRINT_EXP|PRINT_X) )


/* fareset enabled */
#define  DefaultFareEnb   ( Clist2.FSMask & 0x8000 )

#define  BA_1            0x01   /* Accept   $1 bills */
#define  BA_2            0x02   /* Accept   $2 bills */
#define  BA_5            0x04   /* Accept   $5 bills */
#define  BA_10           0x08   /* Accept  $10 bills */
#define  BA_20           0x10   /* Accept  $20 bills */
#define  BA_50           0x20   /* Accept  $50 bills */
#define  BA_100          0x40   /* Accept $100 bills */

#define  ACCEPT_100S      ( Clist2.BillsTaken & BA_1 )
#define  ACCEPT_200S      ( Clist2.BillsTaken & BA_2 )
#define  ACCEPT_500S      ( Clist2.BillsTaken & BA_5 )
#define  ACCEPT_1000S     ( Clist2.BillsTaken & BA_10 )
#define  ACCEPT_2000S     ( Clist2.BillsTaken & BA_20 )
#define  ACCEPT_5000S     ( Clist2.BillsTaken & BA_50 )
#define  ACCEPT_10000S    ( Clist2.BillsTaken & BA_100 )


#define  ALL_CFLAGS    (word)\
                       (C_LOG|C_DRIVER|C_ROUTE|C_RUN|C_TRIP|C_DIR|C_STOP|\
                        C_CITY|C_ETI|C_CBID|C_HOUR|C_EV_TIME|C_VER4|C_DATA|\
                        C_BV07|C_EV_TIME)

/* Typedefs
*/
typedef struct                                  /* current configuration */
{
   long           busno;                        /* bus number     (0-999999) */
   long           fbxno;                        /* farebox number (0-999999) */
   long           driver;                       /* driver number  (0-999999) */
   long           route;                        /* route number   (0-999999) */
   long           run;                          /* run number     (0-999999) */
   long           trip;                         /* trip number    (0-999999) */
   long           stop;                         /* stop number    (0-999999) */
   long           city;                         /* city number    (0-999999) */
   unsigned int   fullsel;                      /* full fare amount selected */
   byte           cut[2];                       /* cut time HHMM  (0-2359)   */
   byte           cut2[2];                      /* cut time HHMM  (0-2359)   */
   byte           fareset;                      /* ?????                     */
   byte           dir;                          /* ?????                     */
   ulong          cuttm;                        /* cut time time_t           */
   ulong          cuttm2;                       /* cut time time_t           */
   long           latitude;
   long           longitude;
   byte           oldfare;
   byte           multi_city;                   /* used for one agency with  */
                                                /* multi location TRiM text  */
   long           old_driver;
   long           old_route;
   long           old_bus;
   long           zone;                         /* origination zone (zone system) */
   long           old_zone;                     /* old origination zone      */
   long           dst_zone;                       /* destination zone (zone system) */
   long           old_dst_zone;                   /* old destination zone      */
   long           tmp_dst_zone;                   /* temp. dest zone (zone system) */
   long           end_zone;
   word           gateno;
   byte           gateon_tm[2];
   byte           gateoff_tm[2];
   word           hour_fare_cnt;
   word           WIPortReset;
   byte           gate_entry_tm;
   byte           spare[61];                    /* spare room                */
}  FBXCNF;

/* Defines
*/
#pragma sep_on segment S_Cnf
extern  FBXCNF       FbxCnf;                    /* defined in "data_vxx.c" */
#pragma sep_off

/* Prototypes
*/
void  ConfigureEntry    ( int );

#endif /*_CNF_H*/

