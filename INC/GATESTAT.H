/*----------------------------------------------------------------------------\
|  Src File:   gatestat.h                                                     |
|  Authored:   11/28/94, tgh                                                  |
|  Function:   Defines & macros for gate status flags.                        |
|  Comments:   "Gflags" are allocated in "stat_vxx.c".                        |
|                                                                             |
|  Revision:                                                                  |
|      1.00:   11/28/94, tgh  -  Initial release.                             |
|                                                                             |
|           Copyright (c) 1993, 1994 GFI All Rights Reserved                  |
\----------------------------------------------------------------------------*/
#ifndef  _GSTATUS_H
#define  _GSTATUS_H

/*----------------------------------------------------------------------------\
|                        GATE STATUS FLAGS & MACROS                           |
\----------------------------------------------------------------------------*/

#define  G_COINREJ_SW      (ulong)0x00000001    /* dump key active         */
#define  G_PASSREJ_SW      (ulong)0x00000002    /* door switch active      */
#define  G_COINE           (ulong)0x00000004    /* coin throat blocked     */
#define  G_COINF           (ulong)0x00000008    /* bill sensor blocked     */
#define  G_COINA           (ulong)0x00000010    /* bill throat blocked     */
#define  G_COIND           (ulong)0x00000020    /* bypass switch active    */
#define  G_COINB           (ulong)0x00000040    /* burnin test jumper      */
#define  G_COINC           (ulong)0x00000080    /* memory clear jumper     */
#define  G_HD30_SW         (ulong)0x00000100    /* spare 1 jumper          */
#define  G_HD60_SW         (ulong)0x00000200    /* spare 2 jumper          */
#define  G_HDHOME_SW       (ulong)0x00000400    /* spare 3 jumper          */
#define  G_OOS_SW          (ulong)0x00000800    /* cashbox present timer   */
#define  G_FREEW_SW        (ulong)0x00001000    /* currently probing       */
#define  G_FARE_SLW        (ulong)0x00002000    /* probe just completed    */
#define  G_FARE_MED        (ulong)0x00004000    /* full cashbox            */
#define  G_FARE_FST        (ulong)0x00008000    /* bypass switch was active*/
#define  G_FARESET_KEY     (ulong)0x00010000    /* invalid configuration   */
#define  G_FARE_STEP       (ulong)0x00020000    /* farebox in self test    */
#define  G_XFER_ISS        (ulong)0x00040000    /* farebox in self test    */
#define  G_XFER_THROAT     (ulong)0x00080000    /* farebox in self test    */
#define  G_SPARE_1         (ulong)0x00100000    /* farebox in self test    */
#define  G_SPARE_2         (ulong)0x00200000    /* farebox in self test    */
#define  G_SPARE_3         (ulong)0x00400000    /* farebox in self test    */
#define  G_INNER_DOOR      (ulong)0x00800000    /* farebox in self test    */
#define  G_OUTER_DOOR      (ulong)0x01000000    /* farebox in self test    */
#define  G_HD30_DECT       (ulong)0x02000000    /* farebox in self test    */
#define  G_HD60_DECT       (ulong)0x04000000    /* farebox in self test    */
#define  G_TM_CLOSE_GATE   (ulong)0x08000000    /* closed based on on/off time */
#define  G_CM_CLOSE_GATE   (ulong)0x10000000    /* close based on action taken */
#define  G_CLOSE_PROC_CM   (ulong)0x20000000    /* open/closed received  from station */
#define  G_INVALIDCNF      (ulong)0x40000000    /* */
#define  G_GATE_OOS        (ulong)0x80000000

/* If any of these flags are set we're
   out of service.
*/
#define  CRITICAL_GFLAGS (unsigned long)\
                        (G_INVALIDCNF|G_OUTER_DOOR|G_FARESET_KEY|G_TM_CLOSE_GATE|G_CM_CLOSE_GATE)

#define  NOTIFY_GFLAGS (unsigned long)\
                        (G_INVALIDCNF|G_OUTER_DOOR|G_FARESET_KEY|G_TM_CLOSE_GATE|G_CM_CLOSE_GATE|G_GATE_OOS )

extern   unsigned long        Gflags;

#define  Set_Gflag( f )       ( Gflags |= (f) )
#define  Clr_Gflag( f )       ( Gflags &=~(f) )
#define  Tst_Gflag( f )       ( Gflags &  (f) )


#define  Gate_Debug_ON        (Gate_Debug & 1)
#define  GATE_DISPLAY         (Gate_Debug & 2)
#define  GATE_DATA            (Gate_Debug & 4)
#define  GATE_RAW             (Gate_Debug & 8)

#define  KEY_SWITCH_TM        MSEC( 350 )

#define  GATE_PROG_KEY        '*'
#define  GATE_STEP_KEY        '#'
#define  GATE_FST_KEY         '1'
#define  GATE_MED_KEY         '2'
#define  GATE_SLO_KEY         '3'


#define  DIAG_1ST_ENTRY    0x80000000

/**
*** Menu Modes
**/
#define  GateNormalMode       0
#define  GateMenuMode         1

/** Interrogation Menu **/
#define  InterrogateMode      2
   #define  GateFirmware      1
   #define  GateVersions      2
   #define  GataDataDisplay   3

/** Diagnostics Menu **/
#define  GateDiagMode         3
   #define  GateDoors         1
   #define  GateSwitches      2
   #define  GateTripod        3
   #define  GateSolenoidTest  4
   #define  GateEntryLampTest 5
   #define  GateSonalertTest  6
   #define  GateDeviceTest    7
   #define  GateDisplayTest   8
   #define  GateCommTest      9
   #define  GateExtendedDiag 10
      #define  Gate_Flags     1
      #define  Trim_Flags     2
      #define  Status_Flags   3
      #define  Process_Flags  4
      #define  Ability_Flags  5
      #define  Ability2_Flags 6
   #define  GateCheck         16

/** Setup Menu **/
#define  GateSetupMode        4
   #define  GateSerial        1
   #define  GateNumber        2
   #define  StationNumber     3
   #define  RailLineNumber    4
   #define  RouteNumber       5
   #define  GateDirect        6
   #define  GateTime          7
   #define  GateDate          8
   #define  GateDay           9
   #define  GateOnTime        10
   #define  GateOffTime       11
   #define  GateEntryTime     12
   #define  GateSwipe_EnbDis  13
   #define  GateSC_EnbDis     14
   #define  GateTrim_EnbDis   15
   #define  GateFareset       16
   #define  Gate_Exit_EnbDis  17
   #define  Gate_ShowID_Passes 18
   #define  Gate_ShowID_Lites  19
   #define  Gate_Type          20

/** Maintenance Menu **/
#define GateMaintMode         5
   #define  Gate_Open_Close   1
   #define  Gate_Clr_Trim     2
   #define  GateCreatePCMCIA  4

/** Gate Types **/
#define  LIGHT_IN             0
#define  LIGHT_OUT            1
#define  HEAVY_IN             2
#define  HEAVY_OUT            3

extern  int  GateMode;
extern  int  GateSubMenu;
extern  int  GateKey;

typedef struct
{
  byte    type;
  byte    spare1;
  word    gate_type        : 1;  /* standard/handicap */
  word    exit_disabled    : 1; /* exits not allowed */
  word    freewheel        : 1; /* free wheel switch active */
  word    trim_off         : 1; /* offline/disabled */
  word    sc_off           : 1; /* offline/disabled */
  word    sw_off           : 1; /* swipcard disabled */
  word    door             : 1; /* open/closed */
  word    entry_restricted : 1; /* reduced fare card restricted (0 = process 1 return)*/
  word    reduced_process  : 1; /* 0 = process, 1 = hold and wait for acceptance */
  word    reduce_lites     : 1; /* flash lites on reduced fare entry */
  word    tme_cmd_oos      : 1; /* time and/or commanded OOS   */
  word    oos              : 1; /* gate out of service */
  word    trim_jam         : 1; /* trim jam */
  word    offline          : 1; /*set by station controller */
  word    spare_bits       : 2;
  ulong   entries;
  ulong   exits;
  ulong   serialno;
  ulong   gflags;
  ulong   sflags;
  ulong   pflags;
  ulong   aflags;
  ulong   a2flags;
  ulong   trimflags;
  word    version;
  word    trim_version;
  word    sc_version;
  word    gate_no;
  word    pending_entries;   /* current entries pending at gate */
  word    gate_open_time;
  word    gate_close_time;
  byte    spare2[6];
} GATESTATUS;

/* Prototypes
*/
void  Gate_Menu      ( void );
void  SolenoidTest   ( ulong );
void  CloseGate      ( void );
void  OpenGate       ( void );

#endif


