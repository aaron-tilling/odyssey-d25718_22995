/*----------------------------------------------------------------------------\
|  Src File:   gen.h                                                          |
|  Authored:   01/27/94, tgh                                                  |
|  Function:   Defines & prototypes for misc. functions / data.               |
|  Comments:                                                                  |
|                                                                             |
|  Revision:                                                                  |
|      1.00:   01/27/94, tgh  -  Initial release.                             |
|                                                                             |
|           Copyright (c) 1993, 1994 GFI/USPS All Rights Reserved             |
\----------------------------------------------------------------------------*/
#ifndef  _GEN_H
#define  _GEN_H

/* Typedefs
*/
typedef  unsigned char  byte;
typedef  unsigned int   word;
typedef  unsigned long  ulong;

typedef  void           (* FPTR) ();            /* ?   parameter, void ret. */
typedef  int            (*nFPTR) ();            /* ?   parameter,  int ret. */
typedef  void           (* FPTRn)(int);         /* int parameter, void ret. */
typedef  int            (*nFPTRn)(int);         /* int parameter,  int ret. */
typedef  int            (*nFPTRv)( void* );     /* v*  parameter,  int ret. */
typedef  void           (* FPTRv)( void* );     /* v*  parameter, void ret. */
typedef  void           (* FPTRs)( char* );     /* c*  parameter, void ret. */

/* Defines
*/
/**
#define   PROG_22985
/**
#define   PROG_22988
/**/

#define  BIT0     0x01
#define  BIT1     0x02
#define  BIT2     0x04
#define  BIT3     0x08
#define  BIT4     0x10
#define  BIT5     0x20
#define  BIT6     0x40
#define  BIT7     0x80


#define  TRUE     1
#define  FALSE    0

/*    terminal definitions    */
#define E_LINE       "\033[0K"
#define GOTOXY(x,y)  "\033[%d;%dH", y, x
#define SAVE_XY      "\033[s"
#define RESTORE_XY   "\033[u"
#define NORMVID      "\033[0m"
#define REVVID       "\033[7m"

#define FACE         "\x1"
#define AM           "\x2"
#define PM           "\x3"
#define THERM        "\x4"
#define BLK_BOX      "\x5"
#define RECTANGLE    "\x6"
#define HORIZ_BAR    "\x7"
#define VERT_BAR     "\x8"
#define T_BAR        "\x9"
#define HORIZ_LINE   "\xA"
#define THERMOMETER  "[\x4\x4\x4\x4\x4\x4\x4\x4]"
/*#define BLANK        "\x0B\x0B\x0B\x0B\x0B\x0B\x0B\x0B\x0B" */
#define BLANK        "\x20\x20\x20\x20\x20\x20\x20\x20\x20"
#define LNG_THIN_LINE "\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07"
#define LNG_THIK_LINE "\x05\x05\x05\x05\x05\x05\x05\x05\x05\x05\x05\x05\x05\x05\x05\x05\x05\x05\x05\x05\x05\x05\x05\x05"

#define  _INITIAL     0
#define  _NEW_THERM   1
#define  _NEW_ISSUE   2
#define  _RECHARGE    3


/* Macros */
/*#define clrscr()           printf("\x1b[2J \x1b[0;0H")
*/

/* prototype */
void  clrscr( void );  

#endif
