/*----------------------------------------------------------------------------\
|  Src File:   gfisock.h                                                      |
|  Authored:   08/07/01, tgh                                                  |
|  Function:   Defines, prototypes & macros for GFI socket processes.         |
|  Comments:                                                                  |
|                                                                             |
|  Revision:                                                                  |
|      1.00:   09/10/01, tgh  -  Initial release.                             |
|      1.01:   12/16/04, tgh  -  Initial release.                             |
|      1.02:   01/08/06, tgh  -  Added table status command/response define.  |
|                                                                             |
|           Copyright (c) 2001-2006  GFI All Rights Reserved                  |
\----------------------------------------------------------------------------*/
#ifndef  _GFISOCK_H
#define  _GFISOCK_H

#include  <gen.h>



/* typedefs
*/


typedef struct                                  /* message header            */
{
   ulong                sync;                   /* sync = 0xfeedf00d         */
   long                 len;                    /* message length            */
   word                 ver;                    /* client/server version     */
   byte                 cs_type;                /* client/server type        */
   byte                 spare1;                 /* spare 1                   */
   long                 id;                     /* client ID                 */
   long                 type;                   /* message type              */
   word                 seq;                    /* message sequence          */
   word                 blk_n;                  /* message block             */
   word                 blk_t;                  /* total blocks              */
   short int            status;                 /* status                    */
   short int            err;                    /* errno                     */
   word                 flags;                  /* flags                     */
   word                 dat_crc;                /* CRC-16 on data            */
   word                 hdr_crc;                /* CRC-16 on header          */
}  GS_HDR;


typedef struct                                  /* file header               */
{
   long                 size;                   /* file size                 */
   time_t               mtime;                  /* last modification time    */
   ulong                cs;                     /* binary checksum           */
   ulong                crc;                    /* CRC-32                    */
   char                 sfn[128];               /* source      filename      */
   char                 dfn[128];               /* destination filename      */
}  GS_FILEHDR;


typedef struct                                  /* date & time               */
{
   time_t               t;                      /* time_t as in "time.h"     */
   word                 sec;                    /* struct tm values ...      */
   word                 min;                    /* don't use actual struct tm*/
   word                 hour;                   /* here since size varies.   */
   word                 mday;
   word                 mon;
   word                 year;
   word                 wday;
   word                 yday;
   word                 isdst;
}  GS_TIME;


typedef struct                                  /* file transfer control     */
{
   time_t               beg;                    /* begin time                */
   time_t               end;                    /* end time                  */
   long                 cnt;                    /* bytes sent or received    */
   long                 pos;                    /* file position             */
   GS_FILEHDR           filehdr;                /* file header               */
}  GS_FILECNTL;


typedef struct                                  /* queued command            */
{
   long     cmd;                                /* command                   */
   word     flags;                              /* flags                     */
   char     sfn[128];                           /* source filename           */
   char     dfn[128];                           /* destination filename      */
}  GS_CMD;


#define  GS_HDR_SZ      (sizeof(GS_HDR))
#define  GS_FILEHDR_SZ  (sizeof(GS_FILEHDR))
#define  GS_TIME_SZ     (sizeof(GS_TIME))
#define  GS_FILECNTL_SZ (sizeof(GS_FILECNTL))
#define  GS_CMD_SZ      (sizeof(GS_CMD))
#define  GS_RXBUF_SZ    (2*GS_TXBUF_SZ)         /* receive  buffer size      */
#define  GS_TXBUF_SZ    GS_MAXMSG_SZ            /* transmit buffer size      */
#define  GS_MAXMSG_SZ   0x800                   /* maximum message size      */
#define  GS_MAXDAT_SZ   (GS_MAXMSG_SZ-GS_HDR_SZ)/* maximum <data>  size      */


/* defines
*/
#define  SIGNATURE         0xfeedf00d
#define  GS_VER            100
#define  DATA_SYSTEM       1                    /* client/server types       */
#define  NETWORK_MANAGER   2
#define  DATA_CONCENTRATOR 3


                                                /* [G]FI [S]ocket [S]tatus   */
                                                /* & macros to manipulated   */

#define  GSS_OFFLINE       (ulong)0x00000001
#define  GSS_DIALUP        (ulong)0x00000002
#define  GSS_SYNC_FS       (ulong)0x00000004    /* sync. fare structure      */
#define  GSS_SYNC_BL       (ulong)0x00000008    /* sync. bad list            */
#define  GSS_SYNC_CNF      (ulong)0x00000010    /* sync. fbx configuration   */
#define  GSS_SYNC_TIME     (ulong)0x00000020    /* sync. date and time       */
#define  GSS_REQ_DATA      (ulong)0x00000040    /* request data files        */
#define  GSS_CONNECTED     (ulong)0x00000080    /* connected at one time     */
#define  GSS_DATABASE      (ulong)0x00000100    /* database connection       */
#define  GSS_SYNC_INFRCTR  (ulong)0x00000200    /* sync. infractor file      */
#define  GSS_FSv2          (ulong)0x00000400    /* farestructure version 2   */
#define  GSS_SYNC_SECTBL   (ulong)0x00000800    /* sync. table (security DB) */
#define  GSS_CHK_SND_DIR   (ulong)0x00001000    /* check "snd" directory     */

#define  GSS_DEFAULTS      (ulong)(GSS_OFFLINE     |\
                                   GSS_SYNC_FS     |\
                                   GSS_SYNC_BL     |\
                                   GSS_SYNC_CNF    |\
                                   GSS_SYNC_TIME   |\
                                   GSS_REQ_DATA    |\
                                   GSS_DATABASE    |\
                                   GSS_CHK_SND_DIR)

#define  SetGSS( s, f )    ((s) |= (f))         /* set   flag(s)             */
#define  ClrGSS( s, f )    ((s) &=~(f))         /* clear flag(s)             */
#define  TstGSS( s, f )    ((s) &  (f))         /* test  flag(s)             */


                                                /* [G]FI [S]ocket [F]lags    */
                                                /* (contained in GS_HDR)     */
#define  GSF_RSPREQ        (word)0x0001         /* response required         */
#define  GSF_SEQ           (word)0x0002         /* use sequence number       */
#define  GSF_BKUP          (word)0x0004         /* create backup file        */

#define  SetGSF( h, f )    ((h)->flags |= (f))  /* set   flag(s)             */
#define  ClrGSF( h, f )    ((h)->flags &=~(f))  /* clear flag(s)             */
#define  TstGSF( h, f )    ((h)->flags &  (f))  /* test  flag(s)             */


                                                /* [G]FI [S]ocket [M]essages */
   /* The following message types are paired
      in command (cmd) - response (rsp) pairs
      as indicated by the define's name.
   */

#define  GSM_CMD_POLL         0x00000001        /* poll                      */
#define  GSM_RSP_POLL         0x00000002        /*                           */
#define  GSM_CMD_DISC         0x00000003        /* disconnect                */
#define  GSM_RSP_DISC         0x00000004        /*                           */
#define  GSM_CMD_BLKHDR       0x00000005        /* block header              */
#define  GSM_RSP_BLKHDR       0x00000006        /*                           */
#define  GSM_CMD_BLKDAT       0x00000007        /* block data                */
#define  GSM_RSP_BLKDAT       0x00000008        /*                           */
#define  GSM_CMD_BLKEND       0x00000009        /* block end                 */
#define  GSM_RSP_BLKEND       0x0000000a        /*                           */
#define  GSM_CMD_FILESTAT     0x0000000b        /* file status               */
#define  GSM_RSP_FILESTAT     0x0000000c        /*                           */
#define  GSM_CMD_REQFILE      0x0000000d        /* request file              */
#define  GSM_RSP_REQFILE      0x0000000e        /*                           */
#define  GSM_CMD_EXEC         0x0000000f        /* execute a command         */
#define  GSM_RSP_EXEC         0x00000010        /*                           */
#define  GSM_CMD_TIME         0x00000011        /* set date and time         */
#define  GSM_RSP_TIME         0x00000012        /*                           */
#define  GSM_CMD_MOVE         0x00000013        /* move/rename file          */
#define  GSM_RSP_MOVE         0x00000014        /*                           */
#define  GSM_CMD_MKDIR        0x00000015        /* make directory            */
#define  GSM_RSP_MKDIR        0x00000016        /*                           */
#define  GSM_CMD_RMDIR        0x00000017        /* remove directory          */
#define  GSM_RSP_RMDIR        0x00000018        /*                           */
#define  GSM_CMD_DEL          0x00000019        /* delete file               */
#define  GSM_RSP_DEL          0x0000001a        /*                           */
#define  GSM_CMD_DIR          0x0000001b        /* file listing              */
#define  GSM_RSP_DIR          0x0000001c        /*                           */
#define  GSM_CMD_APPEND       0x0000001d        /* append to file            */
#define  GSM_RSP_APPEND       0x0000001e        /*                           */
#define  GSM_CMD_ABORT        0x0000001f        /* abort transfer            */
#define  GSM_RSP_ABORT        0x00000020        /*                           */
#define  GSM_CMD_FILEWATCH    0x00000021        /* watch for file(s)         */
#define  GSM_RSP_FILEWATCH    0x00000022        /*                           */
#define  GSM_CMD_FILENOTIFY   0x00000023        /* notify of file(s)         */
#define  GSM_RSP_FILENOTIFY   0x00000024        /*                           */
#define  GSM_CMD_SENDFILE     0x00000025        /* send file                 */
#define  GSM_RSP_SENDFILE     0x00000026        /*                           */
#define  GSM_CMD_TABLESTAT    0x00000027        /* table status              */
#define  GSM_RSP_TABLESTAT    0x00000028        /*                           */
#define  GSM_CMD_CALLFUNCTION 0x00001001        /* call function name in sfn */
#define  GSM_RSP_CALLFUNCTION 0x00001002        /* call function name in sfn */


/* external data
*/
extern   ulong          GS_Status;


/* prototypes
*/
int      RecvFile             ( char*, char*, word );
int      SendFile             ( char*, char*, word );
int      _MoveFile            ( char*, char*, word );
int      Exec                 ( char*, char*, word );


#endif /*_GFISOCK_H*/
