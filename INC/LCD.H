/*----------------------------------------------------------------------------\
|  Src File:   lcd.h                                                          |
|  Authored:   09/18/97, tgh                                                  |
|  Function:   Defines & prototypes for LCD display.                          |
|  Comments:                                                                  |
|                                                                             |
|  Revision:                                                                  |
|      1.00:   09/18/97, tgh  -  Initial release.                             |
|                                                                             |
|           Copyright (c) 1993-1997  GFI/USPS All Rights Reserved             |
\----------------------------------------------------------------------------*/
#ifndef  _LCD_H
#define  _LCD_H

#include <time.h>
#include <string.h>
#include "timers.h"

/* Defines
*/


/* Prototypes
*/
int   LCDprintf            ( int, int, char *fmt, ... );
int   LCDcls               ( void );
int   LCDmsg               ( tmr_t, char *fmt, ... );
int   LCDputs              ( int, int, char* );
int   LCDcenter            ( int, char* );
void  TakeReceipt          ( void );

#endif
