/*----------------------------------------------------------------------------\
|  Src File:   magdsp.h                                                       |
|  Authored:   09/20/94, ???                                                  |
|  Function:   Defines & prototypes for                                       |
|  Comments:                                                                  |
|                                                                             |
|  Revision:                                                                  |
|      1.00:   09/20/94, tgh  -  Initial release.                             |
|                                                                             |
|           Copyright (c) 1993, 1994 GFI All Rights Reserved                  |
\----------------------------------------------------------------------------*/
#ifndef  _MAGDSP_H
#define  _MAGDSP_H

/* Prototypes
*/

void dsp_card( int type );

#endif /*_MAGDSP_H*/
