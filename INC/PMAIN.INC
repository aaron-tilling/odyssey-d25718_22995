*-----------------------------------------------------------------------------+
*  Src File:   pmain.inc                                                      |
*  Authored:   04/20/94, cj, ms ?                                             |
*  Function:                                                                  |
*  Comments:   Modified for USPS                                              |
*              fixme, this should be looked over since it came from           |
*              unreliable sources...                                          |
*                                                                             |
*  Revision:                                                                  |
*      1.00:   04/20/94, tgh  -  Modified for USPS                            |
*      1.01:   07/15/95, tgh  -  Change CSboot size from 128k to 512k.        |
*      1.01:   05/13/05, sjb  -  Change CSboot size from 512k to 1meg.        |
*                                                                             |
*           Copyright (c) 1993-1995  GFI/USPS All Rights Reserved             |
*-----------------------------------------------------------------------------+

csbt_8:         equ     0000000000000010b
csbt_16:        equ     0000000000000011b
cs0_8:          equ     0000000000001000b
cs0_16:         equ     0000000000001100b
cs1_8:          equ     0000000000100000b
cs1_16:         equ     0000000000110000b
cs2_8:          equ     0000000010000000b
cs2_16:         equ     0000000011000000b
cs3_8:          equ     0000001000000000b
cs3_16:         equ     0000001100000000b
cs4_8:          equ     0000100000000000b
cs4_16:         equ     0000110000000000b
cs5_8:          equ     0010000000000000b
cs5_16:         equ     0011000000000000b
cs6_8:          equ     0000000000000010b
cs6_16:         equ     0000000000000011b
cs7_8:          equ     0000000000001000b
cs7_16:         equ     0000000000001100b
cs8_8:          equ     0000000000100000b
cs8_16:         equ     0000000000110000b
cs9_8:          equ     0000000010000000b
cs9_16:         equ     0000000011000000b
cs10_8:         equ     0000001000000000b
cs10_16:        equ     0000001100000000b
cs10_do:        equ     0000000000000000b
cs10_af:        equ     0000000100000000b
cs6_af:         equ     0000000000000001b
cs7_af:         equ     0000000000000100b
cs8_af:         equ     0000000000010000b

cspar0_iv:      equ     csbt_16!cs0_16!cs1_16!cs2_16!cs3_16!cs4_8!cs5_8
cspar1_iv:      equ     cs6_af!cs7_af!cs8_16!cs9_16!cs10_8


blk_size_2k:    equ     000h
blk_size_8k:    equ     001h
blk_size_16k:   equ     002h
blk_size_64k:   equ     003h
blk_size_128k:  equ     004h
blk_size_256k:  equ     005h
blk_size_512k:  equ     006h
blk_size_1m:    equ     007h

cs_base_adrs:   set     0000000h
cs_blk_size:    set     blk_size_1m
csbarbt_iv:     equ     ((cs_base_adrs>>8)&0fff8h)!cs_blk_size

cs_base_adrs:   set     0100000h
cs_blk_size:    set     blk_size_1m
csbar0_iv:      equ     ((cs_base_adrs>>8)&0fff8h)!cs_blk_size

cs_base_adrs:   set     0100000h
cs_blk_size:    set     blk_size_1m
csbar1_iv:      equ     ((cs_base_adrs>>8)&0fff8h)!cs_blk_size

cs_base_adrs:   set     0200000h
cs_blk_size:    set     blk_size_1m
csbar2_iv:      equ     ((cs_base_adrs>>8)&0fff8h)!cs_blk_size

cs_base_adrs:   set     0200000h
cs_blk_size:    set     blk_size_1m
csbar3_iv:      equ     ((cs_base_adrs>>8)&0fff8h)!cs_blk_size

cs_base_adrs:   set     0300000h
cs_blk_size:    set     blk_size_2k
csbar4_iv:      equ     ((cs_base_adrs>>8)&0fff8h)!cs_blk_size

cs_base_adrs:   set     0400000h
cs_blk_size:    set     blk_size_2k
csbar5_iv:      equ     ((cs_base_adrs>>8)&0fff8h)!cs_blk_size

cs_base_adrs:   set     0000000h
cs_blk_size:    set     blk_size_2k
csbar6_iv:      equ     ((cs_base_adrs>>8)&0fff8h)!cs_blk_size

cs_base_adrs:   set     0000000h
cs_blk_size:    set     blk_size_2k
csbar7_iv:      equ     ((cs_base_adrs>>8)&0fff8h)!cs_blk_size

cs_base_adrs:   set     0800000h
cs_blk_size:    set     blk_size_1m
csbar8_iv:      equ     ((cs_base_adrs>>8)&0fff8h)!cs_blk_size

cs_base_adrs:   set     0900000h
cs_blk_size:    set     blk_size_1m
csbar9_iv:      equ     ((cs_base_adrs>>8)&0fff8h)!cs_blk_size

cs_base_adrs:   set     0700000h
cs_blk_size:    set     blk_size_2k
csbar10_iv:     equ     ((cs_base_adrs>>8)&0fff8h)!cs_blk_size


async:          equ     0000000000000000b
sync:           equ     1000000000000000b
off:            equ     0000000000000000b
lower:          equ     0010000000000000b
upper:          equ     0100000000000000b
both:           equ     0110000000000000b
read:           equ     0000100000000000b
write:          equ     0001000000000000b
rd_wr:          equ     0001100000000000b
as:             equ     0000000000000000b
ds:             equ     0000010000000000b
wait0:          equ     0000000000000000b
wait1:          equ     0000000001000000b
wait2:          equ     0000000010000000b
wait3:          equ     0000000011000000b
wait4:          equ     0000000100000000b
wait5:          equ     0000000101000000b
wait6:          equ     0000000110000000b
wait7:          equ     0000000111000000b
wait8:          equ     0000001000000000b
wait9:          equ     0000001001000000b
wait10:         equ     0000001010000000b
wait11:         equ     0000001011000000b
wait12:         equ     0000001100000000b
wait13:         equ     0000001101000000b
sp_cpu:         equ     0000000000000000b
sp_user:        equ     0000000000010000b
sp_supv:        equ     0000000000100000b
sp_su:          equ     0000000000110000b
ipl_all:        equ     0000000000000000b
ipl_1:          equ     0000000000000010b
ipl_2:          equ     0000000000000100b
ipl_3:          equ     0000000000000110b
ipl_4:          equ     0000000000001000b
ipl_5:          equ     0000000000001010b
ipl_6:          equ     0000000000001100b
ipl_7:          equ     0000000000001110b
avec_off:       equ     0000000000000000b
avec_on:        equ     0000000000000001b

* FLASH RAM
csorbt_iv:      equ     async!both!rd_wr!as!wait2!sp_su!ipl_all!avec_off

* STATIC RAM
csor0_iv:       equ     async!lower!rd_wr!as!wait2!sp_su!ipl_all!avec_off
csor1_iv:       equ     async!upper!rd_wr!as!wait2!sp_su!ipl_all!avec_off

* STATIC RAM
csor2_iv:       equ     async!lower!rd_wr!as!wait2!sp_su!ipl_all!avec_off
csor3_iv:       equ     async!upper!rd_wr!as!wait2!sp_su!ipl_all!avec_off

* QUART1
csor4_iv:       equ     async!upper!rd_wr!as!wait2!sp_su!ipl_all!avec_off

* RTC
csor5_iv:       equ     async!upper!rd_wr!as!wait2!sp_su!ipl_all!avec_off

* address a19
csor6_iv:       equ     async!off!write!as!wait0!sp_su!ipl_all!avec_off

* address a20
csor7_iv:       equ     async!off!write!as!wait0!sp_su!ipl_all!avec_off

* PC card
csor8_iv:       equ     async!both!rd_wr!as!wait5!sp_su!ipl_all!avec_off
csor9_iv:       equ     async!both!rd_wr!as!wait5!sp_su!ipl_all!avec_off

* FPGA
* csor10_iv:      equ     async!upper!rd_wr!as!wait2!sp_su!ipl_all!avec_off
csor10_iv:      equ     async!upper!rd_wr!ds!wait3!sp_su!ipl_all!avec_off


iarb0:          equ     0000000000000000b
iarb1:          equ     0000000000000001b
iarb2:          equ     0000000000000010b
iarb3:          equ     0000000000000011b
iarb4:          equ     0000000000000100b
iarb5:          equ     0000000000000101b
iarb6:          equ     0000000000000110b
iarb7:          equ     0000000000000111b
iarb8:          equ     0000000000001000b
iarb9:          equ     0000000000001001b
iarb10:         equ     0000000000001010b
iarb11:         equ     0000000000001011b
iarb12:         equ     0000000000001100b
iarb13:         equ     0000000000001101b
iarb14:         equ     0000000000001110b
iarb15:         equ     0000000000001111b
map_7:          equ     0000000000000000b
map_f:          equ     0000000001000000b
restrict:       equ     0000000010000000b
arb:            equ     0000000000000000b
show:           equ     0000000100000000b
show_arb:       equ     0000001000000000b
show_arb_halt:  equ     0000001100000000b
no_slave:       equ     0000000000000000b
frz_bm:         equ     0010000000000000b
frz_dog:        equ     0100000000000000b
clk_en:         equ     0000000000000000b
hide_clk:       equ     1000000000000000b


mcr_iv:         equ     clk_en!frz_bm!iarb15


_8389kz:        equ     00111111b
_16777kz:       equ     01111111b
_20972kz:       equ     11010011b
_25166kz:       equ     10101111b
_25690kz:       equ     10110000b


pit_off:        equ     00000h
_122us:         equ     00001h
_977us:         equ     00008h
_1098us:        equ     00009h
_5002us:        equ     00029h
_15600us:       equ     00080h
_31100us:       equ     000ffh
_125ms:         equ     00102h
_500ms:         equ     00108h
_1sec:          equ     00110h


pirq_dis:       equ     0000000000000000b
pirq1:          equ     0000000100000000b
pirq2:          equ     0000001000000000b
pirq3:          equ     0000001100000000b
pirq4:          equ     0000010000000000b
pirq5:          equ     0000010100000000b
pirq6:          equ     0000011000000000b
pirq7:          equ     0000011100000000b
pivn:           equ     0ffh

picr_iv:        equ     pirq4!pivn


tpscale_4:      equ     00000000b
tpscale_8:      equ     00000001b
tpscale_16:     equ     00000010b
tpscale_32:     equ     00000011b
tpscale_64:     equ     00000100b
tpscale_128:    equ     00000101b
tpscale_256:    equ     00000110b
tpscale_pclk:   equ     00000111b







