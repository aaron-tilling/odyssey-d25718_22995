*-----------------------------------------------------------------------------+
*  Src File:   quart.inc                                                      |
*  Authored:   01/11/96, tgh                                                  |
*  Function:   QUART equates for new farebox logic board.                     |
*  Comments:   This is a mix of DUART and QUART equates time could be taken   |
*              to remove redundant equates.                                   |
*                                                                             |
*  Revision:                                                                  |
*      1.00:   01/11/96, tgh  -  Initial release.                             |
*                                                                             |
*           Copyright (c) 1993-1996  GFI All Rights Reserved                  |
*-----------------------------------------------------------------------------+

        uart0base:     equ     0300000h
        uart1base:     equ     0300100h

        uart_port_a:   equ     000h     starting adds of registers of port A
        uart_port_b:   equ     010h     starting adds of registers of port B
        uart_port_c:   equ     020h     starting adds of registers of port C
        uart_port_d:   equ     030h     starting adds of registers of port D
        uart_port_e:   equ     040h     starting adds of registers of port E
        uart_port_f:   equ     050h     starting adds of registers of port F
        uart_port_g:   equ     060h     starting adds of registers of port G
        uart_port_h:   equ     070h     starting adds of registers of port H

        RHR:        equ     000h    receive holding register LRC[7] = 0  (r)
        THR:        equ     000h    transmit holding register LRC[7] = 0 (w)
        DLL:        equ     000h    div latch low byte LRC[7] = 1        (r/w)
        DLM:        equ     001h    div latch hi byte  LRC[7] = 1        (r/w)
        IER:        equ     001h    interrupt enable register LRC[7] = 1 (r/w)
        ISR:        equ     002h    interrupt status register            (r)
        FCR:        equ     002h    fifo control register                (w)
        LCR:        equ     003h    line control Regester                (r/w)
        MCR:        equ     004h    modem control Regester               (r/w)
        LSR:        equ     005h    line status Regester                 (r)
        MSR:        equ     006h    modem status Regester                (r)
        RS485:      equ     006h    rs485 turn around delay Regester     (w)
        SPR:        equ     007h    scratch pad register                 (r/w)

*       enhanced registers

        FCTR:       equ     008h    feature control register             (r/w)
        EFR:        equ     009h    enhansed function register           (r/w)
        TXCNT:      equ     00ah    xmit fifo level counter              (r)
        TXTRG:      equ     00ah    xmit fifo trigger level              (w)
        RXCNT:      equ     00bh    receive fifo level counter           (r)
        RXTRG:      equ     00bh    receive fifo trigger level           (w)
        XOFF1:      equ     00ch    xoff character 1                     (w)
        XCHAR:      equ     00ch    xchar        xon,xoff receive flags  (r)  
        XOFF2:      equ     00dh    xoff character 2                     (w)
        XON1:       equ     00eh    xon character 1                      (w)
        XON2:       equ     00fh    xon character 2                      (w)
  
*   uart channel configuration registers

        int_source: equ     080h    interrupt indicator                  (r)
        INT1:       equ     081h    interupt source                      (r)
        INT2:       equ     082h    interupt source                      (r)
        INT3:       equ     083h    interupt source                      (r)
        TMRCTRL:    equ     084h    timer control                        (r/w)
        TIMER:      equ     085h    reserved                             (r)
        TIMERLSB:   equ     086h    timer lsb                            (r/w)
        TIMERMSB:   equ     087h    timer msb                            (r/w)
        MODEX8:     equ     088h    8x mode                              (r/w)
        REG1:       equ     089h    reserved                             (r)
        URESET:     equ     08ah    reset register                       (w)
        USLEEP:     equ     08bh    enable uart sleep                    (r/w)
        DREV:       equ     08ch    device rev                           (r)
        DVID:       equ     08dh    device id (24 quad, 28 octal         (r)
        REG2:       equ     08eh    write to all uarts                   (r/w)


* bit mapped irq status    
        .tx_irq:    equ     1           transmit interrupt
        .rx_irq:    equ     2           receiver interrupt
        .rx_tm_out: equ     3           receiver timeout 
         rx_err:    equ     00000110b   reciever error mask


* bit mapped irq enable bits    
*        .txirq:     equ     0           transmit interrupt enable
*        .rxirq:     equ     1           receiver interrupt enable
*
        .rx_lirq:   equ     3           receiver line staus interrupt enable 
*
* Note: these values are for a non-divided clock
*       with a 7.372800 meg crystal
* used equ below for DLL(div latch low byte) & #0 DLM (div latch hi byte)
* except for 600 baud which will use 0 for DLL & 3 for DLM
* 
* MODE8 set to x16 unless 921.6k baud selected
*   
*   NOTE:B921600_X used as check for 921.6k baud to so MODE8 can
*        be checked because DLL the same for 460.8k & 921.6k, 
*        but MODE8 set to 1 on 921.6k and 0 on 460.8k
*
*        B_600:        equ     00000011b    600    baud (MCR[7]=0,X=16)
*        B_4800:       equ     01100000b    4800   baud (MCR[7]=0,X=16)
*        B_9600:       equ     00110000b    9600   baud (MCR[7]=0,X=16)
*        B_19200:      equ     00011000b    19.2K  baud (MCR[7]=0,X=16)
*        B_38400:      equ     00001100b    38.4K  baud (MCR[7]=0,X=16)
*        B_57600:      equ     00001000b    57.6K  baud (MCR[7]=0,X=16)
*        B_115200:     equ     00000100b    115.2K baud (MCR[7]=0,X=16)
*        B_230400:     equ     00000010b    230.4K baud (MCR[7]=0,X=16)
*        B_460800:     equ     00000001b    460.8K baud (MCR[7]=0,X=16)
*        B_921600:     equ     00000001b    921.6K baud (MCR[7]=0,X=8)
*        B_921600X:    equ     10000001b    921.6K baud (MCR[7]=0,X=8)

*
* Note: these values are for a non-divided clock
*       with a 14.745600 meg crystal
* used equ below for DLL(div latch low byte) & #0 DLM (div latch hi byte)
* except for 600 baud which will use 0 for DLL & 6 for DLM
* 
* MODE8 set to x16 for all baud rates selected
*   
        B_600:        equ     00000110b    600    baud (MCR[7]=0,X=16)
        B_4800:       equ     11000000b    4800   baud (MCR[7]=0,X=16)
        B_9600:       equ     01100000b    9600   baud (MCR[7]=0,X=16)
        B_19200:      equ     00110000b    19.2K  baud (MCR[7]=0,X=16)
        B_38400:      equ     00011000b    38.4K  baud (MCR[7]=0,X=16)
        B_57600:      equ     00010000b    57.6K  baud (MCR[7]=0,X=16)
        B_115200:     equ     00001000b    115.2K baud (MCR[7]=0,X=16)
        B_230400:     equ     00000100b    230.4K baud (MCR[7]=0,X=16)
        B_460800:     equ     00000010b    460.8K baud (MCR[7]=0,X=16)
        B_921600:     equ     00000001b    921.6K baud (MCR[7]=0,X=16)
        B_921600X:    equ     10000001b    921.6K baud (MCR[7]=0,X=8)

*
*  fifo limit
* 

fifo_limit            equ     64

