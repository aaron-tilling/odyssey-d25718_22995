/*----------------------------------------------------------------------------\
|  Src File:   rtc.h                                                          |
|  Authored:   02/09/94, tgh                                                  |
|  Function:   Defines & prototypes for real-time clock functions.            |
|  Comments:                                                                  |
|                                                                             |
|  Revision:                                                                  |
|      1.00:   02/09/94, tgh  -  Initial release.                             |
|                                                                             |
|           Copyright (c) 1993, 1994 GFI/USPS All Rights Reserved             |
\----------------------------------------------------------------------------*/
#ifndef  _RTC_H

#include <time.h>
#include "gen.h"

/* Defines
*/
#define DaySec  86400L
#define HrsSec   3600L
#define MinSec     60L
#define HrsMin     60L
#define DayMin   1440L
#define MIDNIGHT    24   /* 1 based or 0x18 hex                 */
/*
** Seconds from 1/1/1970 -> 1/1/1990: 631152000
** Seconds from 1/1/1990 -> 1/1/1997: 220924800
**                                    ---------
** Seconds from 1/1/1970 -> 1/1/1997: 852076800
*/
#define CUBIC_YEAR_ADJUST  631152000  /* seconds adj. from 1/1/70 - 1/1/90  */
#define ACS_YEAR_ADJUST    9862       /* Days from 1/1/70 -> 1/1/06         */  

/* Typedef(s)
*/
typedef struct
{
   byte  sec   [2];
   byte  min   [2];
   byte  hour  [2];
   byte  day   [2];
   byte  month [2];
   byte  year  [2];
   byte  week     ;
   byte  D        ;
   byte  E        ;
   byte  F        ;
}  RTC;

extern   int   BadClock;

/* Prototypes
*/
int         Initialize_Rtc ( void );
struct tm  *GetRtc         ( struct tm * );
int         SetRtc         ( struct tm * );

#define  _RTC_H
#endif

