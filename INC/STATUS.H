/*----------------------------------------------------------------------------\
|  Src File:   status.h                                                       |
|  Authored:   11/28/94, tgh                                                  |
|  Function:   Defines & macros for status flags.                             |
|  Comments:   "Sflags" are allocated in "stat_vxx.c".                        |
|                                                                             |
|  Revision:                                                                  |
|      1.00:   11/28/94, tgh  -  Initial release.                             |
|      1.01:   10/26/98, sjb  -  relmove ESCROW, FIRSTUSE, CARDPRESENT, &     |
|                                PROCESSING flags for trim to process.h       |
|                                                                             |
|           Copyright (c) 1993, 1994 GFI All Rights Reserved                  |
\----------------------------------------------------------------------------*/
#ifndef  _STATUS_H
#define  _STATUS_H

/*----------------------------------------------------------------------------\
|                          STATUS FLAGS & MACROS                              |
\----------------------------------------------------------------------------*/

#define  S_DUMP            (ulong)0x00000001    /* dump key active         */
#define  S_DOOR            (ulong)0x00000002    /* door switch active      */
#define  S_LATCHUP         (ulong)0x00000004    /* door latch up           */
#define  S_LATCHDWN        (ulong)0x00000008    /* door latch down         */
#define  S_COIN_RTN        (ulong)0x00000010    /* coin in coin return     */
#define  S_BYPASS          (ulong)0x00000020    /* bypass switch active    */
#define  S_BURNIN          (ulong)0x00000040    /* burnin test jumper      */
#define  S_MEMCLR          (ulong)0x00000080    /* memory clear jumper     */
#define  S_SPARES1         (ulong)0x00000100    /* spare 1 jumper          */
#define  S_SPARES2         (ulong)0x00000200    /* spare 2 jumper          */
#define  S_SPARES3         (ulong)0x00000400    /* spare 3 jumper          */
#define  S_CASHBOX         (ulong)0x00000800    /* cashbox present timer   */
#define  S_PROBING         (ulong)0x00001000    /* currently probing       */
#define  S_COMPLETEPENDING (ulong)0x00002000    /* probe just completed    */
#define  S_FULLCASHBOX     (ulong)0x00004000    /* full cashbox            */
#define  S_BYPASS_P        (ulong)0x00008000    /* bypass switch was active*/
#define  S_INVALIDCNF      (ulong)0x00010000    /* invalid configuration   */
#define  S_SELFTEST        (ulong)0x00020000    /* farebox in self test    */
#define  S_TRIMOFFLINE     (ulong)0x00040000    /* TRIM comm. offline      */
#define  S_INVALIDFS       (ulong)0x00080000    /* invalid fare structure  */
#define  S_TESTSET         (ulong)0x00100000    /* test set flag           */
#define  S_LID             (ulong)0x00200000    /* top lid switch active   */
#define  S_BILLOVRRIDE     (ulong)0x00400000    /* bill override enabled via driver */
#define  S_NOSTOCK         (ulong)0x00800000    /* stock empty             */
#define  S_LOWSTOCK        (ulong)0x01000000    /* low stock               */
#define  S_TRIMBYPASS      (ulong)0x02000000    /* trim in bypass          */
#define  S_BILL_OOS        (ulong)0x04000000    /* bill validator oos      */
#define  S_PROBED          (ulong)0x08000000    /* probed with alarm delay */
#define  S_BILL_JAM        (ulong)0x10000000    /* bill jam detected       */
#define  S_TRIM_JAM        (ulong)0x20000000    /* trim card jam detected  */
#define  S_SC_OFFLINE      (ulong)0x40000000    /* smart card off line     */
#define  S_OCU_OFFLINE     (ulong)0x80000000    /* OCU off line            */

#define  ALL_SFLAGS  (unsigned long)\
                     (S_DUMP|S_DOOR|S_LATCHUP|S_LATCHDWN|S_SC_OFFLINE|\
                      S_BYPASS|S_BURNIN|S_MEMCLR|S_SPARES1|S_SPARES2|\
                      S_SPARES3|S_CASHBOX|S_PROBING|S_FULLCASHBOX|S_BYPASS_P|\
                      S_INVALIDCNF|S_SELFTEST|S_TRIMOFFLINE|S_INVALIDFS|\
                      S_TESTSET|S_LID|S_BILLOVRRIDE|S_NOSTOCK|S_LOWSTOCK|\
                      S_TRIMBYPASS|S_BILL_OOS|S_PROBED|S_BILL_JAM|\
                      S_TRIM_JAM|S_COIN_RTN|S_OCU_OFFLINE)

/* If any of these flags are set we're
   out of service.
*/
#define  CRITICAL_FLAGS (unsigned long)\
                        (S_INVALIDCNF|S_DOOR|S_CASHBOX|S_LID)

#define  CTS_NO_REV_FLAGS (unsigned long)\
                          (S_DOOR|S_LATCHUP|S_CASHBOX|S_PROBING|\
                           S_FULLCASHBOX|S_SELFTEST|S_LID)


extern   unsigned long           Sflags;
extern   unsigned long           AutoDumpTime;
extern             int           BadBatteryDetect; /* bad battery detected */
extern            word           Driver_Rev;
extern            word           ChangeCardRev;
extern            word           CashboxBill;         /* paper in cashbox              */
extern            long           CashboxCoin;
extern           ulong           CashboxRevenue;
extern            int            CoinFullFlag;
extern            int            BillFullFlag;

/** AAT - ChangeCardThreshold: version 405 **/
extern            word           ChangeCardThreshold;
/**/

#define  Set_Sflag( f )          ( Sflags |= (f) )
#define  Clr_Sflag( f )          ( Sflags &=~(f) )
#define  Tst_Sflag( f )          ( Sflags &  (f) )

#define     MAX_BILLS             700
#define     MAX_COINS             80000


/* Prototypes
*/
int   InitializeStatus  ( void );
void  CheckStatus       ( void );
void  CheckCashbox      ( void );
void  Init_CBID_Irq2    ( int t );
int   MachineOK         ( void );
void  ServiceTest       ( void );
void  Batt_Check        ( void );
void  ClearShutDown     ( void );
void  OpenRemoteDoor    ( int );
void  LockRemoteDoor    ( void );

#endif

