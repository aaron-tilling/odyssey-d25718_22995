/*----------------------------------------------------------------------------\
|  Src File:   tests.h                                                        |
|  Authored:   09/20/94, ???                                                  |
|  Function:   Defines & prototypes for                                       |
|  Comments:                                                                  |
|                                                                             |
|  Revision:                                                                  |
|      1.00:   02/13/95, tgh  -  Initial release.                             |
|                             -  fixed return types.                          |
|                                                                             |
|           Copyright (c) 1993, 1994 GFI All Rights Reserved                  |
\----------------------------------------------------------------------------*/
#ifndef  _TESTS_H
#define  _TESTS_H


/* Define(s)
*/
   /* media list */
#define  AttrFlag( ndx, f )   (Dlist.MediaList[(ndx)].Attr & (f))
#define  SUNDAY         0
#define  SATURDAY       6
#define  GOODPEAK       0x01                    /* attribute flags */
#define  GOODOFFPEAK    0x02
#define  GOODWKD        0x04
#define  GOODSAT        0x08
#define  GOODSUN        0x10
#define  GOODHOL        0x20
#define  GOODFRIENDLY   0x40
#define  DISABLED       0x80

   /* transfer control table */
#define  TCAttrFlag( ndx, f ) (Dlist.TransferControl[(ndx)].Attr & (f))
#define  ODSR           0x01
#define  SDSR           0x02
#define  ODDR           0x04
#define  SDDR           0x08
#define  CAP0           0x10
#define  HOLD           0x20
#define  UPGRADE        0x40
#define  NO_AUTOXFER    0x80

/* Prototype(s)
*/

int   isbadlisted             ( CARD *c, B_SERIAL *s );
ulong is_date_ok              ( CARD *p );
ulong is_sc_date_ok           ( CARD *p );
int   is_friendly_ok          ( CARD *p );
int   is_friendly__ok         ( int );
ulong check_card_restrictions ( int index );
ulong check_xfer_restrictions ( int index, CARD *card );
word  validate_variable_info  ( int );
int   Upgrade                 ( int index );
word  validate_fixed          ( void );
int   CheckRouteRestrictions  ( CARD *card );
ulong CheckZoneRestrictions   ( CARD *card );
int   CheckDoorAccess         ( CARD *card );

#endif /*_TESTS_H */

