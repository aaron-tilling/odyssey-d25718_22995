/*----------------------------------------------------------------------------\
|  Src File:   timers.h                                                       |
|  Authored:   05/09/94, tgh                                                  |
|  Function:   Prototypes, defines & macros for system timer functions.       |
|  Comments:                                                                  |
|                                                                             |
|  Revision:                                                                  |
|      1.00:   05/09/94, tgh  -  Initial release.                             |
|      1.01:   11/23/98, sjb  -  change Shift tmr to Alt tmr                  |
|                                                                             |
|           Copyright (c) 1993-1996  GFI/USPS All Rights Reserved             |
\----------------------------------------------------------------------------*/
#ifndef  _TIMERS_H
#define  _TIMERS_H

#include  "gen.h"

/* selection of mpu system clock */
/* only one define, all others   */
/* should be undef               */
#undef    cry16777
#define   cry20972
#undef    cry25166
#undef    cry25690

/* These defines assume the use of oc1 (1 ms.) to drive the system timers
*/
#define  _1SEC          (tmr_t)1000             /* assuming 1 ms. period */
#define  _MINMS         (tmr_t)1                /* 1 ms. minmum */
#define  SECONDS( n )   ((tmr_t)( (tmr_t)(n) * _1SEC ))
#define  MSEC(n)        ((tmr_t)(n))

/* This macro should be used if the resolution
   if less than 1 ms.
#define  MSEC(n)        ((tmr_t)(((tmr_t)(n) >= _MINMS)?(tmr_t)(n)/_MINMS:1))
*/


/* Timer data structure definition
*/
/* #define  MAX_TIMERS     111 */
#define  tmr_expired(i) (tmr[i].t == 0 && tmr[i].f == 0)
#define  tmr_setflag(i) (tmr[i].f = 1)

typedef  unsigned long  tmr_t;                  /* timer type */

typedef struct
{
   tmr_t          t;                            /* timer */
   unsigned char  f;                            /* flags */
   unsigned char  s;                            /* spare */
   FPTR           fptr;                         /* function pointer */
}  TMR;

extern   TMR tmr[];

/* Watchdog defines
*/
#define  _WDT_TIME         _1SEC/10           /* service period 100 ms. */
#define  _WDT_SERVICE      0x1010             /* service the swsr register */
#define  _WDT_SYS_TIMERS   0x0001             /* flag background's OK */
#define  _WDT_ALL_FLAGS    0x0001             /* mask for all flags used */

#define  COMM_PROCESS_TMR   0                  /* process comm. received    */
#define  CONDOR_TMR         1                  /* condor coin mech. timer   */
#define  HDLC_Rto           2                  /* probe timer               */
#define  HDLC_Sto           3                  /* probe timer               */
#define  HDLC_Gto           4                  /* hdlc                      */
#define  PPS_TMR1           5                  /* (PPS) timer 1             */
#define  PPS_TMR2           6                  /* (PPS) timer 2             */
#define  RS485_TMR1         7                  /* Rto RS485 (TRIM) comm timer*/
#define  RS485_TMR2         8                  /* Sto RS485 (TRIM) comm timer*/
#define  RS485_TMR3         9                  /* Gto RS485 (TRIM) comm timer */
#define  J1708_Rto          10                 /* j1708                     */
#define  BILL_MECH_TMR      11                 /* process bill mech. message*/
#define  BILL_TMR1          12                 /* bill accept timer         */
#define  CHOPPER_TMR        13                 /* bill chopper timer        */
#define  BILL_ESCROW_TMR    14                 /* bill escrow timer         */
#define  ISSI_TMR           15                 /* sound timer               */
#define  SCLK_TMR           16                 /* software clock/functions  */
#define  STATUS_CHK_TMR     17                 /* check machine status      */
#define  LOCK_ON_TMR        18                 /* elec. lock on timer       */
#define  SORT_TMR           19                 /* sort bad list timer       */
#define  SRVC_TEST_TMR      20                 /* service testing timer     */
#define  OCU_ONLINE_TMR     21                 /* OCU comm. establised      */
#define  SOUND_DELAY_TMR    22                 /* delay between sound       */
#define  CARD_BIT_TMR       23                 /* card bit timer            */
#define  PCMCIA_TMR         24                 /* PCMCIA card maint. timer  */
#define  PPS_TMR3           25                 /* (PPS) timer 3             */
#define  CARD_TMR           26                 /* card read timer           */
#define  DOOR_OPEN_TMR      27                 /* door /cb fault beep timer */
#define  IDLE_MISC_TMR      28                 /* misc. timer for idle state*/
/*                                               used also to display time */
/*                                               in menu mode              */
#define  DISP_DELAY_TMR     29                 /* delay alpha display timer */
#define  PPSTMR             30                 /* PPS poll                  */
#define  T_PBQ              31                 /* passback queue de-queue  t*/
#define  SELF_TEST_TMR      32                 /* self test timer           */
#define  BURN_IN_TMR        33                 /* burn in timer             */
#define  CBID_BIT_TMR       34                 /* cashbox id process timer  */
#define  CASHBOX_TMR        35                 /* casbox timer for cbid     */
#define  PROBE_DELAY_TMR    36                 /* cashbox/door alarm timer  */
#define  DATA_ENTRY_TMR     37                 /* driver data entry timer   */
#define  T_STATUS_CHK       38                 /* farebox status check      */
#define  AUTO_LOG_TMR       39                 /* auto driver log off timer */
#define  AUTO_DUMP_TMR      40                 /* auto dump timer           */
#define  TESTSET_TMR        41                 /* J1708 DRI req timer       */
#define  MAIN_LOG_TMR       42                 /* maintenance login timer   */
#define  BYPASS_EVT_TMR     43                 /* bypass event timer        */
#define  LOG_ERR_TMR        44                 /* dvr login err display tmr */
#define  BATT_CHK_TMR       45                 /* battery check timer       */
#define  LCD_IDLEMSG        46                 /* LCD IDLE                  */
#define  SC_POLL_TMR        47                 /* poll timer smart card	   */
#define  SC_SESSION_TMR     48                 /* session tmr smar card     */
#define  OCU_TMR1           49   	             /* touch pannel              */
#define  OCU_TMR2           50                 /* touch pannel              */
#define  OCU_TMR3           51                 /* touch pannel              */
#define  TRM_OOS_DLY        52                 /* Trim clearjam offline delay */
#define  SC_RESPONSE_TMR    53                 /* smart card response tmr   */
#define  SC_PRESENT_TMR     54                 /* check smartcard is present*/
#define  BILL_POLL_TMR      55                 /* bill poll timer           */
#define  COIN_CUP_TMR       56                 /* coin cup timer            */
#define  LAMP_TMR           57                 /* escrow lamp timer         */
#define  BILL_ELEVATOR_TMR  58                 /* bill elevator             */
#define  DOOR_BEEP_TMR      59                 /* door to open timer        */
#define  HG_GATE_OPEN_ALARM_TMR  60            /* spare                     */
#define  T_BEEPER           61                 /* spare                     */
#define  LCD_TMR            62                 /* LCD display               */
#define  LCD_BUTTON_TMR     63                 /* LCD buttons               */
#define  DTM_TMR            64                 /* Dallas Touch Memory (DTM) */
#define  BILL_RESP_TMR      65                 /* bill response timeout     */
#define  ALT_KEY_TMR        66                 /* clear Alt key timer       */
#define  TRIM_LSTOCK_TMR    67                 /* trim low stock timer      */
#define  LID_TMR            68                 /* multiplex display timer   */
#define  COINRETURN_TMR     69                 /* coin return action        */
#define  COIN_RTN_TMR       70                 /* coin return action, sensor*/
#define  J1708_Gto          71                 /* j1708                     */
#define  J1708_Sto          72                 /* j1708                     */
#define  BV_RESET_TMR       73                 /* bill validator reset timer*/
#define  AUTO_BV_RST_TMR    74                 /* auto validator reset timer*/
#define  LOW_POWER_TMR      75                 /* trim low power display tmr*/
#define  DUMP_ON_TMR        76                 /* dump sol. on timer        */
#define  BV_OLDRESET_TMR    77                 /* old bv reset timer        */
#define  BILL_DEJAM_TMR     78                 /* bill chopper dejam tmr    */
#define  OPEN_HG_GATE       78                 /* hg gate only, entry tmr    */
#define  FILTER_BR_TMR      79                 /* filter bill reject tmr    */
#define  J1708_Cto          80                 /* j1708 comm timer          */
#define  SC_SAME_TMR        81                 /* same SmartCard back again */
#define  OCU__CTS_DCU_TMR   82
#define  BV07_TX_DATA_TMR   83                 /* BV07 debug display trigger*/
#define  BV07_RX_DATA_TMR   84                 /* BV07 debug display trigger*/
#define  GREEN_LAMP_TMR     85                 /* green lamp timer          */
#define  J1708_POLL_TMR     86                 /* J1708 poll timer          */
#define  J1708_Idle_TMR     87                 /* used to limit time between*/
                                              /* saving lat/long           */
#define  FF_DEFAULT_TMR     88                 /* default to full fare timer*/
#define  COIN_REJ_TMR       89                 /* default to full fare timer*/
#define  TKT_PROCESS_TMR    90                 /* Cubic TRiM card proc timer*/
#define  CTS_RECLASS_TMR    91                 /* Cubic reclassify          */
#define  SHUT_DOWN_TMR      92                 /* delay shut down-alert Trim*/
#define  SMART_WRITE_TMR    93                 /* finished smart write tmr  */
#define  IRMA_POLL_TMR      94                 /* People counter  */
#define  RF_POLL_TMR        95                 /* RF poll timer   */
#define  RF_TMR1            96                 /* RF comm timer   */
#define  RF_TMR2            97                 /* RF comm timer   */
#define  RF_TMR3            98                 /* RF comm timer   */
#define  RF2_POLL_TMR       99                 /* RF poll timer   */
#define  RF2_TMR1          100                 /* RF comm timer   */
#define  RF2_TMR2          101                 /* RF comm timer   */
#define  RF2_TMR3          102                 /* RF comm timer   */
#define  RF_RESET_TMR      103                 /* RF reset        */
#define  WI_PORT_OFF_TMR   104                 /* RF reset        */
#define  J1708IDLE_TMR     105                 /* J1708 idle timeout timer */
#define  DoorAccess_TMR    106                 /* Door Access timer (Nashville CQ */
#define  NREV_SOLENOID_TMR 107                 /* nonrev solenid tmr on regular gate */
#define  HG_EXIT_BUTTON_TMR 107                /* handicap exit buttton timer     */
#define  REJECT_SOL_TMR    108                 /* rej coin solenid tmr      */
#define  HG_GATE_SHUT_TMR  109                 /* hg gate shut timer       */
#define  GATE_MENU_TMR     110                 /* Gate "fareset" switch timers */
#define  MAX_TIMERS        111                 /* MAX_TIMERS value -- this always needs to be at the end of the timer list*/
/* input capture/ output compare defines for different crysal speeds */

/* values for 16.777MH crystal */
#ifdef   cry16777

#define  _1MS           1048
#define  _10MS          10480
#define  _50US          53
#define  _100US         106
#define  _500US         524
#define  _3300US        3460
#define  _4700US        4900
#define  _7MS           7400

#endif

/* values for 20.972MH crystal */
#ifdef   cry20972

#define  _1MS           1310
#define  _10MS          13100
#define  _50US          66
#define  _100US         131
#define  _500US         655
#define  _3300US        4325
#define  _4700US        6160
#define  _7MS           9175

#endif

/* values for 25.166MH crystal */
#ifdef   cry25166

#define  _1MS           1573
#define  _10MS          15730
#define  _100US         157
#define  _500US         786
#define  _3300US        5190
#define  _4700US        7392
#define  _7MS           11010

#endif

/* values for 25.690MH crystal */
#ifdef   cry25690

#define  _1MS           1605
#define  _10MS          16050
#define  _100US         161
#define  _500US         802
#define  _3300US        5298
#define  _4700US        7546
#define  _7MS           11239

#endif

#define  RELOAD         _1MS
#define  LRELOAD        _100US
#define  BRELOAD        _500US
#define  MIN_PULSE      _7MS
#define  BITTIME        _3300US
#define  BITTIME_PLUS   _4700US


/* Prototypes
*/
int   WatchDog          ( int );
int   Initialize_Timers ( void );
FPTR  Init_Tmr_Fn       ( int, FPTR, tmr_t );
int   Load_Tmr          ( int, tmr_t );
int   Stop_Tmr          ( int );
int   Event             ( void );
void  Read_MuxInputs    ( void );

void  Select_Mux_Input  ( void );
void  Read_MuxInput     ( void );

tmr_t Get_remaining_Tmr_time( int );

#endif

