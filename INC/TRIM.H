/******************************************************************************
*  Src File:   trim.h
*  Authored:   06/24/02, aat
*  Function:   Defines, prototypes & macros for comm. (Fbx<->TRIM).
*  Comments:   This header is used for the new "Generic" TRiM.  Intended
*              to be common in all equipment.
*
*  Copyright (c) 2002 GFI All Rights Reserved
*
*  $Log: trim.h,v $
*  Revision 1.15  2002/09/10 18:27:08  gks
*  - Changes by AT to remove unused data structures and add TRIM_BLANKCARD.
*
*  Revision 1.14  2002/09/09 20:04:32  gks
*  - Changes by Aaron for Trim v1.71.
*  - Removed unused magnetic failure code definitions.
*
*  Revision 1.13  2002/08/19 17:24:53  gks
*  - Changes by Aaron to send the Trim I/O status.
*
*  Revision 1.12  2002/08/01 17:54:43  gks
*  - AT added TRIM_SENSOR_STATUS structure.
*
*  Revision 1.11  2002/07/25 15:57:17  gks
*  - Updated the new TRIM_STAT_FLAGS structure to include version and devno.
*
*  Revision 1.10  2002/07/24 15:53:07  gks
*  - Includes Aaron's changes for the Generic Trim plus the following.
*  - Removed the TRIM_TCARD structure.
*  - Added the TOF1_AUTO_STATUS define for automatic status updates.
*
*  =================== Aaron's comments start here ==========================
*  Revision 1.9b  2002/07/23 10:45:00  aat
*  - New commands TRIM_AUTO_ACCEPT_ENB and TRIM_AUTO_ACCEPT_DIS have been
*    added to toggle the "Auto Accept" bit in the TRIM_TSETUP.CONFIG structure,
*    the OpFlags1 entry.
*  - A new command TRIM_ACCEPT has been added.  Also a CARD_ACCEPT flag under
*    the "Process Flags".  Both of these will accept one card that is currently
*    at the entry bezel ( The entry sensor has to be blocked ).
*  - Assigned "Auto Status Updates" to bit 15 of TRIM_TSETUP.CONFIG.OpFlags.
*
*  Revision 1.9a  2002/07/17 23:36:30  aat
*  - Renamed the TRIM_CARD structure for the Cubic data to TRIM_CARD_NEW to
*    remain compatible with previous versions.
*  - Added commands from the TRiM --> farebox to indicate the current status
*    of sensors, motor direction, motor speed, etc.
*  - Added structure for TRiM display and tones.
*  - Changed name of 'TRIM_SETUP.CONFIG.baudrate' to 'TRIM_SETUP.CONFIG.spare1
*    parameter is not necessary since the TRiM is searching for baudrate.
*  =================== Aaron's comments end here ============================
*
*  Revision 1.9  2002/07/11 16:34:13  tgh
*  - TRiM encode/read via CHANNELs and status flag(s) update.
*
*  Revision 1.8  2002/07/10 20:43:45  gks
*  - Changed TRIM_CARD structure to use Cubic data structure.
*  - Converted the t2_dualPkt, t3_dualPkt fields in TRIM_TCARD to t2_flags,
*    t3_flags to indicate whether to add sentinels, LRC in addition to dual
*    packet.
*  - Added extern prototype in header for TrimMsg( int msgid, char *data,
*    int size ).
*  - Changed x,y in the TRIM_PRINT structure to col, row.
*
*  Revision 1.7  2002/07/09 20:10:03  gks
*  - Changed x,y from word to byte in the trim print structure.
*
*  Revision 1.6  2002/07/09 18:47:05  gks
*  - Updates from Tony to make the file work with Intel architecture.
*  - Added macros to allow testing of the trim status flags.
*
*  Revision 1.5  2002/06/25 00:34:36  tgh
*  - Removed the inclusion of "mag.h".
*  - Added function prototype for "InitializeTRIM()".
*
*  Revision 1.4  2002/06/24 21:22:41  gks
*  - Updates to some of the structures used in magnetics by Aaron.
*
******************************************************************************/

#ifndef  _TRIM_H
#define  _TRIM_H

#ifdef __cplusplus
extern "C" {
#endif

/* System headers
*/
#include <stdio.h>

/* Project headers
*/
#include "gen.h"
#include "rtc.h"
#include "mag.h"
#include "transact.h"

/* Defines
*/
#define  BLOCK_SIZE         4096           /* block transfer buffer size     */
#define  BLK_S               240           /* frame buffer size              */

#define  UI_TS                32

#define  OCU_ID                1
#define  TRIM_ID               2     /* TRiM's "device" identifer            */
#define  GEN_ID                3     /* to be compatable with older firmware */
#define  FBX_ID                4     /* Farebox's NEW "device" identifer     */
#define  TVM_ID                5     /* TVM's "device" identifer             */
#define  PEM_ID                6     /* PEM's "device" identifer             */
#define  SC_ID                 7     /* SmartCard's "device" identifier      */

   /* block types (to identify block transfer data types) */
#define  DBS_BLK               0x01  /* Data Base                            */
#define  ACS_BLK               0x02  /* ACS Parameter Block                  */
#define  BLST_BLK              0x03  /* Bad List                             */

#define  T_OVRRTE              0x01  /* 1=override route                     */
#define  T_OVRTM               0x02  /* 2=override time                      */
#define  T_OVRDIR              0x04  /* 3=override direction                 */
#define  T_OVRPB               0x08  /* 4=override passback                  */

   /* command types */
#define  TRIMREQFS             0x01  /* req fare structure   TRIM->FBX->TRIM */
#define  TRIMREQDATE           0x02  /* request date & time  TRIM->FBX->TRIM */
#define  TRIMDATE              0x03  /* date & time                          */
#define  TRIM_READ             0x04  /* read card .v1.04                     */
#define  TRIMCLRJAM            0x05  /* clear jam                  TRIM->FBX */
#define  TRIM_RESTORE          0x06  /* restore card .v1.04        FBX->TRIM */
#define  TRIMDISPMSG           0x07  /* display message            FBX->TRIM */
#define  TRIMREQSTATUS         0x08  /* request FULL status       TRIM<->FBX */
#define  TRIMSTATUS            0x09  /* response to request       TRIM<->FBX */
#define  TRIMISSTRNSFR         0x0a  /* issue tranfer              FBX->TRIM */
#define  TRIMCARD              0x0b  /* card read                  FBX->TRIM */
#define  TRIMACCEPT_C          0x0c  /* accept & capture           FBX->TRIM */
#define  TRIMACCEPT_R          0x0d  /* accept & return            FBX->TRIM */
#define  TRIMREJECT_C          0x0e  /* reject & capture           FBX->TRIM */
#define  TRIMREJECT_R          0x0f  /* reject & return            FBX->TRIM */
#define  TRIMPROBING           0x10  /* currently probing                    */
#define  TRIMCOMPLETE          0x11  /* probing complete           FBX->TRIM */
#define  TRIMTNSFROVERRIDE     0x12  /* transfer override          FBX->TRIM */
/*
#define                     0x13
*/
#define  TRIMFBXREC            0x14  /* farebox record                       */
#define  TRIMCARDREMOVED       0x15  /* card just removed          TRIM->FBX */
#define  TRIMCARDPRESENT       0x16  /* card still present         TRIM->FBX */
#define  TRIMCARDREPORT        0x17  /* print data report                    */
#define  TRIMISSUECARD         0x18  /* issue trim card            FBX->TRIM */
#define  TRIMCHANGECARD        0x18  /* print change card          FBX->TRIM */
#define  TRIMRECEIPTCARD       0x19  /* print receipt              FBX->TRIM */
#define  TRIMPRINTERROR        0x1a  /* print error detected       TRIM->FBX */
#define  TRIMCONFIG            0x1b  /* header printing info       FBX->TRIM */
#define  TRIMREQCONFIG         0x1c  /* request header print inf   TRIM->FBX */
#define  TRIMMETROCARD         0x1d  /* trim metro card read       FBX->TRIM */
#define  TRIMADDCARD           0x1e  /* get ready to add value     FBX->TRIM */
#define  TRIMADDVALUE          0x1f  /* add value to card          FBX->TRIM */
#define  TRIMBYPASSON          0x20  /* put trim in bypass         FBX->TRIM */
#define  TRIMBYPASSOFF         0x21  /* remove trim from bypass    FBX->TRIM */
#define  TRIMCANCELXFER        0x22  /* cancel pending transfer    FBX->TRIM */
#define  TRIMQUEXFER           0x23  /* Que issued xfer in pbq     TRIM->FBX */
#define  TRIM_DISP_READ        0x24  /* request peek at card dat   FBX->TRIM */
#define  TRIMOPENLID           0x25  /* release trim lid for xfe   FBX->TRIM */
#define  TRIM_DIAG_REC         0x26  /* received TRiM diag         TRIM->FBX */
#define  TRIM_MEM_CLR          0x27  /* req TRiM to clear mem      FBX->TRIM */
#define  TRIM_BAD_VER          0x28  /* bad verify of card         TRIM->FBX */
#define  TRIMBURNIN            0x29  /* put trim into "burn-in"    FBX->TRIM */
#define  TRIMISSUEDPASS        0x2a  /* issued val/per/ride card   TRIM->FBX */
#define  TRIMREJECT_R_BAD      0x2b  /* reject & return bad list   FBX->TRIM */
#define  TRIMCLRPRINTBUF       0x2c  /* clear print buffer         FBX->TRIM */
#define  TRIMPRINTINFO         0x2d  /* card print information     FBX->TRIM */
#define  TRIMPUTCARD           0x2e  /* send magnetic data         FBX->TRIM */
#define  TRIMCARDPROCFLAGS     0x2f  /* send card process cmds     FBX->TRIM */
#define  TRIMPROCESSCARD       0x30  /* command to process card    FBX->TRIM */
#define  TRIMCARDDATA          0x31  /* magnetic data              TRIM->FBX */
#define  TRIMSETUP             0x32  /* initialize information     FBX->TRIM */
#define  TRIMRWSTATUS          0x33  /* status of mags read        TRIM->FBX */
#define  TRIM_AUTO_ACCEPT_ENB  0x34  /* auto accept card at entry FBX<->TRIM */
#define  TRIM_AUTO_ACCEPT_DIS  0x35  /* don't auto accept card    FBX<->TRIM */
#define  TRIM_ACCEPT           0x36  /* accept card at entry once  FBX->TRIM */
#define  TRIM_DIAGNOSTIC_MODE  0x37  /* place TRiM in diag mode    FBX->TRIM */
#define  TRIM_OCU_TEXT         0x38  /* send message to OCU        TRIM->FBX */
#define  TRIM_UNIT_STATUS      0x39  /* req. unit status      FBX->TRIM->FBX */
#define  TRIM_DEBUG            0x3a  /* trim debug level (1 - 15)  FBX->TRIM */
#define  TRIM_CLEAR_CONVEYOR   0x3b
#define  TRIM_PROCESS_COMPLETE 0x3c

#define  TRIM_OCU_BOX          0x3d
#define  TRIM_OCU_CLR          0x3e
#define  TRIM_EXIT_DIAGS       0x3f
#define  TRIM_TEMPERATURE      0x40  /* trim temperature         */
#define  TRIM_CHANGE_BAUD      0x41   /*command TRIM to change baud to 19200 */
/*#define                        0x42  */
/*#define                        0x43  */
/*#define                        0x44  */

#define  TRIM_MOTOR_STATUS     0x45  /* motor status change        TRIM->FBX */
#define  TRIM_REQSFLAGS        0x46  /* request Sflags            FBX<->TRIM */
#define  TRIM_SFLAGS           0x47  /* resp to TRIM_REQSFLAGS    FBX<->TRIM */
#define  TRIM_BLANKCARD        0x48  /* trim read a blank card     TRIM->FBX */
#define  TRIM_W_VERIFIED       0x49  /* write verify data          TRIM->FBX */
#define  TRIM_W_NOTVERIFIED    0x4a  /* write data not verified    TRIM->FBX */
#define  TRIM_CLRMEM           0x4b  /* clear TRiM's memory        FBX->TRIM */
#define  TRIM_HOST_VERIFY      0x4c  /* host will verify write     FBX->TRIM */
#define  TRIM_HOST_FINAL_V     0x4d  /* host has final say         FBX->TRIM */
#define  TRIM_CLEANING         0x4e  /* prime, next card cleaning  FBX->TRIM */
#define  TRIM_CARD_CAPTURED    0x4f  /* ticket captured            TRIM->FBX */
/*#define                        0x50  */
#define  TRIM_STATCARD         0x51  /* statistics card                      */
#define  TRIM_DISPLAY_MSG      0x52  /* send message(s) to display FBX->TRIM */
#define  TRIM_CARD_LOST        0x53  /* TRiM lost track of card in process   */
#define  TRIM_DIAGNOSTIC_INFO  0x54
#define  TRIMVERIFYDATA        0x55  /* send data back to host for verify    */
#define  TRIM_TVM_CLRJAM       0x56  /* clear trim out bottem for TVM        */
#define  TRIMCLEARDEBUG        0x57  /* tell trim to clear comm debug counts */
#define  TRIM_POWER_DOWN_CMD   0x58  /* trim was told to power down          */
#define  TRIM_SMARTCARD_CMD    0x59
#define  TRIM_READ_ON_UP       0x5a


#define  TRIMBLOCK             0x64  /* data block (block trnsfer)          */
#define  TRIMMLIST             0x79  /* masterlist                          */
#define  TRIMREQMLIST          0x79  /* request masterlist                  */

   /* sci card status flags (used in TRIM_CARD struct below) */
#define  TRIMCARD_INVALID   0x0001
#define  TRIMCARD_OVERRIDE  0x0002
#define  TRIMCARD_EMBTRN    0x0004
#define  TRIMCARD_FIRSTUSE  0x0008
#define  TRIMCARD_NEWFMT    0x0010

/* Added all of the TRiM Status flags.
*/
   /* sci card status flags (used in TRIM_STATUS struct below) */
#define  T_OFFLINE          (ulong)0x00000001  /* comm. offline (don't move)*/
#define  T_JAM              (ulong)0x00000002  /* jam occured               */
#define  T_OUTOFSERVICE     (ulong)0x00000004  /* currently out-of-service  */
#define  T_PROBING          (ulong)0x00000008  /* currently probing         */
#define  T_INVALIDCNF       (ulong)0x00000010  /* invalid configuration     */
#define  T_MLISTPENDING     (ulong)0x00000020  /* waiting for masterlist    */
#define  T_COMPLETEPENDING  (ulong)0x00000040  /* probe just completed      */
#define  T_PRNEXIT_BLK      (ulong)0x00000080  /* print exit sensor jam     */
#define  T_PRNENTRY         (ulong)0x00000100  /* print entry sensor        */
#define  T_PRNEXIT          (ulong)0x00000200  /* print exit sensor         */
#define  T_STOCK_LOW        (ulong)0x00000400  /* stock level low           */
#define  T_WRITE            (ulong)0x00000800  /* write (mag write head)    */
#define  T_CARD_ENTRY       (ulong)0x00001000  /* entry (top)               */
#define  T_STOCK_EMPTY      (ulong)0x00002000  /* stock empty               */
#define  T_INVALIDFS        (ulong)0x00004000  /* invalid fare structure    */
#define  T_PROCESSING       (ulong)0x00008000  /* processing a card         */
#define  T_LOWPOWER         (ulong)0x00010000  /* low power                 */
#define  T_INBYPASS         (ulong)0x00020000  /* trim in bypass            */
#define  T_INBURNIN         (ulong)0x00040000  /* trim in "burn-in" mode    */
#define  T_VERYLOWPOWER     (ulong)0x00080000  /* very long low power       */
#define  T_WATCHDOGLED      (ulong)0x00100000  /* watchdog led enabled      */
#define  T_AUTO_ACCEPT      (ulong)0x00200000  /* auto accept enabled       */
#define  T_DIAGNOSTIC_MODE  (ulong)0x00400000  /* Trim in Dagnostic mode    */
#define  T_AUTO_BYPASS      (ulong)0x00800000  /* automaically go to bypass */

/* To be compatible with old code */

/* USED only in TRiM        (ulong)0x00000001  /* comm. offline (don't move)*/
#define  TRIM_JAM           (ulong)0x00000002  /* jam occured               */
/* USED only in TRiM        (ulong)0x00000004  /* currently out-of-service  */
/* USED only in TRiM        (ulong)0x00000008  /* currently probing         */
/* USED only in TRiM        (ulong)0x00000010  /* invalid configuration     */
/* USED only in TRiM        (ulong)0x00000020  /* waiting for masterlist    */
/* USED only in TRiM        (ulong)0x00000040  /* probe just completed      */
/* USED only in TRiM        (ulong)0x00000080  /*                           */
#define  TRIM_FEED          (ulong)0x00000100  /* print entry sensor        */
#define  TRIM_EXIT          (ulong)0x00000200  /* print exit sensor         */
#define  TRIMSTOCK_LOW      (ulong)0x00000400  /* stock level low           */
#define  TRIM_WRITE         (ulong)0x00000800  /* write (mag write head)    */
#define  TRIMCARD_ENTRY     (ulong)0x00001000  /* entry (top)               */
#define  TRIMSTOCK_EMPTY    (ulong)0x00002000  /* stock empty               */
#define  TRIMINVALIDFS      (ulong)0x00004000  /* invalid fare structure    */
#define  TRIM_PROCESSING    (ulong)0x00008000  /* processing a card         */
#define  TRIMLOWPOWER       (ulong)0x00010000  /* low power                 */
#define  TRIMINBYPASS       (ulong)0x00020000  /* trim in bypass            */
#define  TRIMINBURNIN       (ulong)0x00040000  /* trim in "burn-in" mode    */
#define  TRIMVERYLOWPOWER   (ulong)0x00080000  /* very long low power       */
#define  TRIM_WATCHDOGLED   (ulong)0x00100000  /* wathdog led enabled       */
#define  TRIM_AUTO_ACCEPT   (ulong)0x00200000  /* auto accept enabled       */
#define  TRIM_DIAG_MODE     (ulong)0x00400000  /* Trim in Dagnostic mode    */
#define  TRIM_AUTO_BYPASS   (ulong)0x00800000  /* automaically go to bypass */
#define  TRIM_POWERDOWN     (ulong)0x01000000  /* trim was told to powerdown*/
#define  TRIM_CARDINPROCESS (ulong)0x02000000  /* set card in process flag  */
#define  TRIM_EXTENDED_DIAG (ulong)0x04000000  /* request extend. Trim Diags*/
#define  TRIM_SC_ONLINE     (ulong)0x08000000  /* TRiM SCard Reader OnLine  */
#define  TRIM_ENC_CURENT_RT (ulong)0x10000000  /* encode current route      */
#define  TRIM_SC_PRINTING   (ulong)0x20000000  /* Enb printing on Issued SC's*/

extern   ulong          TrimSflags; /* trim status byte used in TRIM_STATUS */

#define  Set_Tflag( f )          ( TrimSflags |= (f) )
#define  Clr_Tflag( f )          ( TrimSflags &=~(f) )
#define  Tst_Tflag( f )          ( TrimSflags &  (f) )

/* Magnetic card processing command flags
*/
#define  CARD_FEED         (ulong)0x00000001 /* from cassette or top if bypas*/
#define  CARD_VERIFY_ON_UP (ulong)0x00000002 /* read the card on the way up  */
#define  CARD_PRINT        (ulong)0x00000004 /* print info in buffer to card */
#define  CARD_PRINT_STOP   (ulong)0x00000008 /* print and stop at write sensr*/
#define  CARD_POS_2_PRINT  (ulong)0x00000010 /* position card to be printed  */
#define  CARD_WRITE        (ulong)0x00000020 /* write magnetic info to stripe*/
#define  CARD_WRITE_STOP   (ulong)0x00000040 /* write and stop (don't eject) */
#define  CARD_POS_2_WRITE  (ulong)0x00000080 /* position card to be encoded  */
#define  CARD_READ_STOP    (ulong)0x00000100 /* read card and stop           */
#define  CARD_EJECT        (ulong)0x00000200 /* eject card out entry bezel   */
#define  CARD_EJECTSHORT   (ulong)0x00000400 /* send card to top / re-accept */
#define  CARD_CAPTURE      (ulong)0x00000800 /* send card out bottom of trim */
#define  CARD_ACCEPT       (ulong)0x00001000 /* accept card at entry         */
#define  CARD_SHUFFLE      (ulong)0x00002000 /* shuffle card to align print  */
#define  CARD_HOST_VERIFY  (ulong)0x00004000 /* host will verify write       */
#define  CARD_HOST_FINL_V  (ulong)0x00008000 /* host has final say           */
#define  CARD_ISSUED_DATA  (ulong)0x00010000
#define  CARD_MISREAD      (ulong)0x00020000
#define  CARD_READ_ON_UP   (ulong)0x00040000

extern   ulong  TrimCflags;

/* Trim Debug Commands
*/
#define  TRIM_DEBUG_OFF    0x0000 /* turn off Debug Messeges                 */
#define  TRIM_DEBUG_RAWR   0x0001 /* display RAW data  - received  FBX->TRIM */
#define  TRIM_DEBUG_RAWS   0x0002 /* display RAW data  - sent      TRIM->FBX */
#define  TRIM_DEBUG_GENERL 0x0004 /* display general comments     FBX<->TRIM */
#define  TRIM_DEBUG_CMDR   0x0008 /* display cmds only - received  FBX->TRIM */
#define  TRIM_DEBUG_CMDS   0x0010 /* display cmds only - sent      TRIM->FBX */
#define  TRIM_DEBUG_DATAR  0x0020 /* display data only - received  FBX->TRIM */
#define  TRIM_DEBUG_DATAS  0x0040 /* display data only - sent      TRIM->FBX */
#define  TRIM_DEBUG_CONVEY 0x0080 /* convey sens/motor - sent      TRIM->FBX */

extern   word     TrimDebugLevel;


/* Trim Unit Status
*/
#define  TRIM_MOTOR_STOP   (ulong)0x00000000 /* no activity                  */
#define  TRIM_MOTOR_FWD    (ulong)0x00000001 /* main motor is moving forward */
#define  TRIM_MOTOR_REV    (ulong)0x00000002 /* main motor is moving reverse */
#define  TRIM_MOTOR_FAST   (ulong)0x00000004 /* main motor is moving fast    */
#define  TRIM_MOTOR_SLOW   (ulong)0x00000008 /* main motor is moving slow    */
#define  TRIM_MOTOR_FEED   (ulong)0x00000010 /* feed motor running/stopped   */
#define  TRIM_SENSOR_ENTRY (ulong)0x00000020 /* top entry sensor blocked/clr */
#define  TRIM_SENSOR_WRITE (ulong)0x00000040 /* write sensor blocked/clear   */
#define  TRIM_SENSOR_EXIT  (ulong)0x00000080 /* exit/capture sensor blkd/clr */
#define  TRIM_SENSOR_FEED  (ulong)0x00000100 /* feed sensor blocked/clear    */
#define  TRIM_SENSOR_STOCK (ulong)0x00000200 /* stock level stat = LOW STOCK */
#define  TRIM_PLATTEN      (ulong)0x00000400 /* printer plated enb/dis       */
#define  TRIM_LATCH        (ulong)0x00000800 /* top lid latch enb/dis Non ODY*/
#define  TRIM_JUMPER_1     (ulong)0x00001000 /* jumper 1 active (Diag mode)  */
#define  TRIM_JUMPER_2     (ulong)0x00002000 /* jumper 2 active              */
                                             /*  by self-debuging "printf's  */
                                             /*  w/jmpr1-CYCLE_TST(i/c 1000) */
#define  TRIM_JUMPER_3     (ulong)0x00004000 /* jumper 3 active(various tsts)*/
                                             /*  by self-PRINT_TEST          */
                                             /*  w/jmpr1-CYCLE_TEST(series of*/
                                             /*   tests: iss, wrt, read, ver */
                                             /*  w/jmpr2- Not currently used */
#define  TRIM_LOW_POWER    (ulong)0x00008000 /* low power detected           */
#define  TRIM_CARD_CAPTURE (ulong)0x00010000 /* card captured                */

#define  ALL_TUFLAGS    (unsigned long)\
                        (TRIM_MOTOR_STOP|TRIM_MOTOR_FWD|TRIM_MOTOR_REV|\
                         TRIM_MOTOR_FAST|TRIM_MOTOR_SLOW|TRIM_MOTOR_FEED|\
                         TRIM_SENSOR_ENTRY|TRIM_SENSOR_WRITE|TRIM_SENSOR_EXIT|\
                         TRIM_SENSOR_FEED|TRIM_SENSOR_STOCK|TRIM_PLATTEN|\
                         TRIM_JUMPER_1|TRIM_JUMPER_2|TRIM_JUMPER_3|\
                         TRIM_LOW_POWER|TRIM_CARD_CAPTURE)

extern   unsigned long       TrimUflags;

#define  Set_TUflag( f )          ( TrimUflags |= (f) )
#define  Clr_TUflag( f )          ( TrimUflags &=~(f) )
#define  Tst_TUflag( f )          ( TrimUflags &  (f) )

/* TRiM Diagnostics
*/
#define  STOP_DIAGNOSTIC  0
#define  DIAGNOSTICS      1
#define  CTS_BURNIN       4
#define  OCU_KEY         20

#define  TRM_DEBUG_ON            (TRM_Debug & 1)
#define  TRM_COMMANDS            (TRM_Debug & 2)
#define  TRM_DATA                (TRM_Debug & 4)
#define  TRM_RAW_DATA            (TRM_Debug & 8)

#define  ASCOM_SUBSCRIPTION  1
#define  ASCOM_TOKEN_TITLE   2
#define  ACS_ROLLING         2

#undef   word

#ifdef _MSC_VER
   typedef  unsigned short int   word;
   #pragma  pack(1)
#else
   typedef  unsigned int         word;
#endif

/* END DEFINES */

/* Stuctures
*/
/*** FIX ME ***/
typedef struct                             /* card struct                    */
{
   byte  id;
   byte  cmd;
   word  remval;                           /* remaining value                */
   byte  group;                            /* group                          */
   byte  desig;                            /* designation                    */
   word  flags;                            /* process flags                  */
   char  s[8];                             /* text displayed on TPU          */
   byte  sn[8];                            /* serial number (SERIAL_Fx)      */
   byte  xfer;                             /* transfer group .v1.05          */
   byte  xdesig;                           /* transfer designation .v1.05    */
   word  xferval;                          /* transfer value .v1.05          */
   long  origRoute;                        /* Originating Route on xfer      */
   byte  origDir;                          /* Originating Direction on xfer  */
   byte  xflags;                           /* bit mapped flags for xfer      */
                                           /* 1=override xfer route          */
                                           /* 2=override xfer time           */
                                           /* 3=override xfer direction      */
                                           /* 4-8 spare                      */
   ulong expire;
}  TRIM_CARD;


typedef struct                             /* card struct                    */
{
   word  remval;                           /* remaining value                */
   byte  group;                            /* group                          */
   byte  desig;                            /* designation                    */
   word  flags;                            /* process flags                  */
   char  s[8];                             /* text displayed on TPU          */
   byte  sn[8];                            /* serial number (SERIAL_Fx)      */
   byte  xfer;                             /* transfer group .v1.05          */
   byte  xdesig;                           /* transfer designation .v1.05    */
   word  xferval;                          /* transfer value .v1.05          */
   long  origRoute;                        /* Originating Route on xfer      */
   byte  origDir;                          /* Originating Direction on xfer  */
   byte  xflags;                           /* bit mapped flags for xfer      */
                                           /* 1=override xfer route          */
                                           /* 2=override xfer time           */
                                           /* 3=override xfer direction      */
                                           /* 4-8 spare                      */
}  TRIM_METROCARD;

typedef struct /* date & time */
{
   byte  id;
   byte  cmd;
   byte  month;
   byte  day;
   byte  dow;
   byte  year;
   byte  hours;
   byte  min;
   byte  hsec;
   byte  sec;
}  TRIM_DATE;


typedef struct /* card processing response */
{
   byte  id;
   byte  cmd;
   word  f_value;                          /* fare value                     */
   word  m_value;                          /* magnetic rev. used             */
   byte  key;
   byte  flags;
}  TRIM_RESP;


typedef struct /* database record */
{
   byte  id;
   byte  cmd;
   byte  type;
   byte  seq;
   byte  hour;
   byte  min;
   byte  data[1];
}  TRIM_REC;


typedef struct /* block transfer struct */
{
   byte  id;
   byte  cmd;
   byte  nmbr;
   byte  last;
   byte  bytes;
   byte  type;
   byte  msg[BLK_S];
}  TRIM_BLOCK;


typedef struct /* card struct */
{
   byte  id;
   byte  cmd;
   word  value;                            /* value                          */
   byte  keyttp;                           /* fare type                      */
   byte  type;                             /* xfer type                      */
   word  seqno;
}  TRIM_XFER;

typedef struct /* generic struct */
{
   byte  id;
   byte  cmd;
   byte  data[BLK_S];
}  TRIM_GEN;


typedef struct /* status */
{
   byte  id;
   byte  cmd;
   word  version;
   ulong flags;
   long  devno;
   long  bus;
   long  driver;
   long  route;
   long  run;
   long  trip;
   long  stop;
   byte  fareset;
   byte  direction;
   byte  cut[2];
   byte  city;
   byte  spare;
   ulong cuttm;
   word  agency;
   word  crc16;
}  TRIM_STATUS;


typedef struct /* status */
{
   byte  id;
   byte  cmd;
   word  version;
   ulong flags;
   long  devno;
   long  bus;
   long  driver;
   long  route;
   long  run;
   long  trip;
   long  stop;
   byte  fareset;
   byte  direction;
   byte  cut[2];
   byte  city;
   byte  spare;
   ulong cuttm;
   word  crc16;
}  TRIM_STAT;


typedef struct /* status */
{
   byte  id;
   byte  cmd;
   word  version;
   ulong flags;
   long  devno;
   long  bus;
   long  driver;
   long  route;
   long  run;
   long  trip;
   long  stop;
   byte  fareset;
   byte  direction;
   byte  cut[2];
   byte  city;
   byte  spare;
   ulong cuttm;
   word  agency;
   byte  cut2[2];
   ulong cuttm2;
}  TRIM_NEW_STAT;


typedef struct                     /* trim diag data */
{
   byte  id;
   byte  cmd;
   word  version;                          /* trim version                   */
   long  devno;                            /* trim number                    */
   word  read;                             /* differential reads             */
   word  misread;                          /*              misreads          */
   word  write;                            /*              writes            */
   word  badverify;                        /*              bad verify        */
   word  print;                            /*              prints            */
   word  issue;                            /*              issue             */
   word  jam;                              /*              jams              */
   ulong trim_reads;                       /* cumulative   reads             */
   ulong trim_misread;                     /*              misreads          */
   ulong trim_writes;                      /*              writes            */
   ulong trim_badVerif;                    /*              bad verify        */
   ulong trim_print;                       /*              prints            */
   ulong trim_issue;                       /*              issue             */
   ulong trim_jam;                         /*              jams              */
   ulong trim_cycle;                       /*              cycles            */
   byte  coldstart;
   byte  warmstart;
   word  partnum;                          /* part number                    */
}  TRIM_DIAG;


typedef struct                     /* trim diag data */
{
   byte  powerdown;
   byte  resets;
   byte  spare[2];
}  TRIM_EDIAG;


typedef struct /* configuration */
{
   byte   id;
   byte   cmd;
   ulong  flags;
   byte   pbtime;
   byte   spare1;
   word   agency;
   char   head1[HEADER_SIZE];
   char   head2[HEADER_SIZE];
   char   head3[HEADER_SIZE];
   char   options[20];
}  TRIM_CONFIG;

typedef struct /* configuration */
{
   byte   id;
   byte   cmd;
   ulong  flags;
   byte   pbtime;
   byte   spare1;
   word   agency;
   char   head1[HEADER_SIZE];
   char   head2[HEADER_SIZE];
   char   head3[HEADER_SIZE];
   char   options[20];
   byte   VError_X; /* X position for bus # w/verify error     */
   byte   VError_Y; /* Y position for bus # w/verify error     */
   char   VError_Print[12];
}  TRIM_NEW_CONFIG;


typedef struct /* generic struct */
{
   byte  id;
   byte  cmd;
   long  driver_sn;
   long  revenue;
   word  bill_20;
   word  bill_50;
   word  bill_100;
}  TRIM_REV_REPORT;

typedef struct /* change card struct */
{
   byte  id;
   byte  cmd;
   long  driver_sn;
   word  revenue;
   byte  type;
   byte  desig;
   word  init_val;
}  TRIM_CHANGE_CARD;

typedef struct /* receipt card struct */
{
   byte  id;
   byte  cmd;
   word  revenue;
	byte  sn[CARD_S];
}  TRIM_RECEIPT_CARD;


typedef struct                             /* period, ride, or value issued */
{
   byte  id;
   byte  cmd;
   byte  sn[8];                           /* serial number */
   word  rev;                             /* value placed on card */
}  TRIM_CARD_ISSUED;


typedef struct
{
   byte  id;
   byte  cmd;
   long  driver_sn;
   word  revenue;
   byte  type;
   byte  desig;
   word  init_val;
	byte  sn[CARD_S];
}  TRIM_ISSUE_CARD;


typedef struct                                  /* generic struct */
{
   byte  id;
   byte  cmd;
   long  driver;
   word  revenue;
   byte  group;
   byte  desig;
   word  init_val;
   byte  sn[64];
   byte  type;
   byte  exp_off;
}  TRIM_ISSUED_CARD;


typedef struct /* card struct */
{
  byte  type[8];
  word  start[3];
  word  value;
  byte  xfer[8];
  byte  display[8];
  word  end[3];
  word  agency;
  word  exp_xfer[2];

} TRIM_DISP_CARD;


/*** FIX ME ***/
typedef struct /* card struct */
{
  byte  id;
  byte  cmd;
  byte  type[8];
  word  start[3];
  word  value;
  byte  xfer[8];
  byte  display[8];
  word  end[3];
  word  agency;
  word  exp_xfer[2];
  word  iuagency;
  word  lu_val;
  long  origRoute;                        /* Originating Route on xfer       */
  byte  origDir;                          /* Originating Direction on xfer   */
  byte  group;
  ulong xfer_exp;
  byte  xfer_trip;
  byte  xfer_desig;
  word  xfer_val;
} TRIM_DISPLAY_CARD;


typedef struct /* issued card struct */
{
   byte  sn[MSNS];
   word  revenue;
} TRIM_ISSUE_PASS;


typedef struct
{
   byte  id;
   byte  cmd;
   word  version;
   long  devno;
   word  rval;
   byte  sn[8];
}  TRIM_CARD_PROCESSED;


/*****************************************************************
**
**
** New structures for "HOST" controlled TRiM
**
**
*****************************************************************/


typedef  struct
{
   byte  id;
   byte  cmd;
   byte  t2_flags;       /* Track 2 flags for dual packet, sentinels, LRC    */
   byte  t2_size;        /* Track 2 data packet size                         */
   byte  t2_data[50]; /* Track 2 SS Data CRC ES (if TRiM verified packet) */
   byte  t3_flags;       /* Track 3 flags for dual packet, sentinels, LRC    */
   byte  t3_size;        /* Track 3 data packet size                         */
   byte  t3_data[50]; /* Track 3 SS Data CRC ES (if TRiM verified packet) */
   byte  spare[2];
}  TRIM_MAG_DATA;


typedef struct
{
  byte      id;
  byte      cmd;
  byte      tone;
  byte      duration;
  char      s[8];
} TRIM_DISP_SOUND;


typedef struct
{
  byte      id;
  byte      cmd;
  word      col;
  word      row;
  char      s[50];
} TRIM_PRINT;


typedef struct                             /* card command structure */
{
  byte  id;
  byte  cmd;
  byte  device;
  byte  spare0;
  long  devno;
  ulong version;
  ulong flags;
  byte  spare1;
  byte  spare2;
} TRIM_CARD_FLAGS;


typedef struct                             /* card command structure */
{
  byte  id;
  byte  cmd;
  ulong flags;
  byte  spare[2];
} TRIM_PROCESS_FLAGS;


typedef struct
{
  byte    id;
  byte    cmd;
  word    version;
  ulong   flags;
  long    devno;
} TRIM_STAT_FLAGS;


typedef struct
{
   byte    id;
   byte    cmd;
   word    level;
}  TRIM_DEBUG_LEVEL;

typedef struct
{
   byte  id;
   byte  cmd;
   word  version;
   long  devno;
   word  rval;
   byte  sn[8];
}  TRIM_BAD_VERIFY;

typedef struct
{
   byte  id;
   byte  cmd;
   char  Message1[8];
   char  Message2[8];
   char  Message3[8];
}  TRIM_DISPLAY_TEXT;

#ifdef _MSC_VER
   #pragma pack()
#endif

/* Global Data
*/

#pragma sep_on segment S_485
extern  TRIM_DIAG            Trim_Diag;
extern  TRIM_EDIAG           Trim_EDiag;
extern  TRIM_MAG_DATA        TrimData;
extern  TRIM_CARD_FLAGS      TProcFlags;
#pragma sep_off

extern  TRIM_BAD_VERIFY      TrimBadV;
extern  TRIM_DISPLAY_TEXT    Trim_DisplayMsg;

extern byte fd[FDSZ];            /* fixed data buffer    (read)            */
extern byte vd[VDSZ];            /* variable data buffer (read/write)      */
extern byte vf[VDSZ];            /* verify buffer        (read)            */
extern byte cc[CCSZ];            /* verify buffer        (read)            */
extern int   SwipePass;          /* swipe pass fare pending                */
extern int   TRiMBypassChange;
extern int   AutoAcceptDisabled;

extern int   CTS_Normal;
extern int   CTS_BadVer;
extern int   CTS_Misread;
extern int   CTS_Unhandled;

extern int   IssueCondition;
extern int   BadTRiM_FS;
extern int   TrimDataDump;
extern int   TrimCardError;

extern ulong TrimTimer;
extern int   CardIssued;
extern int   ReRead;
extern int   IssueReplacementCard;

/** AAT - For development **/
extern int   DuplicateCard;

/** **/

/* Prototypes
*/
void  init_comm            ( void );
int   TrimDisplay          ( char dsp, char blink, char *fmt, ... );
void  SendBlock            ( int , char*, long );
void  poll_device          ( void );
int   TrimCmd              ( byte, void*, int );
int   TrimPrintf           ( int, int, char *fmt, ... );
int   tprintf              ( int, int, char *fmt, ... );
int   TrimDiag             ( void*, int );
int   SendCmd              ( byte, byte, void*, int );
/*int   TrimEncodeData       ( int, char *fmt, ... );*/
int   RunTrimDiagnostic    ( byte, byte, byte );
void  TrimCardConfigure    ( void );
int   TrimDisplayMsg       ( int );
int   SendTrimSCMsg        ( void );
int   ReqTrimSCInfo        ( void );

#ifdef __cplusplus
}
#endif

#endif /*_TRIM_H*/

