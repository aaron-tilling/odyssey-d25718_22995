/*----------------------------------------------------------------------------\
|  Src File:   wstring.h                                                      |
|  Authored:   04/08/03, tgh                                                  |
|  Function:   Define functions & data for wild card string compare.          |
|  Comments:                                                                  |
|                                                                             |
|  Revision:                                                                  |
|      1.00:   04/08/03, tgh  -  Initial release.                             |
|                                                                             |
|           Copyright (c) 2003       GFI All Rights Reserved                  |
\----------------------------------------------------------------------------*/
#ifndef  _WSTRING_H
#define  _WSTRING_H


/* Prototype(s)
*/
int   wstrcmp           ( char*, char* );
int   wstricmp          ( char*, char* );

#endif /* _WSTRING_H */


