/*----------------------------------------------------------------------------\
|  Src File:   422p.c                                                         |
|  Version :   1.10                                                           |
|  Authored:   04/22/96, tgh                                                  |
|  Function:   Process 422/485 port                                           |
|  Comments:                                                                  |
|                                                                             |
|  Revision:                                                                  |
|  $Log: 422p.c,v $                                                           |
|      1.00:   09/21/96, tgh  -  Initial release.                             |
|      1.01:   08/21/98, sjb  -  process dcu communications.                  |
|      1.02:   09/29/98, sjb  -  add code to handle all key of dcu            |
|      1.03:   10/30/98, sjb  -  add code to multiplx comm to coin mech & dcu |
|      1.05:   01/08/99, sjb  -  modify processing message to/from coin mech  |
|      1.06:   01/18/99, sjb  -  modify processing message to/from coin mech  |
|      1.06:   06/18/99, sjb  -  remove processing message to/from coin mech  |
|      1.09:   03/16/01, sjb  -  remove special sound processing              |
|      1.10:   08/31/04, sjb  -  move Moduel data to xdata                    |
|                                                                             |
|           Copyright (c) 1993-1997  GFI/USPS All Rights Reserved             |
\----------------------------------------------------------------------------*/
/* System headers
*/
#include <stdio.h>
#include <string.h>
#include <time.h>

/* Project headers
*/
#include <gen.h>
#include <rs422.h>
#include <pps.h>
#include <cnf.h>
#include <tlog.h>
#include <timers.h>
#include <status.h>
#include <ability.h>
#include <fbd.h>
#include <sound.h>
#include <display.h>
#include <menu.h>
#include <misc.h>

/* Defines
*/
#define  DUMPMASK       0x00ff                  /* dump button mask */
#define  KEYMASK        0x077f                  /* key mask */
#define  K_DUMP         0x0080                  /* dump button */
#define  K_AUTO         0x0008
#define  K_PROG         0x0004
#define  K_GOTO         0x0002
#define  K_EMER         0x0001
#define  K_MUTE         0x0010
#define  K_BACK         0x0110
#define  K_SKIP         0x0210
#define  K_REPEAT       0x0310
#define  K_CANCEL       0x0410
#define  K_PLAY         0x0040
#define  CR             0x0d
#define  RX_BUF_SZ      256                     /* read buffer size */
#define  TX_BUF_SZ      256                     /* transmit buffer size */
#define  MAXMSG         8                       /* max. queued messages */
#define  NO_DUMP_KEY    0x00ff

   /* Protocol defines */
#define  STX            0x02                    /* start of text */
#define  ETX            0x03                    /* end of text */
#define  EOT            0x04                    /* end of transmission */
#define  ENQ            0x05                    /* enquiry */
#define  ACK            0x06                    /* acknowllege */
#define  NAK            0x15                    /* negative acknowllege */
#define  MAX_RETRIES    3
#define  T1             MSEC( 250 )             /* response time */
#define  T2             MSEC( 500 )             /* message receive time */
#define  KBD_T1         SECONDS( 5 )            /* keyboard abort time */

   /* HDLC defines */
#define  HDLC_SYNC      0xfe
#define  HDLC_RR        0x01


/* Typedefs
*/
typedef  struct
{
   unsigned    scancode;
   unsigned    key;
}  KEYMAP;

typedef  struct
{
   byte        b[TX_BUF_SZ];                    /* message buffer */
   int         sz;                              /* message size */
}  BUF;

typedef  struct
{
   BUF         q[MAXMSG];                       /* message queue */
   int         next;                            /* index to next */
   int         last;                            /* index to last */
   int         cnt;                             /* count of messages queued */
}  QUE;

/* Macros
*/
#define  NextKbdState( fptr )    (KeyState   = (fptr))
#define  NextRx( fptr )          (RxState    = (fptr))
#define  RstNdx()                ( Ndx = 0 )
#define  Rto                     PPS_TMR1      /* response timer */
#define  Gto                     PPS_TMR2      /* general timer (key seq.) */
#define  Sto                     PPS_TMR3      /* session timer inter-char. */
#define  InitTmr( n, f, t )      Init_Tmr_Fn( (n), (f), (t) )
#define  LoadTmr( n, t )         Load_Tmr( (n), (t) )
#define  StopTmr( n )            Stop_Tmr( (n) )

/* Prototypes
*/
int   Nq                ( QUE* );
int   Dq                ( QUE* );
char *strstr            ( const char*, const char* );
byte  CalcLrc           ( byte*, int );
int   Putc              ( byte );
int   Puts              ( byte* );
int   Nputs             ( byte*, int );
int   RS422Putc         ( byte );
int   RS422Puts         ( byte* );
int   RS422Nputs        ( byte*, int );
int   ProcMsg           ( byte* );
int   KbdIdle           ( unsigned );           /* keyboard states */
int   KbdWaitPlay       ( unsigned );
void  KbdAbort          ( void );               /* keyboard timer functions */
int   RxIdle            ( byte );               /* protocol receive states */
int   GetMsgAck         ( byte );
int   GetData           ( byte );
int   GetSync2          ( byte );               /* HDLC protocol states */
int   GetNsNr           ( byte );
int   GetLrc            ( byte );

int   GetCCMsg          ( byte );               /* coin mech message header */
int   GetCCData         ( byte );               /* 3 bytes                  */
int   ProcCCMsg         ( byte* );


void  RtoFnct           ( void );               /* protocol timer functions */
void  GtoFnct           ( void );
void  PPSPoll           ( void );


/* Global data
*/

extern int  RetryCount;
extern int  RetryMax;
extern int  NakCount;

/* Module data
*/

static int     (*RxState)( byte )   = RxIdle;   /* current receive state */
static int     (*KeyState)( word )  = KbdIdle;  /* current receive state */

#option sep_on class xdata

static byte    Lrc;                             /* lrc accumulator */
static int     Retry;                           /* retry counter */
static byte    RxBuf[RX_BUF_SZ];                /* receive buffer */
static int     Ndx         =  0;                /* index into receive buffer */
static QUE     TxQ;                             /* tx message queue */
static int     Currsound;

#option sep_off

static KEYMAP  KeyMapTbl[] =
{
   0x0101,  '*',                                /* regular keys */
   0x0102,  '7',
   0x0104,  '4',
   0x0108,  '1',
   0x0201,  '0',
   0x0202,  '8',
   0x0204,  '5',
   0x0208,  '2',
   0x0301,  '#',
   0x0302,  '9',
   0x0304,  '6',
   0x0308,  '3',
   0x0401,  'D',
   0x0402,  'C',
   0x0404,  'B',
   0x0408,  'A',
   0x0008,  'E',      /*   K_AUTO   (15) */
   0x0004,  'F',      /*   K_PROG   (16) */
   0x0002,  'G',      /*   K_GOTO   (17) */
   0x0001,  'H',      /*   K_EMER   (18) */
   0x0010,  'I',      /*   K_MUTE   (19) */
   0x0110,  'J',      /*   K_BACK   (20) */
   0x0210,  'K',      /*   K_SKIP   (21) */
   0x0310,  'L',      /*   K_REPEAT (22) */
   0x0410,  'M',      /*   K_CANCEL (23) */
   0x0040,  'N',      /*   K_PLAY   (24) */
   0,       0
};

int   InitializePPSProtocol()
{
   memset( (char*)&TxQ, 0, sizeof(TxQ) );
   InitTmr( Rto, RtoFnct, 0 );
   PPSSendFbxCnf();
   Init_Tmr_Fn( PPSTMR, PPSPoll, SECONDS( 1 ) );
   return 0;
}

int   PPSSendFbxCnf()
{
   byte  b[64];
   byte  dir,fare_set;

   dir = FbxCnf.dir;
   if ( dir != 0 )
     --dir;
   fare_set = FbxCnf.fareset+1;
   if ( fare_set == 10 )
     fare_set = 0;

   sprintf( b, "%06lu%06lu%06lu%06lu%06lu%06lu%06lu%c%c",
      FbxCnf.busno, Ml.fbx_v, FbxCnf.driver, FbxCnf.route, FbxCnf.run,
      FbxCnf.trip, FbxCnf.stop, fare_set+'0', dir+'0' );

   PPSMsg( ID_ALL, MT_STATUS, b, strlen(b) );

}

void PPSPoll( void )
{
   PPSMsg( ID_ALL, MT_ONLINE, NULL, 0 );
   OCU_Msg( ID_ALL, MT_ONLINE, NULL, 0 );
   Init_Tmr_Fn( PPSTMR, PPSPoll, SECONDS( 5 ) );
}

/*----------------------------------------------\
|  Function:   ProcessRS422();                  |
|  Purpose :   Process rs422/485 port data.     |
|  Synopsis:   void  ProcessRS422( void );      |
|  Input   :   None.                            |
|  Output  :   None.                            |
|  Comments:   Called via system timer.         |
\----------------------------------------------*/
void  ProcessRS422()
{
   int   ch, n;

   n  = RS422Cnt();
   n %= RX_BUF_SZ;

   while ( n-- )
   {
      ch = RS422Getc();
      if ( Ndx >= RX_BUF_SZ )
      {
         Tlog( "Rxbuf[%d] overrun\n", Ndx );
         IdleState();
      }
      (*RxState)( ch );
   }
}

static
int   Putc( byte c )
{
/*
   Tlog( "TRANSMIT [%c]\n", c );
*/

   return RS422Putc( c );
}

static
int   Puts( s )
byte       *s;
{

/*
   int   i;

   Tlog( "TRANSMIT [" );
   for( i=0; i<strlen(s); i++ )
      if ( s[i] != CR )
         Tlograw( "%c", s[i] );
   Tlograw( "]\n" );
*/
/*
   if ( Tst_Sflag( S_SPARES1 ))
   {
      printf( "TRANSMIT [" );
      for( i=0; i<strlen(s); i++ )
        if ( s[i] != CR )
           printf( "%c", s[i] );
      printf( "]\n" );
   }
*/
   return RS422Puts( s );
}

static
int   Nputs( s, n )
byte        *s;
int             n;
{
/*
   int   i;

   Tlog( "TRANSMIT [" );
   for( i=0; i<n; i++ )
      if ( s[i] != CR )
         Tlograw( "%c", s[i] );
   Tlograw( "]\n" );
*/
/*
   if ( Tst_Sflag( S_SPARES1 ) )
   {
      printf( "TRANSMIT [" );
      for( i=0; i<strlen(s); i++ )
        if ( s[i] != CR )
          printf( "%c", s[i] );
      printf( "]\n" );
   }
*/
   return RS422Nputs( s, n );
}

static
int   IdleState()
{
   NextRx( RxIdle );
   RstNdx();
   return 0;
}

static
int   MapKey( k )
unsigned      k;
{
   int   i, mk = -1;

   switch( k & DUMPMASK )
   {
      case NO_DUMP_KEY:
         mk = 0;
         goto function_end;          /* dump key gone ? */
      break;
      case K_DUMP:                   /* dump key alone ? */
         mk = K_DUMP;
         goto function_end;
      break;
      default:
      break;
   }

   for( i=0; KeyMapTbl[i].key != 0 && mk == -1; i++ )
      if ( KeyMapTbl[i].scancode == (k & KEYMASK) )
         mk = KeyMapTbl[i].key;

   if ( mk != -1 )                              /* match found */
   {
      if ( k & K_DUMP )                         /* if dump was pressed */
         mk |= K_DUMP;                          /* combine with return key */
   }

   function_end:
   return( mk );
}

static
int   KbdIdle( unsigned k )
{
   int   mk = -1;

   if ( (mk=MapKey( k )) >= 0 )
      KeyPush( mk );
   if( mk >= 0x080 )
     Set_Sflag( S_DUMP );
   else
     Clr_Sflag( S_DUMP );

   return mk;
}

static
void  KbdAbort()
{
   PPSMsg( ID_ALL, MT_KEYABORT, NULL, 0 );
   NextKbdState( KbdIdle );
}

static
int   KbdWaitPlay( unsigned k )
{
   int   mk = -1;

   switch( k )
   {
      case  K_DUMP:
         KeyPush( K_DUMP );
         Set_Sflag( S_DUMP );
      case  K_CANCEL:
         StopTmr( Gto );
         NextKbdState( KbdIdle );
         break;
      default:
         if ( (mk=MapKey( k )) >= 0 )
            KeyPush( mk );
         if( mk >= 0x080 )
            Set_Sflag( S_DUMP );
         else
            Clr_Sflag( S_DUMP );
         NextKbdState( KbdIdle );
         break;
   }

   return 0;
}

static
int   ProcKey( s )
byte          *s;
{
   int      E = -1;
   word     k;

   if ( strlen( s ) != 3 )
   {
      Tlog( "invalid key msg size\n" );
      goto function_end;
   }

   k = PhtoL( s, 3 );

   Tlog( "key value = %04x\n", k );

   (*KeyState)( k );

   E = 0;

   function_end:
   return E;
}

static
int   ProcMsg( s )
byte          *s;
{
   int   E = 0;
   int   mt, sz;
   char  src, dst;
   char *data;

   RxBuf[Ndx] = '\0';

   src         =  s[0];                         /* source */
   dst         =  s[1];                         /* destination */
   mt          =  PhtoL( (s+4), 2 );            /* convert message type */
   sz          =  PhtoL( (s+2), 2 );            /* convert data size */
   data        =  (s+6);                        /* point to data */
   *(data+sz)  =  '\0';                         /* null terminate data */

   Tlog( "RECEIVE  [%s]\n", RxBuf );

   if ( dst != ID_ALL && dst != ID_FBX )        /* for me ? */
   {
      goto function_end;
   }

   switch( mt )
   {
      case  MT_KEY:                             /* keyboard input */
         ProcKey( data );
         break;
      case  MT_STATUS:
         PPSSendFbxCnf();
         E = 1;
         break;
      default:
         break;
   }

   function_end:
   return E;
}

/*----------------------------------------------------------------------------\
|                                                                             |
|                P R O T O C O L   T I M E R   F U N C T I O N S              |
|                                                                             |
\----------------------------------------------------------------------------*/
static
void  RtoFnct()
{
   ++RetryCount;
   if ( --Retry > 0 )
      InitTx();
   else
   {
      ++RetryMax;
      Dq( &TxQ );
      if ( TxQ.cnt > 0 )
      {
         Retry = MAX_RETRIES;
         InitTx();
      }
      else
      {
         IdleState();
         RS422Putc( EOT );
      }
   }
}

static
void  StoFnct()
{
   int   i;

   ++NakCount;

   Putc( NAK );
   Tlog( "TIMED OUT RECEIVING MESSAGE [" );
   for( i=0; i<Ndx; i++ )
      Tlograw( "%c", RxBuf[i] );
   Tlograw( "]\n" );
   NextRx( RxIdle );
}

/*----------------------------------------------------------------------------\
|                                                                             |
|                P R O T O C O L   R E C E I V E   S T A T E S                |
|                                                                             |
\----------------------------------------------------------------------------*/
static
int   RxIdle( byte c )
{
   switch( c )
   {
      case  ENQ:
         Putc( ACK );
         break;
      case  STX:
         NextRx( GetData );
         RstNdx();
         Lrc = 0;
         InitTmr( Sto, StoFnct, T2 );
         break;
      case  NAK:
      case  EOT:
         if ( TxQ.cnt ) InitTx();
         break;
      case  HDLC_SYNC:
         if ( Tst_Sflag( S_TESTSET ) )
            NextRx( GetSync2 );
         break;
      default:
         break;
   }
   return 0;
}

static
int   GetSync2( byte c )
{
   if ( c == HDLC_SYNC )
      NextRx( GetNsNr );
   else
      IdleState();
}

static
int   GetNsNr( byte c )
{
   if ( c & 0x01 )
      NextRx( GetLrc );
   else
      IdleState();
}

static
int   GetLrc( byte c )
{
   static   byte  rsp[] = { HDLC_SYNC, HDLC_SYNC, HDLC_RR, HDLC_RR };

   Nputs( rsp, 4 );
   IdleState();
}

static
int   GetMsgAck( byte c )
{
   switch( c )
   {
      case  ACK:
         StopTmr( Rto );
         NextRx( RxIdle );
         Dq( &TxQ );
         if ( TxQ.cnt )InitTx();
         break;
      case  STX:
         StopTmr( Rto );
         Dq( &TxQ );
         NextRx( GetData );
         RstNdx();
         Lrc = 0;
         InitTmr( Sto, StoFnct, T2 );
         break;
      case  NAK:
         Tlog( "NAKed\n" );
         StopTmr( Rto );
         if ( --Retry > 0 )
         {
            InitTx();
         }
         else
         {
            Dq( &TxQ );
            if ( TxQ.cnt > 0 )
            {
               Retry = MAX_RETRIES;
               InitTx();
            }
            else
            {
               IdleState();
               RS422Putc( EOT );
            }
         }
         break;
      case  EOT:
         break;
      default:
         break;
   }
   return 0;
}

static
int   GetData( byte c )
{
   byte  rxlrc;
   int   i;

   Lrc ^= c;
   switch( c )
   {
      case ETX:
         StopTmr( Sto );
         for( i=0, Lrc=0; i<Ndx-2; i++ )
            Lrc ^= RxBuf[i];
         rxlrc = ((RxBuf[Ndx-2]&0x0f) << 4) | (RxBuf[Ndx-1] & 0x0f);
         if ( rxlrc == Lrc )
         {
            if ( ProcMsg( RxBuf ) == 0 )        /* no response */
            {
               if ( TxQ.cnt )
                  InitTx();
               else
               {
                  IdleState();
                  RS422Putc( ACK );
               }
            }
         }
         else
         {
            RS422Putc( NAK );
            LoadTmr( Rto, T1 );
            IdleState();
         }
         break;
      case  STX:
         RstNdx();
         Lrc = 0;
         break;
      default:
         RxBuf[Ndx++] = c;
         break;
   }
   return 0;
}

static
byte  CalcLrc( byte *s, int sz )
{
   int   i;
   byte  acc=0;

   for( i=0; i<sz; i++ )
      acc ^= s[i];

   return acc;
}

int   PPSMsg( byte  dest,
              byte  type,
              byte *data,
              byte  size )
{
   int   E = -1;
   byte *p = TxQ.q[TxQ.next].b, lrc;

   *(p)                 =  STX;                 /* start with STX */
   *(p+1)               =  ID_FBX;              /* source ID */
   *(p+2)               =  dest;                /* destination ID */
   memcpy( (p+3), UcharToPhex( size ), 2 );     /* size of data */
   memcpy( (p+5), UcharToPhex( type ), 2 );     /* message type */
   if ( size > 0 && data != NULL )              /* if data is present */
      memcpy( (p+7), data, size );              /* move data */
   lrc                  =  CalcLrc(p+1, size+6);/* calculate LRC */
   memcpy( p+size+7, UcharToPhex( lrc ), 2 );   /* and move it in buffer */
   *(p+size+9)          =  ETX;                 /* add ETX */
   *(p+size+10)         =  '\0';                /* null terminate to be safe*/
   TxQ.q[TxQ.next].sz   = size+10;              /* data size + overhead */

   Nq( &TxQ );                                  /* queue up message */

   if ( (E=InitTx()) == 0 )
      Retry = MAX_RETRIES;

   return( E );
}

static
int   InitTx()
{
   if ( !tmr_expired(Rto)  ||                   /* already transmitting */
        TxQ.cnt <= 0 )                          /* no messages queued up */
      return -1;

   if ( TxQ.q[TxQ.last].sz <= 0 )               /* no data */
   {
      Dq( &TxQ );
      return -1;
   }

   InitTmr( Rto, RtoFnct, T1 );
/*   DCU_COMM_ON(); */
   NextRx( GetMsgAck );
   Nputs( TxQ.q[TxQ.last].b, TxQ.q[TxQ.last].sz );

   return 0;
}

static
int   BumpQndx( p )
int            *p;
{
   if ( *p < MAXMSG-1 )
      (*p)++;
   else
      *p = 0;

   return 0;
}

static
int   Nq( p )
QUE      *p;
{
   BumpQndx( &p->next );

   if ( p->cnt < MAXMSG )                       /* bump message count */
      p->cnt++;
   else
      BumpQndx( &p->last );

   return 0;
}

static
int   Dq( p )
QUE      *p;
{
   p->q[p->last].sz = 0;                        /* set the size to zero */

   if ( p->cnt <= 0 )
   {
      p->cnt = 0;
      goto function_end;
   }

   p->cnt--;
   BumpQndx( &p->last );

   function_end:
   return 0;
}

