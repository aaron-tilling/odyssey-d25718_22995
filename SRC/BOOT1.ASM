*-----------------------------------------------------------------------------+
*  Src File:   boot_v02.asm                                                   |
*  Authored:   08/25/97, tgh                                                  |
*  Function:   Boot block code.                                               |
*  Comments:                                                                  |
*                                                                             |
*  Revision:                                                                  |
*      1.00:   08/25/97, tgh  -  Initial release.                             |
*      1.01:   09/25/98, sjb  -  modified setup for reprogramming fpga.       |
*                                                                             |
*                                                                             |
*           Copyright (c) 1993-1997  GFI All Rights Reserved                  |
*-----------------------------------------------------------------------------+

*-----------------------------------------------------------------------------+
*                       Include Files                                         |
*-----------------------------------------------------------------------------+
        include gen.inc
        include pmain.inc
        include regs.inc


*-----------------------------------------------------------------------------+
*                       Local Equates                                         |
*-----------------------------------------------------------------------------+
        FPGA_SZ:        equ     (FPGA_DataEnd-FPGA_Data)


        xref    FPGA_Data,FPGA_DataEnd
        xref    relocated_vectors,ChkPCMCIA

        section BootCode,,"boot1"


        _fnct   __boot
        
                move.w  #40cfh,mcr             ; module configuration

                move.b  #_16777kz,syncr         ; clock
                move.b  #10111000b,sypcr        ; enable watchdog 1sec period
                move.b  #055h,swsr              ; service watchdog
                move.b  #0aah,swsr

                move.w  #_1sec,pitr             ; pit 1sec period
                move.w  #picr_iv,picr           ; pit irq level - vect number

                move.b  #00110101b,pepar        ; fixme
                move.b  #01111101b,ddre         ; fixme

*-------------------------------+
*  Be sure to set these up in   |
*  "regs_vxx.c" in the function |
*  "InitializePortF()"          |
*-------------------------------+

                move.b  #00000000b,portf        ; port F value
                move.b  #00000000b,ddrf         ; port F data directions

                move.b  #00001100b,pfpar        ; port-f pin assignments
*                                               : irq2=QUART0
*                                               : irq3=QUART1
*                                               : irq5=card
*                                               ; irq7=power fail

                move.w  #cspar0_iv,cspar0       ; pin assignment regs
                move.w  #cspar1_iv,cspar1

                move.w  #csbarbt_iv,csbarbt     ; flash
                move.w  #csbar0_iv,csbar0       ; RAM (lower byte)
                move.w  #csbar1_iv,csbar1       ; RAM (upper byte)
                move.w  #csbar2_iv,csbar2       ; RAM (lower byte)
                move.w  #csbar3_iv,csbar3       ; RAM (upper byte)
                move.w  #csbar4_iv,csbar4       ; QUART
                move.w  #csbar5_iv,csbar5       ; RTC
                move.w  #csbar6_iv,csbar6       ; address line (a19)       
                move.w  #csbar7_iv,csbar7       ; address line (a20)
                move.w  #csbar8_iv,csbar8       ; PC card
                move.w  #csbar9_iv,csbar9       ; PCMCIA
                move.w  #csbar10_iv,csbar10     ; FPGA

                move.w  #csorbt_iv,csorbt       ; flash
                move.w  #csor0_iv,csor0         ; RAM (lower byte)
                move.w  #csor1_iv,csor1         ; RAM (upper byte)
                move.w  #csor2_iv,csor2         ; RAM (lower byte)
                move.w  #csor3_iv,csor3         ; RAM (upper byte)
                move.w  #csor4_iv,csor4         ; QUART
                move.w  #csor5_iv,csor5         ; RTC
                move.w  #csor6_iv,csor6         ; address line                
                move.w  #csor7_iv,csor7         ; address line (a20)    
                move.w  #csor8_iv,csor8         ; PC card
                move.w  #csor9_iv,csor9         ; PCMCIA
                move.w  #csor10_iv,csor10       ; FPGA

                move.b  #tpscale_16,tmsk2       ; WO set IC/OC timer prescale

                move.l  #relocated_vectors,d0   ; move VBR to vector table
                movec   d0,VBR


*-----------------------------------------------------------------------------+
*       Initialize FPGA via the SPI port                                      |
*-----------------------------------------------------------------------------+

                move.b  #00100000b,qpdr
                move.b  #00100000b,qddr 

                move.l  #000000080h,d0          ; FPGA stabilization
                dbra    d0,*

                btst.b  #.bit7,porte            ; reprogram flash ?
                beq.s   fpga_init               ; branch if yes, already hosed

                bclr.b  #.bit5,qpdr             ; clear program bit of fpga
                move.b  #10,d0
                dbra    d0,*

        fpga_reinit:
                btst.b  #.bit7,porte            ; reprogram flash ?
                bne.s   fpga_reinit             ; branch if yes, already hosed

                bset.b  #5,qpdr                 ; set program bit of fpga
                move.b  #10,d0
                dbra    d0,*

        fpga_notready:
                btst.b  #4,qpdr                 ; ready to program flag ?
                beq.b   fpga_notready           ; branch if not yet 

                move.b  #10,d0
                dbra    d0,*
                                     
        fpga_init:
                move.l #FPGA_Data,a0           ; a0 -> FPGA data
                move.l #FPGA_DataEnd,a1        ; a1 -> end of FPGA data

        fpga_next:

                move.b  (a0),0700002h          ; move next word into FPGA0

        fpga_w1:btst.b  #4,portf                ; byte finished ?
                beq.b   fpga_w1

                adda.l  #1,a0                   ; increment address
                cmpa.l  a0,a1                   ; test if more to send
                bne.l   fpga_next

                btst.b  #.bit7,porte            ; repeat if needed
                bne.s   continue

                reset

                jmp     __boot

        continue:

                jsr       ChkPCMCIA             ; check for pc card        

                movea.l   208004h,a6            ; pointer to "__main()"
                jmp       (a6)

                end



