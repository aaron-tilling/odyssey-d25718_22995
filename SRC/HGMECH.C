/**************************************************************************
*
*                                  FILE
*
*  /HgMchv01.c
*
***************************************************************************
*
*                               DESCRIPTION
*
*     This file contains the routines that handle the elderly/handicap
*     gate door. It enables entries and monitors barrier arm position.
*
***************************************************************************
*
*                         VERSION CONTROL HISTORY
*
*  Revision:
*      1.00:   11/29/2000, jks  -  Initial release -- taken from hgmech.src
*
***************************************************************************
*
*                            COPYRIGHT NOTICE
*
*        Copyright (c) 2000 - 2001 GFI Genfare All Rights Reserved
*
**************************************************************************/

/* System headers
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/* Project headers
*/
#include "gen.h"
#include "timers.h"
#include "ability.h"
#include "gatestat.h"
#include "sound.h"
#include "process.h"
#include "status.h"
#include "lcddisp.h"
#include "port.h"
#include "fbd.h"

/* Local variables */
static char  gate_released;
static char  barrier_opened;
static char  barrier_closed;
static char  detected_open_barrier_arm;
static byte  hg_exit_button_request_pending;
static byte  hg_exit_button_handled;

static byte  shut_gate_timer_running;

/* Global data
*/
extern int   Display_On;    /** Added 01/06/09 **/
extern char  already_started_auto_dump_timer;

/* Local macros
*/

#define hg_exit_button ( Tst_Gflag( G_HD60_SW ))
#define GATE_OPENED    ( Tst_Gflag( G_HDHOME_SW ))

/* current hardware has solenoids switched */

#define OPEN_GATE()\
  gate_released = TRUE;\
  LampOff( ENTRY_SOLENOID );\
  LampOff( NON_REVERSING_SOLENOID );

#define CLOSE_GATE()\
  if( gate_released )\
  {\
    stop_gate_open_alarm();\
    gate_released = FALSE;\
  }\
  barrier_opened = FALSE;

/* Local function prototypes
*/
static void shut_gate                 ( void );
static void shut_gate_delay           ( void );
static void check_for_pending_exit    ( void );
static void close_hg_gate             ( void );
static void entry                     ( void );
static void door_open_switch_detected ( void );
static void gate_open_alarm_loop      ( void );
static void stop_gate_open_alarm      ( void );

/**************************************************************************
*
*  Function:                      initialize_hg_head_mechanism
*
***************************************************************************
*
*  Description:
*    This module
*
*  Design Notes:
*
**************************************************************************/
void Initialize_HG_Head_Mechanism( void )
{
  gate_released = FALSE;
  barrier_opened = FALSE;
  barrier_closed = FALSE;
  detected_open_barrier_arm = FALSE;
  hg_exit_button_request_pending = FALSE;
  hg_exit_button_handled = FALSE;
  shut_gate_timer_running = FALSE;
  Display_On = FALSE; /** Added 01/06/09 **/
  if ( !GATE_OPENED )
  {
    barrier_opened = TRUE;
    LampOn( ENTRY_SOLENOID );
  }
}


/**************************************************************************
*
*  Function:                handle_handicap_gate_mechanism
*
***************************************************************************
*
*  Description:
*    This module
*
*  Design Notes:
*
**************************************************************************/
void Handle_Handicap_Gate_Mechanism( void )
{

  if ( GateMode == GateDiagMode && GateSubMenu == GateTripod )
  {
     return;
  }

  if( GATE_OPENED )
  {
    door_open_switch_detected();
    Stop_Tmr( HG_GATE_SHUT_TMR );
    shut_gate_timer_running = FALSE;
  }
  else
  {
    if( barrier_opened )
    {
      shut_gate();
    }
    else
    {
      Stop_Tmr( HG_GATE_SHUT_TMR );
      shut_gate_timer_running = FALSE;

      if ( !Pending_Fares_Counter && Tst_Gflag( G_FREEW_SW ))
        ++Pending_Fares_Counter;

      if( Pending_Fares_Counter )
      {
        if ( Tst_Pflag( P_PROCESSING|P_CARDPRESENT ) && !gate_released )
        {
          if( !already_started_auto_dump_timer )
          {
            Init_Tmr_Fn( AUTO_DUMP_TMR, Auto_Dump, SECONDS( 60 ) );
            already_started_auto_dump_timer = TRUE;
            LCDprintf( LCD_C, 4, GateStr(__REMOVEPASS) );
            LCDIdleMsg( SECONDS(60) );
          }
          CLOSE_GATE();
        }
        else
        {
          if ( !Display_On )
          {
            strcpy( GateMsgs.GATE_Message, GateStr(__THANKYOU_ENT) );
            strcpy( GateMsgs.GATE_Message2, GateStr(__ENJOYRIDE) );
            GateMessages();

/*        EntryMessage();*/
            ++Display_On;
          }
          if( !gate_released )
          {
             OPEN_GATE();
             Init_Tmr_Fn( AUTO_DUMP_TMR, Auto_Dump, SECONDS( 60 ) );
             already_started_auto_dump_timer = TRUE;
          }
        }
      }
      else
      {
        check_for_pending_exit();
      }
    }
  }
}


/**************************************************************************
*
*  Function:                           shut_gate
*
***************************************************************************
*
*  Description:
*   This module
*
*  Design Notes:
*
**************************************************************************/
void shut_gate( void )
{
  if( barrier_closed )
  {
    shut_gate_timer_running = FALSE;
    detected_open_barrier_arm = FALSE;
    hg_gate_needs_to_be_closed = FALSE;
    stop_gate_open_alarm();
    if( hg_exit_button_request_pending )
    {
      if( !hg_exit_button )
      {
        hg_exit_button_request_pending = FALSE;
      }
/*      INCREMENT_3_BYTE_VAR( tally_entry.s.exit_count );
      master_list_changed = TRUE;
      quarter_hour_event_storage.i.local_exits++;
*/
      ++GateExits;
      if( !GateMode  )
      {
        Stop_Tmr( AUTO_DUMP_TMR );
/*      Stop_Tmr( DISPLAY_ROTATE_TMR );
        continue_displaying_card_msg = FALSE;
        display_rotating_message = FALSE;
*/

      }
      CLOSE_GATE();
      LCDIdleMsg( MSEC(50) );
      Display_On = FALSE;;
      LampOff( ENTRY_SOLENOID );
    }
    else
    {
      entry();
    }
  }
  else
  {
    if( !shut_gate_timer_running )
    {
      Init_Tmr_Fn( HG_GATE_SHUT_TMR, shut_gate_delay, MSEC( 300 ) );
      shut_gate_timer_running = TRUE;
    }
    LampOn( NON_REVERSING_SOLENOID );
  }
}


/**************************************************************************
*
*  Function:                        shut_gate_delay
*
***************************************************************************
*
*  Description:
*    This module
*
*  Design Notes:
*
**************************************************************************/
void shut_gate_delay ( void )
{
  barrier_closed = TRUE;
  shut_gate_timer_running = FALSE;
}


/**************************************************************************
*
*  Function:                    check_for_pending_exit
*
***************************************************************************
*
*  Description:
*    This module
*
*  Design Notes:
*
**************************************************************************/
void check_for_pending_exit ( void )
{
  if( hg_exit_button )
  {
    if( !hg_exit_button_handled )
    {
      OPEN_GATE();
      Init_Tmr_Fn( HG_EXIT_BUTTON_TMR, close_hg_gate, SECONDS( 20 ) );
      hg_exit_button_request_pending = TRUE;
      hg_exit_button_handled = TRUE;
    }
  }
  else
  {
    hg_exit_button_handled = FALSE;
    if( !hg_exit_button_request_pending )
    {
      Stop_Tmr( HG_EXIT_BUTTON_TMR );
      close_hg_gate();
      LampOn( NON_REVERSING_SOLENOID );
    }
  }
}


/**************************************************************************
*
*  Function:                        close_hg_gate
*
***************************************************************************
*
*  Description:
*    This module
*
*  Design Notes:
*
**************************************************************************/
void close_hg_gate ( void )
{
  hg_exit_button_request_pending = FALSE;
  Display_On = FALSE;;
  CLOSE_GATE();
  if ( ( GetLamps() & NON_REVERSING_SOLENOID ) == 0 )
    LampOn( ENTRY_SOLENOID );
  else
    LampOff( ENTRY_SOLENOID );
}


/**************************************************************************
*
*  Function:                             entry
*
***************************************************************************
*
*  Description:
*    This module
*
*  Design Notes:
*
**************************************************************************/
void entry( void )
{
  if( Pending_Fares_Counter )
  {

/* fixme *****
    Pending_Fares_Counter--;
*******/
/*    INCREMENT_3_BYTE_VAR( tally_entry.s.entry_count );
    master_list_changed = TRUE;
    quarter_hour_event_storage.i.entries++;
*/
    ++GateEntries;
    if( Pending_Fares_Counter )   /* if additional fare(s) pending */
    {
      if( Tst_Gflag( G_FREEW_SW ))
      {
        Pending_Fares_Counter = 1;
      }
      else
      {
        Pending_Fares_Counter = 0;
      }
/*    kick_start_message_display(THANKS_DISPLAY_MSG_6);
*/
/*
      Init_Tmr_Fn( AUTO_DUMP_TMR, Auto_Dump, AUTO_DUMP_TIME_DELAY );
      Init_Tmr_Fn( EXIT_THROAT_BLOCKED_TMR,
                   handle_exit_throat_blocked_timed_out,
                   EXIT_THROAT_BLOCKED_TIME_DELAY );
*/
      Stop_Tmr( AUTO_DUMP_TMR );
    }
    else
    {
      Stop_Tmr( AUTO_DUMP_TMR );
      if( !GateMode  )
      {
/*        Stop_Tmr( DISPLAY_ROTATE_TMR );
        continue_displaying_card_msg = FALSE;
        display_rotating_message = FALSE;
*/
      }
    }
  }
  else
  {
/*    INCREMENT_3_BYTE_VAR( tally_entry.s.barrier_alarm_cond );
    master_list_changed = TRUE;
*/
    Stop_Tmr( AUTO_DUMP_TMR );
    if( !GateMode  )
    {
/*    Stop_Tmr( DISPLAY_ROTATE_TMR );
      continue_displaying_card_msg = FALSE;
      display_rotating_message = FALSE;
*/
    }
  }

  CLOSE_GATE();
  if( !GateMode && !Pending_Fares_Counter )
    LCDIdleMsg( MSEC(50) );

  if( Pending_Fares_Counter )   /* need to check it here, because when */
                                /*   CLOSE_GATE calls stop_gate_open_alarm */
                                /*   it wipes out all beeps */
  { /* if additional fare(s) pending */
    BeepOnce( 1 );
  }
}


/**************************************************************************
*
*  Function:                   door_open_switch_detected
*
***************************************************************************
*
*  Description:
*    This module
*
*  Design Notes:
*
**************************************************************************/
void door_open_switch_detected( void )
{
  barrier_opened = TRUE;
  barrier_closed = FALSE;
  Stop_Tmr( HG_EXIT_BUTTON_TMR );
  LampOn( ENTRY_SOLENOID );
  LampOff( NON_REVERSING_SOLENOID );
  if( !detected_open_barrier_arm )
  {
    if( gate_released )
    {
      Init_Tmr_Fn( HG_GATE_OPEN_ALARM_TMR,
                   gate_open_alarm_loop, SECONDS( 20 ) );
    }
    else
    {
      Init_Tmr_Fn( HG_GATE_OPEN_ALARM_TMR,
                   gate_open_alarm_loop, SECONDS( 2 ) );
    }
    detected_open_barrier_arm = TRUE;
  }
}


/**************************************************************************
*
*  Function:                     gate_open_alarm_loop
*
***************************************************************************
*
*  Description:
*    This module
*
*  Design Notes:
*
**************************************************************************/
void gate_open_alarm_loop ( void )
{
  if ( !GateMode && !hg_gate_needs_to_be_closed )
  {
    LCDprintf( LCD_C, 4, GateStr(__CLOSEGATE) );
    LCDIdleMsg( SECONDS(60) );
  }
  hg_gate_needs_to_be_closed = TRUE;

  if ( !GateMode  )
    BeepOnce( 1 );

  Load_Tmr( HG_GATE_OPEN_ALARM_TMR, MSEC( 300 ) );
}

/**************************************************************************
*
*  Function:                     stop_gate_open_alarm
*
***************************************************************************
*
*  Description:
*    This module
*
*  Design Notes:
*
**************************************************************************/
void stop_gate_open_alarm ( void )
{
  Stop_Tmr( HG_GATE_OPEN_ALARM_TMR );
}
