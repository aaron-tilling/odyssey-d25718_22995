/*----------------------------------------------------------------------------\
|  Src File:   Pbqp.c                                                         |
|  Version :   3.06                                                           |
|  Authored:   12/13/93, cj, rz                                               |
|  Function:   Passback queue processing.                                     |
|  Comments:                                                                  |
|                                                                             |
|  Revision:                                                                  |
|  $Log: pbqp.c,v $                                                           |
|      1.00:   11/25/94, tgh  -  changed header and filename.                 |
|                             -  Removed configuration block references.      |
|      2.00:   10/10/95, tgh  -  Added Steve's fix for passback queue.        |
|      3.00:   11/20/95, sjb  -  passback time defined in Clist.PRIVATE       |
|      3.01:   07/25/99, sjb  -  modified serial no. for credit cards         |
|      3.06:   09/18/02, sjb  -  queue size was made globe for debugging      |
|                                                                             |
|           Copyright (c) 1993-1995  GFI All Rights Reserved                  |
\----------------------------------------------------------------------------*/
/* System headers
*/
#include <stdio.h>
#include <string.h>
#include <time.h>

/* Project headers
*/
#include "gen.h"
#include "mag.h"
#include "timers.h"
#include "transact.h"
#include "pbq.h"
#include "misc.h"
#include "status.h"
#include "ability.h"
#include "probe.h"

#define QSIZE    100
#define UNUSED   0xff
/* passback time in minutes */
#define DURATION MSEC(600*Clist.PRIVATE.PassBackDuration)
#define M_DURATION MSEC(300)

/* Global data
*/
PBQ   Pbq[PBQSIZE];               /* passback queue memory                 */
int   PbqIdx;
word  pbq_sig;
int   QueuePass;

/*
**  Initialize passback queue
*/
void init_pbq( int flush )
{
  if( PbqIdx >= PBQSIZE )
    PbqIdx = 0;
  if( pbq_sig != PBQ_SIG || flush )
  {
    memset( Pbq, UNUSED, sizeof(Pbq) );
    PbqIdx = 0;
    pbq_sig = PBQ_SIG;
  }
  if ( MyAgency == AGENCY_MARKET || Tst_Sflag( S_SPARES2 ) )
    Init_Tmr_Fn( T_PBQ, dq_pbq, M_DURATION );
  else
    Init_Tmr_Fn( T_PBQ, dq_pbq, DURATION );
}


/*
**  Add a serial nubmer to the passback queue
*/
PBQ   *store_pbq( B_SERIAL *s, void *p, time_t t, byte fs )
{
   int i = PbqIdx;
   Pbq[PbqIdx].PBflag = 1;              /* initialize no image store flag  */
   /********************************************************************
      t currently used to indicate if received serial number from other
      gate so not to send data again
   ********************************************************************/

   if ( s != NULL )
   {
     memcpy( (void*)&Pbq[PbqIdx].Serial, (void*)s, sizeof(B_SERIAL) );
   }
   if ( p != NULL )
      {
        memcpy( (void*)&Pbq[PbqIdx].image, p, sizeof(Pbq[PbqIdx].image) );
        Pbq[PbqIdx].PBflag = 0;         /* clear no image stored bit      */
      }

   Pbq[PbqIdx].time  = t;
   Pbq[PbqIdx].fs    = fs;

   if( ++PbqIdx >= PBQSIZE )
      PbqIdx = 0;

   QueuePass = FALSE;                  /* clear queue pass flag */
   return &Pbq[i];
}


/*
**  Search the passback queue for a serial number
*/
PBQ* check_pbq( B_SERIAL *p )
{
  int i;

  for( i=PbqIdx; i>=0; i-- )
     if( memcmp((void*)&Pbq[i].Serial, (void*)p, sizeof(B_SERIAL) ) == 0 )
        return &Pbq[i];

  for( i=PBQSIZE-1; i>PbqIdx; i-- )
     if( memcmp((void*)&Pbq[i].Serial, (void*)p, sizeof(B_SERIAL) ) == 0 )
        return &Pbq[i];

  return NOT_FOUND;
}


/*
**  Dequeue a serial number from the passback queue
*/
void dq_pbq( void )
{
  memset( (void*)&Pbq[PbqIdx], UNUSED, sizeof(B_SERIAL) );
  if ( MyAgency == AGENCY_MARKET || Tst_Sflag( S_SPARES2 ))
     Load_Tmr( T_PBQ, M_DURATION );
  else
     Load_Tmr( T_PBQ, DURATION );
  if( ++PbqIdx >= PBQSIZE )
    PbqIdx = 0;
}


/*
**  Dequeue a serial number from the passback queue
*/
PBQ* delete_pbq( B_SERIAL *p )
{
  PBQ *q = check_pbq( p );

  if( q != NOT_FOUND )
    memset( (void*)q, UNUSED, sizeof(B_SERIAL) );
  return q;
}

/*
** gate passback que check
*/
void Gate_check_pbq( B_SERIAL *p )
{
   if ( check_pbq( p ) == NOT_FOUND )
     store_pbq( p, NULL, 1, 0 );     /* set t to 1 to indicate not to send */
}

