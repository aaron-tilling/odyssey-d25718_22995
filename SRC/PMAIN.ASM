*-----------------------------------------------------------------------------+
*  Src File:   pmainv03.asm                                                   |
*  Authored:   01/15/96, tgh                                                  |
*  Function:   Startup initialization.                                        |
*  Comments:                                                                  |
*                                                                             |
*  Revision:                                                                  |
*      1.00:   01/15/96, tgh  -  Initial release.                             |
*      2.00:   01/01/97, tgh  -  don't enable interrupts                      |
*      3.00:   08/24/97, tgh  -  moved CPU & FPGA setup into "boot_vxx.asm".  |
*                             -  only C environment code here.                |
*                                                                             |
*           Copyright (c) 1993-1997  GFI All Rights Reserved                  |
*-----------------------------------------------------------------------------+

*-----------------------------------------------------------------------------+
*                       Include Files                                         |
*-----------------------------------------------------------------------------+
        include gen.inc

*-----------------------------------------------------------------------------+
*                       Local Equates                                         |
*-----------------------------------------------------------------------------+


        xref    _main,data

        section   S___main,,"code"


*-----------------------------------------------------------------------------+
*                       Set up C environment                                  |
*-----------------------------------------------------------------------------+
        _fnct   __main

                lea     data,A5                 ; a5 -> data
                suba.l  A6,A6                   ; clear all registers
                suba.l  A4,A4
                suba.l  A3,A3
                suba.l  A2,A2
                suba.l  A1,A1
                suba.l  A0,A0
                clr.l   D7
                clr.l   D6
                clr.l   D5
                clr.l   D4
                clr.l   D3
                clr.l   D2
                clr.l   D1
                clr.l   D0

                jsr     _main                   ; call main()

                jmp     *                       ; should never get here

                end     __main

