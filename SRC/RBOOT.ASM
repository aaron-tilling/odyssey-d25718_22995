*-----------------------------------------------------------------------------+
*  Src File:   ldboot01.asm                                                   |
*  Authored:   08/21/97, tgh                                                  |
*  Function:   reprogram boot block.                                          |
*  Comments:                                                                  |
*                                                                             |
*  Revision:                                                                  |
*      1.00:   06/05/01, sjb  -  Initial release.                             |
*                                                                             |
*           Copyright (c) 2001        GFI/USPS All Rights Reserved            |
*-----------------------------------------------------------------------------+

*-----------------------------------------------------------------------------+
*                       Include Files                                         |
*-----------------------------------------------------------------------------+
        include gen.inc
        include fpga.inc
        include regs.inc
        include quart.inc

*-----------------------------------------------------------------------------+
*                       Local Equates                                         |
*-----------------------------------------------------------------------------+

        PCMCIA:         equ     0800000h        ; PCMCIA base address
        PCMCIA_STAT:    equ     070000dh        ; PCMCIA status inputs
        PCMCIA_PGMID:   equ     055aa0001h      ; card holds a program image
        PCMCIA_EXEID:   equ     055aa0002h      ; card holds executable code

        RamLoadAddress: equ     0010d000h       ; relocate code here
        ModuleAddress:  equ     00000000h       ; flash base address

        FLASH_TYPE:     equ     00004000h       ; addrees to check flash type

*
* Am29F800B equates
*
        FLSH_ADDR1:      equ    0aaah       
        FLSH_ADDR2:      equ    0555h  
        FLSH_ADDR3:      equ    0002h  

        FLSH_UNLK1_DAT: equ     00aah
        FLSH_UNLK2_DAT: equ     0055h

        FLSH_RSET_DAT:  equ     00f0h

        FLSH_AUTO3_DAT: equ     0090h
        FLSH_PRGB3_DAT: equ     00a0h
        FLSH_CERA3_DAT: equ     0080h
        FLSH_SERA3_DAT: equ     0080h

        FLSH_UNLK4_DAT: equ     00aah
        FLSH_UNLK5_DAT: equ     0055h

        FLSH_CERA6_DAT: equ     0010h
        FLSH_SERA6_DAT: equ     0030h

        FLSH_MFID_DATA: equ     0001h
        AMD_flash:      equ     02258h          ;
        AMD_sflash:     equ     022abh          ;

        Intel_flash:    equ     0ffffh          ;
        Intel_device:   equ     04471h

        Mfg_ID:         equ     0011fffch       ;
        Flash_Select:   equ     0011fffeh       ; 

*-----------------------------------------------------------------------------+
*                       Local Macros                                          |
*-----------------------------------------------------------------------------+

*       Service watchdog
        _watchdog:      macro
                        move.b  #055h,swsr      ;; service watchdog
                        move.b  #0aah,swsr
                        endm


*-----------------------------------------------------------------------------+
*                       Data Segment (local data)                             |
*-----------------------------------------------------------------------------+
               section  udata,,"data"           uninitialized data

*-----------------------------------------------------------------------------+
*                       External References                                   |
*-----------------------------------------------------------------------------+

*-----------------------------------------------------------------------------+
*                       Code Segment                                          |
*-----------------------------------------------------------------------------+
                section S_boot_code,,"code"

      xdef  _Boot_Code,_Boot_CodeEnd

*-----------------------------------------------------------------------------+
*                       Flash programming utilities                           |
*                                                                             |
*       NOTE:  Any subroutines placed in this block must be called with       |
*              the "bsr" mnemonic in place of "jsr".                          |
*                                                                             |
*-----------------------------------------------------------------------------+
      _Boot_Code:   equ     *               ; start code

*-----------------------------------------------------------------------------+
*  Function:   BlockEraseAndProgram()                                         |
*  Purpose :                                                                  |
*  Synopsis:   void  BlockEraseAndProgram( void );                            |
*  Input   :                                                                  |
*  Output  :   int - 0(OK) -1(ERR)                                            |
*  Register:                                                                  |
*  Comments:                                                                  |
*-----------------------------------------------------------------------------+
       BlockEraseAndProgram:

                move.l  #0fffh,d1               ; d1 == module length
                movea.l #00802000h,a0           ; a0 -> module data
                bsr     CalcCS
                cmp.l   (a0),d0                 ; compare word
                beq     BootCSOK
                move.b  #03h,led                ; indicate booboo
                bra     ChkWait

        BootCSOK:   
                
                cmp.w   #AMD_flash,FLASH_TYPE    ; AMD or intel?
                beq.s   AMD_stuff
                cmp.w   #AMD_sflash,FLASH_TYPE   ; AMD or intel?
                bne.s   intel_stuff

           AMD_stuff: 
                move.b  #00111101b,ddre         ; setup for AMD 29F400 
                bra     continue     

           intel_stuff:
                bsr     PowerOn                 ; turn flash power on
                btst.b  #.bit1,porte            ; vpp on ?
                bne.s   continue                ; branch if yes
                bsr     PowerOff
                move.b  #09h,led                ; indicate vpp error
                bra     ChkWait

           continue:
                move.b  #011h,led
                movea.l #000000h,a0             ; 16k boot block
                bsr     Erase

*                move.b  #012h,led
*                movea.l #004000h,a0             ; 8k boot block
*                bsr     Erase

                move.b  #015h,led

                move.l  #1fffh,d1               ; d1 == module length
                movea.l #00802000h,a0           ; a0 -> module data
                movea.l #00000h,a1              ; a1 -> destination

                bsr     Write                   ; write data to flash

                cmp.w   #AMD_flash,FLASH_TYPE   ; AMD or intel?
                beq.s   setup_verify
                cmp.w   #AMD_sflash,FLASH_TYPE  ; AMD or intel?
                beq.s   setup_verify
                bsr     PowerOff                    ; turn flash power off

           setup_verify:
                move.l  #0fffh,d1               ; d1 == module length
                movea.l #00802000h,a0           ; a0 -> module data
                movea.l #00000h,a1              ; a1 -> destination

                bsr     Verify                  ; verify data in flash
                cmp.w   #0,d0                   ; if it's the same
                beq     BootProgramOK
                move.b  #0bh,led                ; indicate error
                bra     ChkWait

           BootProgramOK:
                move.b  #0fh,led                ; indicate ok

           ChkWait:                
                _watchdog
                move.b  PCMCIA_STAT,d0          ; check CD1 & CD2
                and.b   #09h,d0
                beq     ChkWait                 ; card present stay in loop
               
        ExitBlock:
                rts

*---------------------------------------------------------------------------+
*   Function:   CalcCS                                                      |
*   Synopsis:   int CS = CalcDS( p, n );                                    |
*   Input   :   a0 - points to data                                         |
*   Input   :   d1 - holds number of bytes (should be a multiple of 4 ).    |
*   Output  :   d0 will hold checksum                                       |
*   Comments:   local function to calculate a simple 32-bit checksum.       |
*---------------------------------------------------------------------------+
        CalcCS:
                move.l  #0,d0                   ; initialize accumulator
                blo.b   CalcCS_exit
                bra.b   CalcCS02

        CalcCS01:
                add.l   (a0)+,d0
        CalcCS02:
                subq.l  #1,d1                   ; update/check  counter
                bpl.b   CalcCS01

        CalcCS_exit:
                rts

*-----------------------------------------------------------------------------+
*  Function:   PowerOn()                                                      |
*  Purpose :   Local function to turn on programming voltage.                 |
*  Synopsis:   void  PowerOn( void );                                         |
*  Input   :   None.                                                          |
*  Output  :   None.                                                          |
*  Register:                                                                  |
*  Comments:                                                                  |
*-----------------------------------------------------------------------------+
        PowerOn:

                movem.l d0-d7/a0-a6,-(sp)       ;save registers used

                move.b  #0d2h,0700009h          ; enable "FLSHPGM" via FPGA
                move.b  #010h,led
                bset.b  #6,porte                ; enable "WP#"
                movem.l (sp)+,d0-d7/a0-a6       ; restore registers used

                rts

*-----------------------------------------------------------------------------+
*  Function:   PowerOff()                                                     |
*  Purpose :   Turn of flash programming voltage.                             |
*  Synopsis:   void  PowerOff( void );                                        |
*  Input   :   None.                                                          |
*  Output  :   None.                                                          |
*  Register:                                                                  |
*  Comments:                                                                  |
*-----------------------------------------------------------------------------+
        PowerOff:

                bclr.b  #6,porte                ; set WP#, unlock boot block of flash
                move.b  #00h,led                ; clear Vpp enable bit in fpga
                move.b  #000h,0700009h

                clr.l   d0                      ; flag OK

                rts


*-----------------------------------------------------------------------------+
*  Function:   Erase()                                                        |
*  Purpose :   Erase a block of flash.                                        |
*  Synopsis:   void  Erase( char* );                                          |
*  Input   :   a0 -> address within block to erase                            |
*  Output  :   None.                                                          |
*  Register:                                                                  |
*  Comments:                                                                  |
*-----------------------------------------------------------------------------+
        Erase:
                cmp.w   #AMD_flash,FLASH_TYPE    ; AMD or intel?
                beq.s   Erase_AMD
                cmp.w   #AMD_sflash,FLASH_TYPE   ; AMD or intel?
                bne.s   intel_erase

          Erase_AMD: 
                move.b  #FLSH_RSET_DAT,(A1)     ; devices reset

                _watchdog                 

                movea.l #FLSH_ADDR1,A1          ; setup command address 
                movea.l #FLSH_ADDR2,A2

                move.b  #FLSH_UNLK1_DAT,(A1)    ; unlock flash
                move.b  #FLSH_UNLK2_DAT,(A2)    ; unlock flash
                move.b  #FLSH_SERA3_DAT,(A1)    ; sector erase command
                move.b  #FLSH_UNLK4_DAT,(A1)    ; unlock flash
                move.b  #FLSH_UNLK5_DAT,(A2)    ; unlock flash
                move.b  #FLSH_SERA6_DAT,(A0)    ; erase chip/sector

        b_e_1:  move.b  (A0),D0
                btst    #7,D0
                bne.b   exit_erase              ; Wait for ready
                btst    #5,D0
                beq.b   b_e_1                   ; time out ?

                move.b  (A0),D0                 ; maybe, check 1 more time
                btst    #7,D0
                bra.s   exit_erase   

        intel_erase:
                move.w  #0020h,(a0)             ; erase command
                move.w  #00d0h,(a0)             ; .

        Erase01:
                _watchdog                       ; wait till ready
                move.w  (a0),d0
                andi.w  #0080h,d0
                beq.s   Erase01

                move.w  #$00ff,(a0)             ; Return to read mode
        exit_erase:
                rts

*-----------------------------------------------------------------------------+
*  Function:   Write()                                                        |
*  Purpose :   Write a block of words into flash.                             |
*  Synopsis:   void  Write( dst, src, n );                                    |
*  Input   :   a0 -> source                                                   |
*              a1 -> destination                                              |
*              d1 -> holds count (number of words)                            |
*  Output  :   None.                                                          |
*  Register:                                                                  |
*  Comments:                                                                  |
*-----------------------------------------------------------------------------+
        Write:
                cmp.w   #AMD_flash,FLASH_TYPE    ; AMD or intel?
                beq.s   Write_AMD
                cmp.w   #AMD_sflash,FLASH_TYPE   ; AMD or intel?
                bne     intel_write

        Write_AMD:
                movea.l #FLSH_ADDR1,A2          ; AMD chip, get ready to write
                movea.l #FLSH_ADDR2,A3
                movea.l #FLSH_ADDR1,A2

        ww_1:   move.w  (A0)+,D2
                move.b  #FLSH_UNLK1_DAT,(A2)    ; unlock flash
                move.b  #FLSH_UNLK2_DAT,(A3)    ; unlock flash
                move.b  #FLSH_PRGB3_DAT,(A2)    ; program word command
                move.w  D2,(A1)+                ; Write data from buffer to device

        ww_2:   _watchdog                       ; wait for status
                move.w  (-2,A1),D0              ; Get status reg
                cmp.w   D0,D2
                beq.b   ww_3                    ; Wait for ready
                btst    #5,d0
                beq.b   ww_2
                move.w  (-2,A1),D0              ; Get status reg
                cmp.w   D0,D2
                bne.s   ww_4                    ; timeout

        ww_3:   subq.l  #1,D1                   ; Decrement buffer byte count
                bne.b   ww_1                    ; Loop till no more data
        ww_4:   bra     exite_write

        intel_write: 
                move.w  #0040h,(a1)             ; write command
                move.w  (a0)+,(a1)+             ; write data to flash

        Write01:
                _watchdog                       ; wait for status
                move.w  #0070h,(-2,a1)          ; read status command
                move.w  (-2,a1),d2
                andi.w  #0080h,d2
                beq.b   Write01

                subq.l  #1,d1                   ; update/check word counter
                bpl.b   intel_write

                move.w  #$00ff,(-2,a1)          ; Return to read mode

        exite_write:
                rts

*-----------------------------------------------------------------------------+
*  Function:   Verify()                                                       |
*  Purpose :   Verify a block of long words in flash.                         |
*  Synopsis:   itn   Verify( dst, src, n );                                   |
*  Input   :   a0 -> source                                                   |
*              a1 -> destination                                              |
*              d1 -> holds count (number of long words)                       |
*  Output  :   None.                                                          |
*  Register:                                                                  |
*  Comments:                                                                  |
*-----------------------------------------------------------------------------+
        Verify:
                _watchdog
                cmp.l   (a0)+,(a1)+             ; compare word
                bne.b   VerifyErr
                subq.l  #1,d1                   ; update/check word counter
                bgt.b   Verify                  ; if greater than zero
                move.l  #000000000h,d0          ; flag OK
                bra.b   Verify_exit

        VerifyErr:
                move.l  #0ffffffffh,d0          ; flag error

        Verify_exit:
                rts

*-----------------------------------------------------------------------------+
*                       End Flash programming utilities                       |
*-----------------------------------------------------------------------------+

      _Boot_CodeEnd:   equ     *               ; end code

