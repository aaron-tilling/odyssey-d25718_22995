/*----------------------------------------------------------------------------\
|  Src File:   sclk.c                                                         |
|  Version :   1.15                                                           |
|  Authored:   01/15/96, tgh                                                  |
|  Function:   Software clock functions and data.                             |
|  Comments:   System timer 16 is used for maintaining software clock ...     |
|              For now this module will just perform time based operations    |
|              such as midnight processing ...                                |
|                                                                             |
|  Revision:                                                                  |
|  $Log: sckl.c,v $                                                           |
|      1.00:   01/15/96, tgh  -  Initial release.                             |
|      1.01:   01/15/96, sjb  -  add midnight & hourly, �&�hour event checks  |
|      1.02:   04/25/97, sjb  -  fix processing years                         |
|      1.03:   05/02/97, sjb  -  fix software clock                           | 
|      1.04:   06/25/97, sjb  -  fix peak time check                          | 
|      1.05:   07/02/97, sjb  -  fix software clock                           | 
|      1.06:   11/26/97, sjb  -  don't store hourly event if nothing happened | 
|      1.07:   04/02/98, sjb  -  update rtc on the hour for DLS               | 
|      1.09:   11/28/00, aat  -  define Ev_HOURLY_EVENT - 50                  |
|      1.10:   12/04/02, sjb  -  check for barrery or rtc errors and default  |
|                                to software clock if bad                     |
|      1.11:   04/14/03, sjb  -  modify checking for hardware clock error     |
|                             -  add clock fault event                        | 
|      1.12:   04/14/03, jks  -  CTS DCU (San Diego) modification:            |
|                                  set/clear status bits                      |
|      1.13:   09/30/04, sjb  -  add code to check and store Periodic event   |
|      1.14:   02/03/06, sjb  -  tweak code that checks Periodic event        |
|      1.15:   07/17/06, sjb  -  tweak code to handle DSL dates received from |
|                                data system                                  |
|           Copyright (c) 1993-2003  GFI All Rights Reserved                  |
\----------------------------------------------------------------------------*/
/* System headers
*/
#include <stdio.h>
#include <string.h>
#include <time.h>

/* Project headers
*/
#include "gen.h"
#include "rtc.h"
#include "sclk.h"
#include "timers.h"
#include "transact.h"
#include "cnf.h"
#include "fbd.h"
#include "ability.h"
#include "status.h"
#include "process.h"

/* Defines
*/
#define days_per_month(mnth,yr) ((mnth!=1) ? __month[mnth] : (yr&03) ? 28 : 29)
#define BEG_DST 96
#define END_DST 303

#pragma separate __month class constant
extern const short __month[12];

/* Typedef(s)
*/

/* Prototype(s)
*/
void  OneSecond( void );
int   first_sunday( int, int, int, int);

/* Gobal data
*/
int         dls_window;
extern int  BadClock;

int         DayLight_Day;
int         STD_Day;

/* Module data
*/
static   struct tm   LastTm;
static   time_t      _T;
static   int         rtc_ok;
 
/*----------------------------------------------\
|  Function:   Initialize_Sclk()                |
|  Purpose :   Initialize software clock.       |
|  Synopsis:   void  Initialize_Sclk( void );   |
|  Input   :   None.                            |
|  Output  :   None.                            |
|  Comments:                                    |
\----------------------------------------------*/
int   Initialize_Sclk()
{
   struct tm   *t,tm;
   time_t      sw_t, hw_t;

   rtc_ok = 0;;

   if ( BadClock == CLOCK_ERROR )
   {
     tm = *gmtime( &_T );
     sw_t  = hw_t =_T;
   }
   else
   {
      t=GetRtc( NULL );
      if( t->tm_year < 70 )
         t->tm_year += 100;
      sw_t = _T;
      t->tm_isdst = 0;    /* clear so mktime() won't adjust for DLS time */
      tm = *t;            /* save local copy of time */ 
      _T   = hw_t = mktime( t );
   }

   tm.tm_yday  = JulianDay( tm.tm_mon, tm.tm_mday, tm.tm_year );
   tm.tm_isdst = DayLightCheck( &tm ); /* check and set DLS flag */

   memcpy( (char*)&LastTm, (char*)&tm, sizeof( struct tm ) );
        
   Init_Tmr_Fn( SCLK_TMR, OneSecond, SECONDS( 1 ) );

   return 0;
}


/*----------------------------------------------\
|  Function:   time()                           |
|  Purpose :   return time_t                    |
|  Synopsis:   time_t time( time_t* );          |
|  Input   :   None.                            |
|  Output  :   None.                            |
|  Comments:                                    |
\----------------------------------------------*/
time_t   time( time_t *t )
{
   if ( t != NULL ) *t = _T;
   return _T;
}

static
void  OneSecond()
{
   struct tm   *t,tm;
   time_t      timer;
       
   if ( BadClock != CLOCK_ERROR )
     t = GetRtc( NULL );
   else
     t = gmtime( &_T );

   if ( LastTm.tm_sec != t->tm_sec )            /* new second ? */
   {
     if ( BadClock != CLOCK_ERROR )
       rtc_ok = 0;;

     LastTm.tm_sec = t->tm_sec;
     if ( LastTm.tm_min != t->tm_min )         /* new minute ? */
     {
       if( t->tm_year < 70 )
         t->tm_year += 100;

       t->tm_yday = JulianDay( t->tm_mon, t->tm_mday, t->tm_year );
       t->tm_isdst = 0;                       /* don't adjust for DLS */ 

       tm = *t;
       timer = mktime( t );                   /* check clocks   */
       *t = tm;

       if ( labs(timer - _T) > 2 )            /* fixme ver 1.12 */
         _T = timer;

       LastTm.tm_min = t->tm_min;

       if ( (t->tm_min % 5) == 0 )           /* mod 5 */
       { 
         if( HOUR_RR && (!FB_TIMED_EV || (FB_TIMED_EV && !EV_TME_CTRL)))
         {
           if( t->tm_min == 30 )
           {
             _T = timer;             /* make sure time in sync  ver 1.12 */
             if( Tst_Aflag( A_HALFHOUR_RR) && MachineOK() && HourlyEv() )
               NewEv( Ev_HOURLY_EVENT, 0 ); /* store on the �hour event */
           }
           else if( t->tm_min == 15 || t->tm_min == 45 )
           {
             _T = timer;             /* make sure time in sync  ver 1.12 */
             if( Tst_Aflag( A_QUARHOUR_RR) && MachineOK() && HourlyEv() )
               NewEv( Ev_HOURLY_EVENT, 0 ); /* store on the �hour event */
           }
         }
         else if ( FB_TIMED_EV && EV_TME_CTRL )
         {
           if ( Ev_Timer )
           {
             ++Ev_Timer_Counter;              /* bump 5 minite timer */
             if ( Ev_Timer_Counter & 1 )
             {
               if ( (t->tm_min % 10) == 0 )
                 ++Ev_Timer_Counter;          /* bump 5 minite timer */
             }
             else if ( t->tm_min % 10 )
               ++Ev_Timer_Counter;            /* bump 5 minite timer */
   
             if ( (Ev_Timer % 12) == 0 )      /* hours */
             {
               if (  t->tm_min == 0 && Ev_Timer_Counter >= Ev_Timer )
               {
                  if( t->tm_hour != 0 && MachineOK() && HourlyEv() )
                     NewEv( Ev_PERIODIC, 0 ); /* store event */
                  Ev_Timer_Counter = 0;
               }
             }
             else if ( (Ev_Timer % 6) == 0 )      /* 1/2 hour */
             {
               if ( ((t->tm_min % 30) == 0) && 
                 Ev_Timer_Counter >= Ev_Timer )
               {
                 if( t->tm_hour != 0 && MachineOK() && HourlyEv() )
                   NewEv( Ev_PERIODIC, 0 ); /* store event */
                 Ev_Timer_Counter = 0;  
               }
             }
             else if ( (Ev_Timer % 4) == 0 )     /* 20 min  */
             {
               if ( ((t->tm_min % 20) == 0) && 
                 Ev_Timer_Counter >= Ev_Timer )
               {
                 if( t->tm_hour != 0 && MachineOK() && HourlyEv() )
                   NewEv( Ev_PERIODIC, 0 ); /* store event */
                 Ev_Timer_Counter = 0;  
               }
             }
             else if ( (Ev_Timer % 3) == 0 )    /* 1/4 hours */ 
             {
               if ( ((t->tm_min % 15) == 0) && 
                  Ev_Timer_Counter >= Ev_Timer )
               {
                 if( t->tm_hour != 0 && MachineOK() && HourlyEv() )
                   NewEv( Ev_PERIODIC, 0 ); /* store event */
                 Ev_Timer_Counter = 0;  
               }
             }
             else if ( (Ev_Timer % 2) == 0 )  /* 10 min */
             {
               if ( ((t->tm_min % 10) == 0) && 
                  Ev_Timer_Counter >= Ev_Timer )
               {
                 if( t->tm_hour != 0 && MachineOK() && HourlyEv() )
                   NewEv( Ev_PERIODIC, 0 ); /* store event */
                 Ev_Timer_Counter = 0;  
               }
             }
             else
             {
               if ( Ev_Timer_Counter >= Ev_Timer )    /* all the rest */
               {
                 if( t->tm_hour != 0 && MachineOK() && HourlyEv() )
                   NewEv( Ev_PERIODIC, 0 ); /* store event */
                 Ev_Timer_Counter = 0;  
               }
             }
           }
           else
             Ev_Timer_Counter = 0;  /* either OOS or nothing happened */ 

           if ( Ev_Timer_Counter < 0 )
             Ev_Timer_Counter = 0; 
         }

         if ( t->tm_min == 0 ) /* new hour ? */
         {  
           if ( DLS_TIME )
           {
             tm.tm_isdst = DayLightCheck( &tm );  /* check for time change */
             if ( LastTm.tm_isdst != tm.tm_isdst ) /* time switch ? */
             {
               if( tm.tm_isdst )                  /* dls time ? */ 
               {
                 ++t->tm_hour;                  /* add hour */
                 tm.tm_hour = t->tm_hour;       /* update local copy */
                 SetRtc( t );                   /* update rtc  */
                 timer += HrsSec;               /* add hour to timer */
                 if( !Tst_Sflag( S_TRIMOFFLINE ) )
                   SendTime();                  /* send new time to TRiM */
                 dls_window = 0;
               }
               else
               {
                 if ( t->tm_hour == 2 )          /* dls to std time */
                 {
                   if (!dls_window )
                   {
                     t->tm_hour = LastTm.tm_hour;    /* go back 1 hour */
                     tm.tm_hour = t->tm_hour;        /* update local copy */
                     SetRtc( t );                    /* update rtc */
                     timer -= HrsSec;                /* minus hour from timer */
                     if( !Tst_Sflag( S_TRIMOFFLINE ) )
                       SendTime();                  /* send new time to TRiM */
                     dls_window = TRUE;           /* set flag, action taken */
                   }
                 } 
                 else if ( t->tm_hour != 1 )
                   dls_window = 0;           /* clear flag  */
               }
             }
             else if ( t->tm_hour != 1 )
               dls_window = 0;           /* clear flag  */
           }
           memcpy( (char*)&LastTm, (char*)&tm, sizeof( struct tm ) );

           _T = timer;             /* make sure time in sync  ver 1.12 */
           if ( tm.tm_hour == 0 )
             NewEv( Ev_MIDNIGHT, 0 );
           else
           {
             if( HOUR_RR && (!FB_TIMED_EV || (FB_TIMED_EV && !EV_TME_CTRL)))
             {
               if ( MachineOK() && HourlyEv() )
                 NewEv( Ev_HOURLY_EVENT, 0 );    /* store on the hour event */
             }
           }
         }
       }
     }
   }
   else
   {
     if ( BadClock != CLOCK_ERROR )
     {
       if ( rtc_ok++ > 5 )
       {
         BadClock = CLOCK_ERROR;
         NewEv( Ev_CLOCK_FAULT, 0 );     /* clock fault detected */
       }
     }  
   }

   Load_Tmr( SCLK_TMR, SECONDS( 1 ) );
}

char  *AscTimeStr( time_t *p )
{
   time_t      t;
   char       *s1, *s2;

   if ( p != (time_t*)0 )
      t = *p;
   else
      time( &t );
   s1 = asctime( gmtime( &t ) );
   if ( (s2=strrchr( s1, '\n' )) ) *s2 = '\0';
   return( s1 );
}

char  *TimeStr( time_t *p )
{
   time_t      t;
   struct tm  *tm;
   static char b[16];

   if ( p != (time_t*)0 )  t = *p;
   else                    time( &t );
   tm = gmtime( &t );
   sprintf( b, "%02d:%02d:%02d", tm->tm_hour , tm->tm_min , tm->tm_sec  );
   return( b );
}

char  *DateStr( time_t *p )
{
   time_t      t;
   struct tm  *tm;
   static char b[16];

   if ( p != (time_t*)0 )  t = *p;
   else                    time( &t );
   tm = gmtime( &t );
   tm->tm_year = tm->tm_year%100;
 
   sprintf( b, "%02d/%02d/%02d", tm->tm_mon+1, tm->tm_mday, tm->tm_year );
   return( b );
}

_IH void PitIrq(void)
{
   _T++;
}

int   IsPeak( p )
struct tm    *p;
{
   int         E = 0, i, am_on, am_off, pm_on, pm_off;
   time_t      t;
   struct tm  *tm;

   if ( p != NULL )
   {
      tm = p;
   }
   else
   {
      time( &t );
      tm = gmtime( &t );
   }

   i = ( tm->tm_hour * 60 ) + tm->tm_min;
   am_on  = ( Dlist.PeakList[0].Hr_start * 60 ) + Dlist.PeakList[0].Mi_start;
   am_off = ( Dlist.PeakList[0].Hr_end * 60 ) + Dlist.PeakList[0].Mi_end;
   pm_on  = ( Dlist.PeakList[1].Hr_start * 60 ) + Dlist.PeakList[1].Mi_start;
   pm_off = ( Dlist.PeakList[1].Hr_end * 60 ) + Dlist.PeakList[1].Mi_end;

   E = ( (i >= am_on && i < am_off) || (i >=pm_on && i < pm_off) );
 
   return E;
}

int DayLightCheck( struct tm *t )
{
/*   printf("start DLS %d end DLS %d  current day %d\n",
     DayLight_Day, STD_Day, t->tm_yday );
*/
   if ( ( DayLight_Day < t->tm_yday || 
        ( DayLight_Day == t->tm_yday && 2 <= t->tm_hour )) &&
        ( t->tm_yday < STD_Day ||  
        ( t->tm_yday == STD_Day && t->tm_hour < 2 )) )
   {
     return TRUE;
   }
   else
     return FALSE;
} 

int  JulianDay( int month, int day, int year )
{
   int   i,yday;

   for (i = 0, yday = day; i < month; i++)
	{
     yday += days_per_month(i,year);
   }
   if ( yday )
     --yday;

   return yday;   
} 

void daylight_time(dyr,dwk,yr,hr)
  int              dyr,dwk,yr,hr;
{
   DayLight_Day = first_sunday(BEG_DST,dyr,dwk,yr);
   STD_Day      = first_sunday(END_DST,dyr,dwk,yr);
}

static
int first_sunday(dyr,tdyr,tdwd,year)
    int dyr,tdyr,tdwd,year;
{
    if (!(year &03) && 58 <= tdyr)
      ++dyr;
    return (dyr - (dyr - tdyr + tdwd + 371) % 7);
}

int   IsHoliday( p )
struct tm       *p;
{
   int         E = 0, i;
   time_t      t;
   struct tm  *tm;

   if ( p != NULL )
   {
      tm = p;
   }
   else
   {
      time( &t );
      tm = gmtime( &t );
   }

   for( i=0; i<HL_SIZE && E==0; i++ )
      E = tm->tm_mon+1 == Dlist.HolidayList[i].Month &&
          tm->tm_mday  == Dlist.HolidayList[i].Day      ;

   return E;
}
