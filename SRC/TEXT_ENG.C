/*----------------------------------------------------------------------------\
|  Src File:   text_eng.c                                                     |
|  Version :   1.02                                                           |
|  Authored:   05/04/2005, aat                                                |
|  Function:   Display messages.                                              |
|  Comments:                                                                  |
|                                                                             |
|  Revision:                                                                  |
|  $Log: text_eng.c,v $                                                       |
|      1.00:   05/03/05, aat  -  Initial release.                             |
|      1.02:   07/26/05, sjb  -  Add probing text                             |
|                                                                             |
|           Copyright (c) 2005 GFI All Rights Reserved                        |
\----------------------------------------------------------------------------*/
/* System headers
*/
#include <stdio.h>

/* Project headers
*/
#include  "cnf.h"
#include  "text.h"
#include  "transact.h"


/* Defines
*/


/* Global data
*/
int   Text_Debug;

/*************************************************
*                                                *
* 8 character words                              *
*                                                *
*************************************************/

char  *TextTokens_1[] = {
         /*  0           1           2           3           4           5    */

/*  0 */ "PASSBACK", "BAD AGCY", "SECURITY", "BAD LIST", "UNLISTED", "NOT USED",
/*  1 */ "INVALID ", "EXPIRED ", "PEAK"    , "OFF PEAK", "WEEKDAY" , "SATURDAY",
/*  2 */ "SUNDAY"  , "HOLIDAY" , "NO RIDES", "NO VALUE", "T AGENCY", "T UNLIST",
/*  3 */ "T EXPIRE",  "RTE/DIR", "BAD XFER", "UNLOCKED", "PC LOW B",  "PC BATT",
/*  4 */ "        ",  "FARESET",   "DRIVER", "OPERATOR",    "ROUTE",      "RUN",
/*  5 */     "TRIP",   "DIRECT",     "CITY",    "BLOCK",  "INBOUND", "OUTBOUND",
/*  6 */    "NORTH",     "WEST",     "EAST",    "SOUTH", "DISABLED", "BAD FARE",
/*  7 */ "NOT ABLE", "RESTORE ", "RESTORED", "NO MONEY", "UNKNOWN ",      "MEM",
/*  8 */ "CHK DIR ", "RESERVED", "SUPERVIS", "TRIMONLY", "TOP LID ", "UNLIMTED",
/*  9 */ " CODE 3 ", "BYPASS",   "PLACE IN", "COIN CUP", "MEMCLEAR", "LOWPOWER",
/* 10 */ "NOVERIFY", "BAD SIZE", "CHECK ID", "BAD FMT ", "ALT KEY ", "BAD BILL",
/* 11 */ "BILL REJ", "BV OOS  ", "BILL OOS", "BILLFEED", "BILL OK ", "RESET BV",
/* 12 */ "CLR FEED", "CHANGE  ", "VAL CARD", "BAD ID  ", "BATT BAD", "BAD CODE",
/* 13 */  "CONTROL", "Bus No  ", "Baudrate",     "Time",     "Date", "Day     ",
/* 14 */      "R/R", "HOLD    ", "PERIOD  ", "TRANSFER", "ACCEPTED", "OVERRIDE",
/* 15 */ "REJECTED", "CARD JAM", "TRIM FLT", "MISFEED ", "MAN FEED", "TRIM OFF",
/* 16 */  "OFFLINE", "TRM BYPS", "AUTOFEED", "MISREAD ", "AGENCY  ", "NO DRIVE",
/* 17 */ "NO ROUTE", "NO RUN  ", "NO BLOCK", "NO TRIP ", "NO DIR  ", "NO CUT  ",
/* 18 */ "NO STOP ", "BAD CUT ", "INVLDCNF", " PLEASE ", " LOG IN ", "CASHBOX ",
/* 19 */ "BATT OK ", "COMPLETE", "XFER ON ", "XFER OFF", "AUTO BYP", "NO AUTO ",
/* 20 */ "PROBE   ", "NO REV  ", "TRIM FST", "TRIM SLW", "T & C   ", "FREE    ",
/* 21 */ "TALLY   ",     "CBID",       "NO", "BV 07/08", "BV 05   ", "FBX CTRL",
/* 22 */ "DS CTRL ", "CAPTURE ", "RETURN  ", "PUT X   ", "PUT EXP ", "NO MARK ",
/* 23 */"ALLOW REV","NO REVERS", "TST BV  ", "TST BILL", "TST WARB", "TST LAMP"
    };

/*
** Second table was necessary because box did not opperate if text referenced
** was more then 143.  Not sure why maybe fix later.
*/
char  *TextTokens_2[] = {
         /*  0           1           2           3           4           5    */

/* 24 */ "TST MEMO", "TST_TRIM", "TST DISP", "TST BVBM", "TST TRBM", "TEST ALL",
/* 25 */  "  PRESS",  " GREEN ", "LOWSTOCK", "NO STOCK", " PERCENT",     "FSET",
/* 26 */ "MAINT # ",  "DEFAULT", "TICKET  ", "CARD    ", "READ    ", "RELEASE ",
/* 27 */    "FARE ", "RECHARGE",     "BILL", "CLASSIFY", "ISSUE X ", "AUTOBILL",
/* 28 */ "TIC OVR ", "$1 OVR  ", "$2 OVR  ", "$5 OVR  ", "$10 OVR ", "$20 OVR ",
/* 29 */ "ISS VALU",     "MENU",   " Next ","Use  \x0e",     "9600",    "19200",
/* 30 */    "38400",    "57600",   "115200",   "230400", "BV Type ","Capt Ctrl",
/* 31 */"Capt/Retrn","MarkXfer","Use  \x0d", "Location",      "N/A", "SelfTest",
/* 32 */ "VER  4  ", "EV Ctrl ",      "OFF", " DS TIME", " FB TIME",  "EV Time",
/* 33 */ "VER4 ON ", "VER 4 ? ", "Door    ",  "Buttons", "Open    ", "Closed  ",
/* 34 */      "Out",      "In ",     "Both",     "Left",    "Right",     "None",
/* 35 */       "ON",      "OTI",     "SONY",      "ACS",     "TRIM",       "OK",
/* 36 */     "BAUD",    "ENTRY",    "BLOCK",    "CLEAR",    "WRITE",    "PRINT",
/* 37 */     "EXIT",    "STOCK","DIRECTION",    "Count",      "Key",      "TTP",
/* 38 */  "Page UP","Page Down",  "No Comm",    "Motor",   "Manual",   "Normal",
/* 39 */ "OCU Card", "PROGRAM ","FARE TBLE", "FRIENDLY", "CONFIG. ", "  BOOT  ",
/* 40 */"OCU FAULT",    "RIDE ",    "RIDES", "OCU PGRM",   "CREDIT",    "Have:",
/* 41 */    "Fare:",    "Bal :",    "Owe :", "J1708FLT", "J1708 ON", "NO J1708",
/* 42 */    "VALUE",    "Paid:", "SERVICE ",  "on Card",      "for", "VIEW LOG",
/* 43 */       "or", "LID OPEN",   "Insert",    "COINS",    "BILLS","MAGNETICS",
/* 44 */"TRANSFERS","SMARTCARD",  "Present",  "NO TRIM","next \176", "\177 YES",
/* 45 */   "ONLINE",     "FAST",     "SLOW","\177 exit", "FULLFARE", "FARESET ",
/* 46 */" PROBE ? ",   "460800",   "921600", "CHK STOK",  "GEMP C2", "OK - COUP",
/* 47 */ "OK - VAL", "3.0 TIKT", "3.5 TIKT", "4.0 TIKT", "4.5 TIKT", "5.0 TIKT"
    };

/*
** Third table was necessary because box did not opperate if text referenced
** was more then 188.  Not sure why maybe fix later.
*/
char  *TextTokens_3[] = {
         /*  0           1           2           3           4           5    */
/* 48 */ "5.5 TIKT",   "SCREEN",  "CANADA",   "U.S.A.", "CLR IRMA", "INT IRMA",
/* 49 */ "OFF SND ", "SOUND ON", "SENSORS",  "SHOW ID",    "PAPER",    "Used:",
/* 50 */ "Remain:" ,     "temp",    "ZONE",     "STOP",      "NOT",  "OTI6000",
/* 51 */ "OTI3000" , "NOTBLANK","Cleaning", "Proof of",  "Payment", "Added:  ",
/* 52 */ "SHORT "  ,  "NO ZONE", "NO CITY", "SWAPZONE", "INV ZONE", "DST ZONE",
/* 53 */ "SELECT"  ,    "Swipe",  "Please",     "Card",  "BAD RTE", "TAP"
    };

char  *TextStr_8( int index )
{
   if( index >= 288 )
   {
     index = index-288;

     if( Text_Debug )
     {
       printf(" TextStr_8-3 Language %d index %d  -", Language, index );
       printf( "%s\n", TextTokens_3[(char)index]);
     }
     return TextTokens_3[(char)index];
   }
   else if( index >= 144 )
   {
     index = index-144;

     if( Text_Debug )
     {
       printf(" TextStr_8-2 - Language %d index %d  -", Language, index );
       printf( "%s\n", TextTokens_2[(char)index]);
     }
     return TextTokens_2[(char)index];
   }
   else
   {
     if( Text_Debug )
     {
       printf(" TextStr_8-1 - Language %d index %d - ", Language, index );
       printf( "%s\n", TextTokens_1[(char)index]);
     }
     return TextTokens_1[(char)index];
   }
}



/*************************************************
*                                                *
* 10 - 14 character words                        *
*                                                *
*************************************************/


char  *TextTokens[] = {
         /*    1                 2                3                 4     */

/*  0 */      "        ",      "          ",  "            ",  "               ",
/*  1 */  "COIN BYPASS ",    "  BILL OOS  ",  " BILL FEED  ",     "TRIM OFFLINE",
/*  2 */  "TRIM BYPASS ",    " LOW STOCK  ",  "  NO STOCK  ",     "TOP LID OPEN",
/*  3 */  " SC OFFLINE ",    " DOOR OPEN  ","TOP LID CLOSED",     " NO CASHBOX ",
/*  4 */  " LOGGED OFF ",    "LOGIN SCREEN"," \x0f OR #  TO ",    "STEP THROUGH",
/*  5 */ "WHEN FINISHED",    "  VALIDATE  ",  " NOVALIDATE ",     " UPLOAD OCU ",
/*  6 */  "DOWNLOAD OCU",       "SELF TEST",     "DIRECTION",       "SCREEN 1/2",
/*  7 */    "SCREEN 2/2",      "Farebox No",    "Cashbox ID",       " Previous ",
/*  8 */ "OCU Rev Video",  "FAREBOX STATUS","CLEAR COIN BYPASS","OUT OF SERVICE",
/*  9 */ "Low DoorLatch",   "HI  DoorLatch","BILL VALIDATOR",   "BILL TRANSPORT",
/* 10 */    "SMART CARD",     "FB BAUDRATE",  "NO BILL MECH",       "CHK CONFIG",
/* 11 */"FARE SELECTION",     "GFi Genfare","FRIENDLY AGENCIES",   "Maintenance",
/* 12 */   "Customer ID",    "Device Reset",     "PM Device",        "Coin Test",
/* 13 */    "Trim Diag ",   "Reset Farebox","Reset Bill Val",   "Not Available ",
/* 14 */    "Reset TRiM","Reset Peripherals", "Program Card",   "Fare Structure",
/* 15 */"Friendly Agency",  "Configuration",    "Boot Block",       "OCU Backup",
/* 16 */"Bill Validator",  "Coin Validator",  "Swipe Reader",        "TRiM Unit",
/* 17 */    "Smart Card",    "RESET COUNTS",    "NO PC CARD",    "WRITE PROTECT",
/* 18 */  " RECLASSIFY ","INVALID AGENCY",  "CHANGE/VALUE",       "MULTI-RIDE",
/* 19 */      "MAIN/DVR",  "OCU PGRM ERROR",   "UPLOAD DONE",      "PERCENT 100",
/* 20 */"FOR RECHARGE  ",    "TAKE RECEIPT",   "    Card is",     "Buttons Used",
/* 21 */"Passenger Input",  "    SCANNING ", "SONY SmrtCrd ",    "OTI3000 SCrd ",
/* 22 */ "ACS SmartCard",     "TRIM ONLINE", "CARD MISFEED ",     "    CBX     ",
/* 23 */  " AMDT ALARM ",     "  PROBE ?  ",   "COMPLETE   ",     " SIGN ALARM ",
/* 24 */ "GemProx SCrd ",  "  OK - COUP   "," OK - COUP ESC",   "   OK - VAL   ",
/* 25 */" OK - VAL ESC ",    "TRIM SC OFF ",      "RF POWER",        "MAGNETIC ",
/* 26 */     "SC NO PRT",      " SC ONLINE",    " NO DIRECT",    "OTI6000 SCrd ",
/* 27 */    "Not Enough",      " Remaining",      "VERIFIED",      "Current Cfg",
/* 28 */    "CARD QUEST",         "FAREBOX",  "CardQuest No","CARD QUEST STATUS",
/* 29 */     "DATA PORT",     "Reset J1708",   "ORIGINATION",      "DESTINATION",
/* 30 */       "PRESS #",       "TEMPORARY",     "TRiM Type",        "SC PRINT ",
/* 31 */  "TSC & SC OFF",    "TRM & SC OFF",    "DOOR ACCES",        "Door Time"
    };

char  *TextStr( int index )
{
   if( Text_Debug )
   {
     printf(" TextStr     - Language %d index %d  - ", Language, index );
     printf( "%s\n", TextTokens[(char)index]);
   }
   return TextTokens[(char)index];
}

/*************************************************
*                                                *
* Phrases                                        *
*                                                *
*************************************************/


char  *PhraseTokens[] = {

/*  0 */ "NO DRIVE - Press # to Log In  ", "NO ROUTE - Press # to Log In  ",
/*  1 */ "NO BLOCK - Press # to Log In  ", " NO RUN - Press # to Log In   ",
/*  2 */ "NO DIRECT - Press # to Log In ", "NO TRIP - Press # to Log In   ",
/*  3 */              "TRIM CARD MISFEED",                  "NO CARD STOCK",
/*  4 */ "TRiM Bypass ON                ", "TRiM Bypass OFF               ",
/*  5 */ "Device Reset                  ",                "Jumper Required",
/*  6 */                   "Load PC-Card",                "Turn FEED ON   ",
/*  7 */                "Turn FEED OFF  ",                "TRiM Update Req",
/*  8 */                "Reset Farebox  ", "Reset Bill Val                ",
/*  9 */ "Reset TRiM                    ", "Reset Peripherals             ",
/* 10 */ "   option not enabled at DS   ",       "      OCU Offline       ",
/* 11 */    "    OCU version to old     ",    "    Feature not enabled    ",
/* 12 */ " Requires Probing  "           ,   "NO TOWN - Press # to Log In ",
/* 13 */ "Reset Card Quest              ",               "Reset Smart Card",
/* 14 */ "NO                       YES"  ,            "SELECT CURRENT ZONE",
/* 15 */ "SELECT DESTINATION ZONE"       , "NO ZONE - Press # to Log In   ",
/* 16 */ "NO CITY - Press # to Log In   "
    };

char  *PhraseStr( int index )
{
   if( Text_Debug )
   {
     printf(" PhraseStr   - Language %d index %d  - ", Language, index );
     printf( "%s\n", PhraseTokens[(char)index]);
   }
   return PhraseTokens[(char)index];
}



/*************************************************
*                                                *
* Screens/Screen text                            *
*                                                *
*************************************************/

char  *ScreenTokens[]= {

/*  0 */  "    COMPLETE INFORMATION    ",     "   (config., diags., data)  ",
/*  1 */  "    FAREBOX CONFIGURATION   ",     "      J1708 DIAGNOSTICS     ",
/*  2 */  "     BURN-IN DIAGNOSTICS    ",     "   DIAGNOSTIC INFORMATION   ",
/*  3 */  " (data information follows) ",     "   SMART CARD DIAG DISPLAY  ",
/*  4 */  "      TRIM DIAGNOSTICS      ",     "    FAREBOX DATA DISPLAY    ",
/*  5 */  "      MAINTENANCE MENU      ",     "      DEVICE RESET MENU     ",
/*  6 */  "     PROGRAM PCMCIA CARD    ",     "      DEVICE MAINTENANCE    ",
/*  7 */  "CANADA  COIN VALIDATOR TEST ",     "   US  COIN VALIDATOR TEST  ",
/*  8 */  "    FIRMWARE INFORMATION    ",     "      IRMA INFORMATION      ",
/*  9 */  "      CARD INFORMATION      ",
/* 10 */"Maintenance          PM Device",   "Customer ID          Coin Test",
/* 11 */"Load PC-Card         Trim Diag",   "Program Card          OCU Card",
/* 12 */"Fare Structure      OCU Backup",   "Friendly Agency               ",
/* 13 */"Configuration                 ",   "Boot Block                    ",
/* 14 */"Bill Validator                ",   "Coin Validator                ",
/* 15 */"Swipe Reader                  ",   "TRiM Unit                     ",
/* 16 */"Smart Card                    ",               "Farebox          -",
/* 17 */            "OCU              -",               "TRiM             -",
/* 18 */            "SmartCard        -",               "Bill Validator   -",
/* 19 */            "Data System      -",                    "Make        -",
/* 20 */                 "Doors       -",                    "Serial No.  -",
/* 21 */                 "Software    -",                    "Diagnostics -",
/* 22 */                 "Entries     -",                    "Exits       -",
/* 23 */                       "Agency:",                       "Card Type:",
/* 24 */                      "Display:",                      "Start Date:",
/* 25 */                      "Expires:",                        "Serial #:",
/* 26 */                    "Remaining:",                      "Home Agency",
/* 27 */                    "Rides Usd:",                     "Expire Time:",
/* 28 */                 "Route/Direct:",                      "EMBED XFER:",
/* 29 */  " Press \x0f or # to return  ",     "   Press # for next page    ",
    "   use \x0f and \x10 or number    ",     "   to choose, # when done   ",
    "   use \x0f and \x10 to choose    ",     " Enter number followed by # ",
/* 32 */  "   press # for selections   ",     "press # to toggle selection ",
/* 33 */  "      press # to reset      ",     "      press # to clear      ",
/* 34 */  "  press # to program card   ",     " press # to clear registers ",
/* 35 */  "RF COM1          -"          ,               "RF COM2          -",
/* 36 */  "RF Version       -"          ,     "    UNLOCK LOG IN SCREEN    ",
/* 37 */ " ENTER LOG CODE FOLLOWED BY # ",                       "Radio off",
/* 38 */            "Card Quest       -",     "     Press # to return      "
         };

char  *ScreenStr( int index )
{
   if( Text_Debug )
   {
     printf(" ScreenStr   - Language %d index %d  - ", Language, index );
     printf( "%s\n", ScreenTokens[(char)index]);
   }
   return ScreenTokens[(char)index];
}



/*************************************************
*                                                *
* LCD Display text                               *
*                                                *
*************************************************/

char  *LCDTokens[]= {

/*  0 */ "                    ",
/*  1 */ "PLEASE PRESENT CARD ",             "    FOR RECHARGE    ",
/*  2 */ "     Clear Feed     ",             "   Reset Validator  ",
/*  3 */ "    Validator OOS   ",             "    Validator OK    ",
/*  4 */ "    Please remove   ",             "     your bill.     ",
/*  5 */ "  Check Coin Return ",             "  No Coins Accepted ",
/*  6 */ "  TRIM CARD MISFEED ",             "   TRiM IS OFFLINE  ",
/*  7 */ "    NO CARD STOCK   ",             "  CARD WAS MISREAD  ",
/*  8 */ "   CARD IS INVALID  ",             "  Insert into TRiM  ",
/*  9 */ "    REJECTED CARD   ",             "    BAD CARD CODE   ",
/* 10 */ "  CARD IS NOT USED  ",             "     BAD CARD ID    ",
/* 11 */ "    INVALID FARE    ",             "  CAN NOT RECHARGE  ",
/* 12 */ " RECHARGE COMPLETE  ",             "NO SERVICE AVAILABLE",
/* 13 */ "  TO SATISFY FARE   ",             " CARD RECENTLY USED ",
/* 14 */ " CARD IS NOT LISTED ",             " CARD IS BAD LISTED ",
/* 15 */ "  BAD CARD AGENCY   ",       "\177 NO           YES \176",
/* 16 */ "  PROBING ENABLED   ",             "  PRINTING RECEIPT  ",
/* 17 */ " no receipt wanted  ",             "   WANT RECEIPT ?   ",
/* 18 */ " Change is Encoded  ",             " Change Encoded on  ",
/* 19 */ "  Value Encoded On  ",       "\177 CANCEL    ACCEPT \176",
/* 20 */ "\177 LEFT BUTTON       ",          "      RIGHT BUTTON \176",
/* 21 */ "COIN VALIDATOR TEST ",             "CONTROL UNIT OFFLINE",
/* 22 */ "    CARD CREATED    ",             " SOFTWARE VERSIONS  ",
/* 23 */ "\177 CHANGE            ",          "LANGUAGE is Spanish ",
/* 24 */ "LANGUAGE is French  ",             "LANGUAGE is English ",
/* 25 */ "LANGUAGE is ?       ",             "  CHANGE to SPANISH ",
/* 26 */ "  CHANGE to FRENCH  ",             "  CHANGE to ENGLISH ",
/* 27 */ "  CHANGE to ?       ",             " COUNTRY is CANADA  ",
/* 28 */ "   COUNTRY is USA   ",             "CONFIG. as CARDQUEST",
/* 29 */ " CONFIG. as FAREBOX ",             "SELECT CONTROL UNIT ",
/* 30 */ "     GFI OCU w/MENUS",             "     GFI OCU No MENU",
/* 31 */ "             CTS DCU",             "  CHANGE to 9600    ",
/* 32 */ "  CHANGE to 19200   ",             "  CHANGE to 38400   ",
/* 33 */ "  CHANGE to 57600   ",             "  CHANGE to 115200  ",
/* 34 */ "  CHANGE to 230400  ",             "IR BAUD RATE   9600k",
/* 35 */ "IR BAUD RATE  19200k",             "IR BAUD RATE  38400k",
/* 36 */ "IR BAUD RATE  57600k",             "IR BAUD RATE 115200k",
/* 37 */ "IR BAUD RATE 230400k",             "OCU BAUD RATE  9600k",
/* 38 */ "OCU BAUD RATE 19200k",             "OCU BAUD RATE 38400k",
/* 39 */ "OCU BAUD RATE 57600k",             " CASHBOX ID ENABLED ",
/* 40 */ " CASHBOX ID DISABLED",             "SmartCard DISABLED  ",
/* 41 */ "   CHANGE to SONY   ",             "  CHANGE to OTI3000 ",
/* 42 */ "  CHANGE to ACS     ",             " DISABLE SmartCard  ",
/* 43 */ "BILL VALIDATOR JAMED",             " BILL VALIDATOR OOS ",
/* 44 */ "\177 ATTEMPT RESET     ",          " BILL VALIDATOR OK  ",
/* 45 */ "CHK CONFIGURATION   ",             "\177 SELECT FUNCTION   ",
/* 46 */ "SLOW DOWN TRIM COMM?",             "SPEED UP TRIM COMM ?",
/* 47 */ "  CLEAR TRIM PATH   ",             "    MANUAL TRIM     ",
/* 48 */ "TURN TRiM BYPASS OFF",             "TURN TRiM BYPASS ON ",
/* 49 */ " TURN OFF STATUS LED",             " TURN ON STATUS LED ",
/* 50 */ "  ISSUE TEST CARD   ",             " ISSUE DIFFERENTIAL ",
/* 51 */ "  ISSUE CUMULATIVE  ",             "   READ TEST CARD   ",
/* 52 */ "  RETURN TRIM CARD  ",             "Insert Card         ",
/* 53 */ " CHANGE NOT ALLOWED ",             "   CONFIGURED FOR   ",
/* 54 */ "DISPLAY  :          ",             "EXPIRES  :          ",
/* 55 */ "TYPE     :          ",             "REMAINING:          ",
/* 56 */ "RIDES USD:          ",             "STORED VALUE :      ",
/* 57 */ "STORED RIDE:        ",             "You Have     :      ",
/* 58 */ "Charge       :      ",             "Balance      :      ",
/* 59 */ "Fare Owed    :      ",             "Fare         :      ",
/* 60 */ "Amount Owed  :      ",             "Value        :      ",
/* 61 */ "Rides        :      ",             "Inserted     :      ",
/* 62 */ "Card Value   :      ",             "Total Value  :      ",
/* 63 */ "Using        :      ",             "Rides Remaining:    ",
/* 64 */ "SMARTCARD ID :      ",             "BILL COUNT   :      ",
/* 65 */        "SC RECHARGE :",                    "VISA CARD   :",
/* 66 */        "MASTERCARD  :",                    "DISCOVER    :",
/* 67 */ "     LAMP TEST      ",             "    MEMORY TEST     ",
/* 68 */ "   BILL VALIDATOR   ",             " CONFG. FOR NO BILL ",
/* 69 */ "   VALIDATOR OOS    ",             "   BILL TRANSPORT   ",
/* 70 */ " CONFG. FOR NO BILL ",             "     SOUND TEST     ",
/* 71 */ "     TRIM TEST      ",             " CONFG. FOR NO TRiM ",
/* 72 */ "    DISPLAY TEST    ",             "BV / TRANSPORT TEST ",
/* 73 */ "TRIM/TRANSPORT TEST ",             "TEST ALL COMPONENTS ",
/* 74 */ "       CPU TEST     ",             "  ODYSSEY SELF TEST ",
/* 75 */ "CREATE PROGRAM CARD ",             "  CREATE FARE CARD  ",
/* 76 */ " CREATE CONFIG CARD ",             "CREATE FRIENDLY CARD",
/* 77 */ "  CREATE BOOT CARD  ",             " CREATE PCMCIA CARD ",
/* 78 */ "LCD MENU MAIN SCREEN",
/* 79 */ " No Extended Memory ",                  "Ext Mem Tested ",
/* 80 */      "Not Accessable ",             "Bill Val out-of-serv",
/* 81 */ "  Entry & Exit Sens ",             "     Exit Sensor    ",
/* 82 */ "    Entry Sensor    ",             "   Undefined fault  ",
/* 83 */      "   Reset count ",             "      No comm.      ",
/* 84 */ "  Entry Exit Optic  ",             "  Exit & Optic Sens ",
/* 85 */ " Entry & Optic Sens ",             "   Optical Sensor   ",
/* 86 */ "  Entry  Exit  Mag  ",             " Exit & Mag Sensors ",
/* 87 */ " Entry & Mag Sensors",             "   Magnetic Sensor  ",
/* 88 */ " Entry  Exit  Motor ",             " Exit Sensor & Motor",
/* 89 */ "Entry Sensor & Motor",             "        Motor       ",
/* 90 */ "Entry & Exit Sensors",             "IR BAUD RATE 460800k",
/* 91 */ "IR BAUD RATE 921600k",             "  CHANGE to 460800  ",
/* 92 */ "  CHANGE to 9216000 ",             "  CHANGE to GEMPROX ",
/* 93 */ "     RF ENABLED     ",             "    RF DISABLED     ",
/* 94 */ "  SMART CARD TRiM   ",             " MAGNETIC CARD TRiM ",
/* 95 */ "  CHANGE to OTI6000 ",             "EMBED XFER :        ",
/* 96 */ "Added: "             ,             "EXP: "               ,
/* 97 */ "Old Date: "          ,             " Unable to Process  ",
/* 98 */ "Please TAP or",                    "SWIPE your card"     ,
/* 99 */ "Insert / Swipe",                   "PLEASE SHOW ID",
/*100 */ "AMERICAN EXP:"
    };


char  *LCDStr( int index )
{
   if( Text_Debug )
   {
     printf(" LCDStr      - Language %d index %d  - ", Language, index );
     printf( "%s\n", LCDTokens[(char)index]);
   }
   return LCDTokens[(char)index];
}



/*************************************************
*                                                *
* Diagnostic display                             *
*                                                *
*************************************************/

char  *DiagTokens[]= {

/*  0 */ "3 in 10 cycles",                          "More then 10",
/*  1 */     "Zero Count",                           "BV OOS Time",
/*  2 */  "TRiM OOS Time",                         "No Stock Time",
/*  3 */   "Misfeed Time",              "Not Tested   (base) 256k",
/*  4 */ "Ext Not Tested",                            "Optic Sens",
/*  5 */      "Mag Sense",                             "Exit Sens"
    };

char  *DiagStr( int index )
{
   if( Text_Debug )
   {
     printf(" DiagStr     - Language %d index %d  - ", Language, index );
     printf( "%s\n", DiagTokens[(char)index]);
   }
   return DiagTokens[(char)index];
}


/*************************************************
*                                                *
* Ocdsp.c Text                                   *
*                                                *
*************************************************/


char  *OcdspTokens[] = {

/*  0 */ "Agency ID       ", "Old Agency ID   ", "J1708 MID       ",
/*  1 */ "Max Events      ", "Passback Time   ", "Dump Time       ",
/*  2 */ "Bill Escrow Time", "Memory Threshold", "Coin Threshold  ",
/*  3 */ "Bill Threshold  ", "Pass Exception  ", "LAMetroOption   ",
/*  4 */ "OOS_Coin_Bill   ", "Logon           ", "HourlyEvent     ",
/*  5 */ "Daylight        ", "RevStatus       ", "TransPrintOption",
/*  6 */ "OptionFlags 1   ", "OptionFlags 2   ", "OptionFlags 3   ",
/*  5 */ "OptionFlags 4   ", "OptionFlags 5   ", "OptionFlags 6   ",
         "OptionFlags 7   ", "OptionFlags 8   ", "BillsTaken      ",
/*  6 */ "----------------------------",
/*  7 */ "PROBE INFORMATION:          ",
/*  8 */ "  Probe Type    ", "  MUX Version   ", "  Prob ID       ",
/*  9 */ "  User ID       ", "  Location      ", "  Lockcode      ",
/* 10 */ "  Lockflag      ",
/* 11 */ "----------------------------",
/* 12 */ "J1708 INFORMATION:          ",
/* 13 */ "  Total Mess Cnt", "  FB Message Cnt", "  Idle Line Cnt ",
/* 14 */ "  Timeout       ", "  LRC Error     ", "  Collision     ",
/* 15 */ "  No Response   ", "  Stop Received ", "  Time Received ",
/* 16 */ "  Time Err Rec. ", "  Message Error ", "  LAT/LONG Rec. ",
/* 17 */ "  Stop          ", "  Latitude      ", "  Longitude     ",
/* 18 */ "  J1708 Poll    ", "  J1708 Pid 254 ", "  J1708 Config. ",
/* 19 */ "  Idle Enabled  ", "  J1708 MID     ", "  Stat/Idle tmr ",
/* 20 */ "  J1708 Idle Rst", "  J1708 Idle Err", "  J1708 PRO. RST",
/* xx */ "  J1708 COMM Rst", "  J1708 Reset   ", "----------------------------",
/* 21 */ "BURNIN TIME     ", "BURNIN RESETS   ",
/* 22 */ "BILL VALIDATOR:             ",
/* 23 */ "  Serial Number ", "  Bill Reset Cnt", "  Status        ",
/* 24 */ "  BV BurnIn Time",
/* 25 */ "BILL TRANSPORT:             ",
/* 26 */ "  Cycle on Time ", "  Referance     ", "  Lowest Choper ",
/* 27 */ "  Highest Choper", "  Threshold     ", "  BT BurnIn Time",
/* 28 */ "MEMORY:                     ",
/* 29 */ "  Tested Good   ", "  Not Accessable",
/* 30 */ "TRiM UNIT:                  ",
/* 31 */ "  Serial Number ", "  Mode          ", "  Reads         ",
/* 32 */ "  Misreads      ", "  Writes        ", "  Bad Verifies  ",
/* 33 */ "  Prints        ", "  Issues        ", "  Jams          ",
/* 34 */ "  Clr Jams      ", "  Cold Starts   ", "  Warm Starts   ",
/* 35 */ "  Power Downs   ", "  Resets        ", "  TRiM Burn Time",
/* 36 */ "----------------------------",
/* 37 */ "Bus Number      ", "Farebox Serial #", "Cashbox Serial #",
/* 38 */ "Probe Count     ", "Warm Starts     ", "Cold Starts     ",
/* 39 */ "Event Count     ", "Coins Returned  ", "Dime Count      ",
/* 40 */ "Penny Count     ", "Nickel Count    ", "Quarter Count   ",
/* 41 */ "Dollar Coin     ", "Half Dollar     ", "Total Coins     ",
/* 42 */ "Coin Errors     ", "Bill Count Err  ", "Bill False Start",
/* 43 */ "Bill Rejected   ", "Bill Returned   ", "Attempt Bill Rtn",
/* 44 */ "Bill Vend Err   ", "Total Paper     ", "$ 1 Bill Count  ",
/* 45 */ "$ 2 Bill Count  ", "$ 5 Bill Count  ", "$10 Bill Count  ",
/* 46 */ "$20 Bill Count  ", "Total Passes    ", "Pass Misreads   ",
/* 47 */ "Passback Count  ", "Invalid Passes  ", "Expired Passes  ",
/* 48 */ "Badlist Passes  ", "Dump Count      ", "Dump Revenue    ",
/* 49 */ "Estimated Revenu", "Magnetic Revenue"
   };

char  *OcdspTokens2[] = {

/* 50 */ "----------------------------",
/* 51 */ "TRIM DIAGNOSTICS:           ",
/* 52 */ " T  Reads       ",  " T  Misreads    ", " T  Writes      ",
/* 53 */ " T  Bad Verifies",  " T  Prints      ", " T  Issues      ",
/* 54 */ " T  Card Jams   ",  " T  Cold Starts ", " T  Warm Starts ",
/* 55 */ " T  Power Downs ",  " T  Resets      ", " T  Print Error ",
/* 56 */ " T  C Reads     ",  " T  C Misreads  ", " T  C Writes    ",
/* 57 */ " T  C Bad Verify",  " T  C Prints    ", " T  C Issues    ",
/* 58 */ " T  C Cycles    ",  " T  C Jams      ",
/* 59 */ "----------------------------",
/* 60 */ "SMART CARD DIAGNOSTICS:     ",
/* 61 */ " SC  Reads      ",  " SC  Read Errors", " SC  Read Block ",
/* 62 */ " SC  Misread    ",  " SC  Read Recovy", " SC  Recovry Err",
/* 63 */ " SC  Writes     ",  " SC  Write Err. ", " SC  Write Block",
/* 64 */ " SC  WriteBlk Er",  " SC  Offline    ", " SC  Retries    ",
/* 65 */ " SC  Comm. Error",  " SC  C Reads    ", " SC  C Writes   ",
/* 66 */ "----------------------------",
/* 67 */ "Time            ",  "Date            ", "Total Revenue   "
   };


char  *OcdispStr( int index )
{
   if( index >= 136 )
   {
     index = index-136;

     if( Text_Debug )
     {
       printf(" OcdispStr-2 - Language %d index %d  - ", Language, index );
       printf( "%s\n", OcdspTokens2[(char)index]);
     }
     return OcdspTokens2[(char)index];
   }
   else
   {
     if( Text_Debug )
     {
       printf(" OcdispStr   - Language %d index %d  - ", Language, index );
       printf( "%s\n", OcdspTokens[(char)index]);
     }
     return OcdspTokens[(char)index];
   }
}

/*************************************************
*                                                *
* Probe  Text                                    *
*                                                *
*************************************************/
char  *ProbeTokens[] = {

/*  0 */ "ACS DATA   ", "LAST BLK   ", "BAD FARE   ", "BLK DATA   ",
/*  1 */ "LAST BLK   ", "DATETIME   ", "BAD LIST   ",      "BDLST ",
/*  2 */ "BADLSTLB   ", "CONFIG     ", "NEW CONF   ", "BAD CONF   ",
/*  3 */ "TRN CTL    ",       "TRAN ", "TRN LAST   ", "TRIM CNF   ",
/*  4 */ "BAD CNF    ", "+COMPLETE+ ", "BAD FRD    ", "DATA OUT   ",
/*  5 */       "DATA ", "NOT HOME   ", "COMPLETE   ", "DRIVER LIST",
/*  6 */      "DVR   ", "DRIVER LAST", "ROUTE LIST ",      "RTE   ",
/*  7 */ "ROUTE LAST ", "ACS CONFIG ",      "ACS   ", " ACS LAST  ",
/*  8 */ "ZONE PARMS ",     "ZONE   ", "ZONE LAST  ", " BAD ZONE  "
   };


char  *ProbeTextStr( int index )
{
   if( Text_Debug )
   {
     printf(" Probe Language %d index %d  - ", Language, index );
     printf( "%s\n", ProbeTokens[(char)index]);
   }
   return ProbeTokens[(char)index];
}

/*************************************************
*                                                *
* Gate - General Text (operation)                *
*                                                *
*************************************************/

char  *GateTokens[] = {
/*  0 */ "THANK YOU - ENTER",    "ENJOY YOUR RIDE",
/*  1 */ "PLEASE REMOVE PASS",   "INSERT PASS AGAIN",
/*  2 */ "PASS INVALID",         "INVALID DATE",
         "BAD VERIFY",           "**  GATE CLOSED  **",
/*  5 */ "PLEASE CLOSE GATE",    "ENTER",
         "GATE NUMBER",          "GATE COMM. CHECK ?",
         "GATE NUMBERING CHECK", "LIGHT  IN",
         "LIGHT OUT",            "HEAVY  IN",
         "HEAVY OUT",            "PLEASE SEE AGENT"
   };

char  *GateStr( int index )
{
   if( Text_Debug )
   {
     printf(" Gate Language %d index %d  - ", Language, index );
     printf( "%s\n", GateTokens[(char)index]);
   }
   return GateTokens[(char)index];
}
