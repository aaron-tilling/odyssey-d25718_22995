#-----------------------------------------------------------------------------+
#  Src File:   Version.m                                                      |
#  Authored:   09/10/94, tgh, generic                                         |
#  Function:                                                                  |
#  Comments:                                                                  |
#       itools compiler :                                                     |
#       c68332,   Version 3.119.1.2 (c) 1993 Intermetrics, Inc                |
#       asm68332, Version 3.119.1.2 (c) 1993 Intermetrics, Inc                |
#       llink,    Version 1.109.1.1 (c) 1992 Intermetrics, Inc                |
#       form,     Version 4.48.1.2  (c) 1993 Intermetrics, Inc                |
#                                                                             |
#           Copyright (c) 1994-2000  GFI All Rights Reserved                  |
#-----------------------------------------------------------------------------+

 OBJS   = \
 pmain.ol fpga.ol  vect.ol  regs.ol tmrs.ol \
 main.ol  quart.ol commp.ol dbgc.ol dbgp.ol \
 port.ol  rtc.ol   sclk.ol  disp.ol ocup.ol \
 door.ol  sound.ol card.ol  keyp.ol tests.ol \
 lamp.ol  coin.ol  232c.ol  tlog.ol mgctl.ol \
 485c.ol  485p.ol  prbc.ol  prbp.ol scic.ol \
 data.ol  test.ol  menu.ol  msgs.ol dbase.ol \
 util.ol  dispm.ol fare.ol  dll.ol  misc.ol \

OBJ2   = \
 pbqp.ol     diag.ol    cbid.ol   scip.ol  stat.ol \
 flash.ol    boot.ol    lcdc.ol   q0ac.ol  q1ac.ol \
 dmenu.ol    bvp.ol     dalas.ol  dtm.ol   text.ol \
 rf2_com.ol  422c.ol    trans.ol  hdlc.ol  mgdsp.ol \
 ocdsp.ol    rboot.ol   scctl.ol  issue.ol scpr.ol \
 gemp_fbx.ol oti3000.ol sscp.ol   proc.ol  wiport.ol \
 oti6000.ol  version.ol

OBJ3   = \
 ctsdcup.ol 232p.ol     rf_comp.ol \
 422p.ol    rf2_comp.ol b22995.ol  \
 octalcc.ol octaldc.ol  rf_com.ol  octald.ol

XPATH  = Z:\G\itools

AS     = $(XPATH)\x\asm68332
CC     = $(XPATH)\x\c68332
LINK   = $(XPATH)\x\llink
FORM   = $(XPATH)\x\form

MAP    = $(XPATH)\x\gsmap

INC    = ..\inc

CFLAGS = -S $(XPATH)\rtlibs\lib332\inc -S $(INC) -err+ error -ia -cs $(dbgflg)
#CFLAGS = -S $(XPATH)\rtlibs\lib332\inc -S $(INC) -ia -cs $(dbgflg)
AFLAGS = -S $(INC) -l -ex
LFLAGS = -i Ver22995.lnk -L $(XPATH)\rtlibs\lib332\lib\lib332 -rs idata -b _rompOutSeg -o 22995.ab -c loc1.cmd
FFLAGS = -f xm -w 80000
FFLAG2 = -f xm -w 100000
MFLAGS = -n -o

.asm.ol     :
               $(AS) $*.asm $(AFLAGS)

boot.ol :   boot.asm $(INC)\pmain.inc $(INC)\regs.inc $(INC)\gen.inc

pmain.ol :  pmain.asm $(INC)\pmain.inc $(INC)\regs.inc $(INC)\gen.inc

fpga.ol :   fpga.asm

vect.ol :   vect.asm

quart.ol :  quart.asm

dbgc.ol :   dbgc.asm

port.ol :   port.asm

232c.ol :   232c.asm

485c.ol :   485c.asm

422c.ol :   422c.asm

prbc.ol :   prbc.asm

scic.ol :   scic.asm

misc.ol :   misc.asm $(INC)\gen.inc

flash.ol :  flash.asm

q0ac.ol :   q0ac.asm

q1ac.ol :   q1ac.asm

dalas.ol :  dalas.asm

rboot.ol :  rboot.asm

rf_com.ol : rf_com.asm

octalcc.ol : octalcc.asm

octaldc.ol : octaldc.asm

rf2_com.ol : rf2_com.asm

.c.ol       :
               $(CC) $*.c $(CFLAGS)

main.ol :   main.c $(INC)\led.h $(INC)\magcard.h $(INC)\menu.h\
            $(INC)\rtc.h $(INC)\text.h

regs.ol :   regs.c $(INC)\regs.h

tmrs.ol :   tmrs.c $(INC)\timers.h $(INC)\coin.h $(INC)\bill.h $(INC)\ability.h

commp.ol :  commp.c $(INC)\comm.h

dbgp.ol :   dbgp.c $(INC)\rs422.h $(INC)\trim.h  $(INC)\text.h

rtc.ol :    rtc.c

sclk.ol :   sclk.c

disp.ol :   disp.c  $(INC)\text.h

coin.ol :   coin.c $(INC)\coin.h $(INC)\port.h $(INC)\util.h $(INC)\fbd.h\
            $(INC)\text.h $(INC)\timers.h

door.ol :   door.c $(INC)\door.h $(INC)\port.h $(INC)\text.h

sound.ol :  sound.c $(INC)\sound.h $(INC)\port.h

card.ol :   card.c $(INC)\port.h $(INC)\text.h

keyp.ol :   keyp.c $(INC)\port.h $(INC)\menu.h $(INC)\diag.h $(INC)\text.h

lamp.ol :   lamp.c $(INC)\lamp.h $(INC)\bill.h $(INC)\port.h

tlog.ol :   tlog.c

485p.ol :   485p.c $(INC)\trim.h $(INC)\text.h

422p.ol :   422p.c $(INC)\rs422.h $(INC)\text.h

prbp.ol :   prbp.c $(INC)\probe.h $(INC)\text.h

data.ol :   data.c $(INC)\fbd.h

test.ol :   test.c $(INC)\diag.h $(INC)\text.h

tests.ol :  tests.c

menu.ol :   menu.c $(INC)\menu.h $(INC)\msgs.h $(INC)\text.h

msgs.ol :   msgs.c $(INC)\msgs.h

dbase.ol :  dbase.c $(INC)\transact.h $(INC)\gen.h

util.ol :   util.c $(INC)\util.h $(INC)\port.h

dispm.ol :  dispm.c $(INC)\menu.h

fare.ol :   fare.c $(INC)\menu.h $(INC)\sound.h $(INC)\transact.h\
            $(INC)\fbd.h $(INC)\text.h

dll.ol :    dll.c $(INC)\menu.h

mgctl.ol :  mgctl.c $(INC)\menu.h $(INC)\text.h

pbqp.ol :   pbqp.c $(INC)\pbq.h

diag.ol :   diag.c $(INC)\menu.h $(INC)\bill.h $(INC)\coin.h $(INC)\rs485.h\
            $(INC)\diag.h $(INC)\text.h

cbid.ol :   cbid.c $(INC)\timers.h $(INC)\transact.h $(INC)\ability.h

scip.ol :   scip.c $(INC)\text.h

stat.ol :   stat.c $(INC)\status.h $(INC)\timers.h  $(INC)\text.h

lcdc.ol :   lcdc.c $(INC)\lcddisp.h $(INC)\text.h $(INC)\timers.h

oti3000.ol : oti3000.c $(INC)\oti.h $(INC)\scmd.h $(INC)\scard.h $(INC)\text.h

bvp.ol :    bvp.c $(INC)\port.h $(INC)\bill.h $(INC)\text.h $(INC)\timers.h

dtm.ol :    dtm.c

trans.ol :  trans.c $(INC)\transact.h

hdlc.ol :   hdlc.c

232p.ol :   232p.c $(INC)\text.h

mgdsp.ol :  mgdsp.c $(INC)\text.h

ocup.ol :   ocup.c $(INC)\ocu.h $(INC)\menu.h $(INC)\text.h

ocdsp.ol :  ocdsp.c $(INC)\text.h

scctl.ol :  scctl.c $(INC)\scmd.h $(INC)\scard.h $(INC)\text.h

issue.ol :  issue.c $(INC)\magcon.h $(INC)\magcard.h $(INC)\text.h

scpr.ol :   scpr.c $(INC)\text.h

dmenu.ol :  dmenu.c $(INC)\fbxmenu.h $(INC)\menuproc.h

ctsdcup.ol : ctsdcup.c $(INC)\cts_dcu.h

text.ol     : text.c $(INC)\text.h

text_eng.ol : text_eng.c $(INC)\text.h

text_fre.ol : text_fre.c $(INC)\text.h

rf2_comp.ol : rf2_comp.c

232p_rf.ol  : 232p_rf.c

422p_ocu.ol : 422p_ocu.c

gemp_fbx.ol : gemp_fbx.c $(INC)\gemp.h $(INC)\scmd.h $(INC)\scard.h $(INC)\text.h

rf_comp.ol  : rf_comp.c

octald.ol   : octald.c

octal.ol    : octal.c

oti6000.ol  : oti6000.c $(INC)\oti.h $(INC)\cnf.h $(INC)\scmd.h $(INC)\scard.h\
              $(INC)\text.h

sscp.ol     : sscp.c $(INC)\oti.h $(INC)\scmd.h $(INC)\scard.h $(INC)\text.h

proc.ol     : proc.c $(INC)\process.h

b22995.ol   : b22995.c

b22988.ol   : b22988.c

wiport.ol   : wiport.c $(INC)\rf.h

version.ol  : version.c


22995.ab      :  $(OBJS) $(OBJ2) $(OBJ3)
                 $(LINK) $(LFLAGS)

22995.s19     :  22995.ab
                 $(FORM) 22995.ab $(FFLAG2) -o 22995.s19

22995.map     :  22995.s19
                 $(MAP)  22995.ab $(MFLAGS)


